# Build.
FROM mcr.microsoft.com/dotnet/sdk:8.0.100-bookworm-slim-amd64 as build
RUN apt-get update && \
    apt-get install -y python3 libatomic1
RUN dotnet workload install wasm-tools
COPY ./home-server.Client.Web /home-server.Client.Web
WORKDIR /home-server.Client.Web
RUN dotnet publish \
    -c Release \
    -o /output

# Runtime.
FROM fholzer/nginx-brotli:v1.24.0
COPY ./client.nginx.conf /etc/nginx/nginx.conf
COPY --from=build /output/wwwroot /usr/share/nginx/html
