using System;
using System.Threading.Tasks;
using home_server.Services.Users;
using Moq;
using Serilog;
using Xunit;

namespace home_server.Tests.Services.Users;

public class DefaultUserRegistrationServiceTest
{
    Mock<IUserRepository> m_userRepository;
    Mock<IUserRegistrationService> m_userRegistrationService;
    DefaultUserRegistrationService m_service;

    public DefaultUserRegistrationServiceTest()
    {
        m_userRepository = new Mock<IUserRepository>(MockBehavior.Strict);
        m_userRegistrationService = new Mock<IUserRegistrationService>(MockBehavior.Strict);
        m_service = new DefaultUserRegistrationService(
            m_userRepository.Object,
            m_userRegistrationService.Object,
            Log.Logger);
    }

    [Fact]
    public async Task CreateDefaultUserAsync_Test()
    {
        // Setup.
        m_userRepository
            .Setup(x => x.CountAsync())
            .ReturnsAsync(0);

        m_userRegistrationService
            .Setup(x => x.RegisterAsync("admin", "admin"))
            .ReturnsAsync(new SuccessRegistrationResult("adminid"));
        
        // Test.
        await m_service.CreateDefaultUserAsync();
        
        // Assert.
        m_userRegistrationService.VerifyAll();
    }

    [Fact]
    public async Task CreateDefaultUserAsync_Skip()
    {
        // Setup.
        m_userRepository
            .Setup(x => x.CountAsync())
            .ReturnsAsync(1);
        
        // Test.
        await m_service.CreateDefaultUserAsync();
    }
}
