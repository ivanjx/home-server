using System;
using System.Threading.Tasks;
using home_server.Services;
using home_server.Services.Authentication;
using home_server.Services.Users;
using Moq;
using Serilog;
using Xunit;

namespace home_server.Tests.Services.Users;

public class UserRegistrationServiceTest
{
    Mock<IUserRepository> m_userRepository;
    Mock<IAuthenticationStringGenerator> m_authenticationStringGenerator;
    Mock<ITimeService> m_timeService;
    UserRegistrationService m_service;

    public UserRegistrationServiceTest()
    {
        m_userRepository = new Mock<IUserRepository>(MockBehavior.Strict);
        m_authenticationStringGenerator = new Mock<IAuthenticationStringGenerator>(MockBehavior.Strict);
        m_timeService = new Mock<ITimeService>(MockBehavior.Strict);
        m_service = new UserRegistrationService(
            m_userRepository.Object,
            m_authenticationStringGenerator.Object,
            m_timeService.Object,
            Log.Logger);
    }

    [Fact]
    public async Task RegisterAsync_Test()
    {
        // Setup.
        DateTime now = DateTime.Parse("2023-11-09 01:00:00")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        
        m_userRepository
            .Setup(x => x.IsUsernameExistsAsync("user1"))
            .ReturnsAsync(false);

        m_authenticationStringGenerator
            .Setup(x => x.GeneratePads())
            .Returns("pads");

        m_authenticationStringGenerator
            .Setup(x => x.GeneratePasswordHash("password", "pads"))
            .Returns("passwordhash");

        m_userRepository
            .Setup(x =>
                x.AddAsync(
                    "user1",
                    "passwordhash",
                    "pads",
                    now))
            .ReturnsAsync("userid");
        
        // Test.
        RegistrationResult result = await m_service.RegisterAsync(
            "user1",
            "password");
        
        // Assert.
        SuccessRegistrationResult successResult =
            Assert.IsType<SuccessRegistrationResult>(result);
        Assert.Equal("userid", successResult.UserId);

        m_userRepository.VerifyAll();
    }

    [Fact]
    public async Task RegisterAsync_Validate()
    {
        await Assert.ThrowsAsync<InvalidParameterException>(() =>
            m_service.RegisterAsync("us", "123"));
        await Assert.ThrowsAsync<InvalidParameterException>(() =>
            m_service.RegisterAsync("ususususususususususu", "123"));
        await Assert.ThrowsAsync<InvalidParameterException>(() =>
            m_service.RegisterAsync("us#123", "123"));
    }

    [Fact]
    public async Task RegisterAsync_ValidateDuplicateUsername()
    {
        // Setup.
        m_userRepository
            .Setup(x => x.IsUsernameExistsAsync("user1"))
            .ReturnsAsync(true);
        
        // Test.
        RegistrationResult result = await m_service.RegisterAsync(
            "user1",
            "password");
        
        // Assert.
        Assert.IsType<DuplicateUsernameRegistrationResult>(result);
    }
}
