using System;
using home_server.Services.Authentication;
using Xunit;

namespace home_server.Tests.Services.Authentication;

public class AuthenticationStringGeneratorTest
{
    AuthenticationStringGenerator m_service;

    public AuthenticationStringGeneratorTest()
    {
        m_service = new AuthenticationStringGenerator();
    }

    [Fact]
    public void HashToken_Test()
    {
        string result = m_service.HashToken("token");
        Assert.Equal(
            "3C469E9D6C5875D37A43F353D4F88E61FCF812C66EEE3457465A40B0DA4153E0",
            result);
    }

    [Fact]
    public void GeneratePasswordHash_Test()
    {
        string result = m_service.GeneratePasswordHash("password", "pads");
        Assert.Equal(
            "2715011A0E9923C7ED3A4ACD9FA54BD2DADE5C25BF8B4D2E2A5FCE2E416BC3CF",
            result);
    }
}
