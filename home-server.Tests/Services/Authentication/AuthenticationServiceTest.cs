using System;
using System.Threading.Tasks;
using home_server.Services;
using home_server.Services.Authentication;
using Moq;
using Serilog;
using Xunit;

namespace home_server.Tests.Services.Authentication;

public class AuthenticationServiceTest
{
    Mock<IUserAuthenticationRepository> m_authenticationRepository;
    Mock<IUserAuthenticationTokenRepository> m_authenticationTokenRepository;
    Mock<IAuthenticationStringGenerator> m_authenticationStringGenerator;
    Mock<ITimeService> m_timeService;
    AuthenticationService m_service;

    public AuthenticationServiceTest()
    {
        m_authenticationRepository = new Mock<IUserAuthenticationRepository>(MockBehavior.Strict);
        m_authenticationTokenRepository = new Mock<IUserAuthenticationTokenRepository>(MockBehavior.Strict);
        m_authenticationStringGenerator = new Mock<IAuthenticationStringGenerator>(MockBehavior.Strict);
        m_timeService = new Mock<ITimeService>(MockBehavior.Strict);
        m_service = new AuthenticationService(
            m_authenticationRepository.Object,
            m_authenticationTokenRepository.Object,
            m_authenticationStringGenerator.Object,
            m_timeService.Object,
            Log.Logger);
    }

    [Fact]
    public async Task AuthenticateAsync_Test()
    {
        // Setup.
        m_authenticationRepository
            .Setup(x => x.GetByUsernameAsync("username"))
            .ReturnsAsync(
                new UserAuth(
                    "userid",
                    "hash",
                    "pads"));

        m_authenticationStringGenerator
            .Setup(x =>
                x.GeneratePasswordHash(
                    "password",
                    "pads"))
            .Returns("hash");

        DateTime now = DateTime.Parse("2023-11-13 01:00:00")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);

        m_authenticationStringGenerator
            .Setup(x => x.GenerateToken())
            .Returns("rawtoken");
        m_authenticationStringGenerator
            .Setup(x => x.HashToken("rawtoken"))
            .Returns("authtoken");

        UserAuthenticationToken token = new UserAuthenticationToken(
            "authtoken",
            true,
            now,
            now.AddDays(60),
            "android &amp; iphone",
            "1.1.1.1",
            "userid",
            "username");
        m_authenticationTokenRepository
            .Setup(x => x.AddAsync(token))
            .ReturnsAsync(token);
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "username",
            "password",
            "android & iphone",
            "1.1.1.1",
            true);
        
        // Assert.
        SuccessAuthenticationResult successResult =
            Assert.IsType<SuccessAuthenticationResult>(result);
        Assert.Equal("rawtoken", successResult.RawToken);
        Assert.Equal(token, successResult.Token);

        m_authenticationRepository.VerifyAll();
        m_authenticationTokenRepository.VerifyAll();
    }

    [Fact]
    public async Task AuthenticateAsync_ShortLived()
    {
        // Setup.
        m_authenticationRepository
            .Setup(x => x.GetByUsernameAsync("username"))
            .ReturnsAsync(
                new UserAuth(
                    "userid",
                    "hash",
                    "pads"));

        m_authenticationStringGenerator
            .Setup(x =>
                x.GeneratePasswordHash(
                    "password",
                    "pads"))
            .Returns("hash");

        DateTime now = DateTime.Parse("2023-11-13 01:00:00")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);

        m_authenticationStringGenerator
            .Setup(x => x.GenerateToken())
            .Returns("rawtoken");
        m_authenticationStringGenerator
            .Setup(x => x.HashToken("rawtoken"))
            .Returns("authtoken");

        UserAuthenticationToken token = new UserAuthenticationToken(
            "authtoken",
            false,
            now,
            now.AddHours(1),
            "android &amp; iphone",
            "1.1.1.1",
            "userid",
            "username");
        m_authenticationTokenRepository
            .Setup(x => x.AddAsync(token))
            .ReturnsAsync(token);
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "username",
            "password",
            "android & iphone",
            "1.1.1.1",
            false);
        
        // Assert.
        SuccessAuthenticationResult successResult =
            Assert.IsType<SuccessAuthenticationResult>(result);
        Assert.Equal("rawtoken", successResult.RawToken);
        Assert.Equal(token, successResult.Token);

        m_authenticationRepository.VerifyAll();
        m_authenticationTokenRepository.VerifyAll();
    }

    [Fact]
    public async Task AuthenticateAsync_InvalidUsername()
    {
        // Setup.
        m_authenticationRepository
            .Setup(x => x.GetByUsernameAsync("username"))
            .ReturnsAsync(null as UserAuth);
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "username",
            "password",
            "android & iphone",
            "1.1.1.1",
            true);
        
        // Assert.
        Assert.IsType<InvalidAuthenticationResult>(result);
        m_authenticationRepository.VerifyAll();
    }

    [Fact]
    public async Task AuthenticateAsync_InvalidPassword()
    {
        // Setup.
        m_authenticationRepository
            .Setup(x => x.GetByUsernameAsync("username"))
            .ReturnsAsync(
                new UserAuth(
                    "userid",
                    "hash",
                    "pads"));

        m_authenticationStringGenerator
            .Setup(x =>
                x.GeneratePasswordHash(
                    "password",
                    "pads"))
            .Returns("hashhash");
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "username",
            "password",
            "android & iphone",
            "1.1.1.1",
            false);
        
        // Assert.
        Assert.IsType<InvalidAuthenticationResult>(result);
        m_authenticationRepository.VerifyAll();
    }

    [Fact]
    public async Task AuthenticateAsync_Token_Test()
    {
        // Setup.
        DateTime now = DateTime.Parse("2023-11-13 01:00:00")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        
        m_authenticationStringGenerator
            .Setup(x => x.HashToken("rawtoken"))
            .Returns("authtoken");

        UserAuthenticationToken token = new UserAuthenticationToken(
            "authtoken",
            false,
            now.AddMinutes(-29),
            now.AddMinutes(31),
            "android",
            "1.1.1.1",
            "userid",
            "username");
        m_authenticationTokenRepository
            .Setup(x => x.GetByIdAsync("authtoken"))
            .ReturnsAsync(token);
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "rawtoken",
            "1.1.1.1");
        
        // Assert.
        SuccessAuthenticationResult successResult =
            Assert.IsType<SuccessAuthenticationResult>(result);
        Assert.Equal(token, successResult.Token);
        Assert.Equal("rawtoken", successResult.RawToken);

        m_authenticationTokenRepository.VerifyAll();
    }

    [Fact]
    public async Task AuthenticateAsync_Token_InvalidToken()
    {
        // Setup.
        m_authenticationStringGenerator
            .Setup(x => x.HashToken("rawtoken"))
            .Returns("authtoken");

        m_authenticationTokenRepository
            .Setup(x => x.GetByIdAsync("authtoken"))
            .ReturnsAsync(null as UserAuthenticationToken);
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "rawtoken",
            "1.1.1.1");
        
        // Assert.
        Assert.IsType<InvalidTokenAuthentcationResult>(result);
        m_authenticationTokenRepository.VerifyAll();
    }

    [Fact]
    public async Task AuthenticateAsync_Token_Expired()
    {
        // Setup.
        DateTime now = DateTime.Parse("2023-11-13 01:00:00")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        
        m_authenticationStringGenerator
            .Setup(x => x.HashToken("rawtoken"))
            .Returns("authtoken");

        UserAuthenticationToken token = new UserAuthenticationToken(
            "authtoken",
            false,
            now.AddMinutes(-60),
            now.AddMinutes(-1),
            "android",
            "1.1.1.1",
            "userid",
            "username");
        m_authenticationTokenRepository
            .Setup(x => x.GetByIdAsync("authtoken"))
            .ReturnsAsync(token);

        m_authenticationTokenRepository
            .Setup(x => x.DeleteAsync("authtoken"))
            .Returns(Task.CompletedTask);
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "rawtoken",
            "1.1.1.1");
        
        // Assert.
        Assert.IsType<InvalidTokenAuthentcationResult>(result);
        m_authenticationTokenRepository.VerifyAll();
    }

    [Fact]
    public async Task AuthenticateAsync_Token_Expired_LongLived()
    {
        // Setup.
        DateTime now = DateTime.Parse("2023-11-13 01:00:00")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        
        m_authenticationStringGenerator
            .Setup(x => x.HashToken("rawtoken"))
            .Returns("authtoken");

        UserAuthenticationToken token = new UserAuthenticationToken(
            "authtoken",
            true,
            now.AddDays(-60),
            now.AddDays(-1),
            "android",
            "1.1.1.1",
            "userid",
            "username");
        m_authenticationTokenRepository
            .Setup(x => x.GetByIdAsync("authtoken"))
            .ReturnsAsync(token);

        m_authenticationTokenRepository
            .Setup(x => x.DeleteAsync("authtoken"))
            .Returns(Task.CompletedTask);
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "rawtoken",
            "1.1.1.1");
        
        // Assert.
        Assert.IsType<InvalidTokenAuthentcationResult>(result);
        m_authenticationTokenRepository.VerifyAll();
    }

    [Fact]
    public async Task AuthenticateAsync_Token_Extend()
    {
        // Setup.
        DateTime now = DateTime.Parse("2023-11-13 01:00:00")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        
        m_authenticationStringGenerator
            .Setup(x => x.HashToken("rawtoken"))
            .Returns("authtoken");

        UserAuthenticationToken token = new UserAuthenticationToken(
            "authtoken",
            false,
            now.AddMinutes(-30),
            now.AddMinutes(30),
            "android",
            "1.1.1.1",
            "userid",
            "username");
        m_authenticationTokenRepository
            .Setup(x => x.GetByIdAsync("authtoken"))
            .ReturnsAsync(token);

        m_authenticationTokenRepository
            .Setup(x =>
                x.UpdateExpireTimeWhenNotExpiredAsync(
                    "authtoken",
                    now,
                    now.AddMinutes(60)))
            .Returns(Task.CompletedTask);
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "rawtoken",
            "1.1.1.1");
        
        // Assert.
        SuccessUpdatedAuthenticationResult successResult =
            Assert.IsType<SuccessUpdatedAuthenticationResult>(result);
        UserAuthenticationToken newToken = token with
        {
            ExpireAt = now.AddMinutes(60)
        };
        Assert.Equal(newToken, successResult.Token);
        Assert.Equal("rawtoken", successResult.RawToken);
    }

    [Fact]
    public async Task AuthenticateAsync_Token_Extend_LongLived()
    {
        // Setup.
        DateTime now = DateTime.Parse("2023-11-13 01:00:00")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        
        m_authenticationStringGenerator
            .Setup(x => x.HashToken("rawtoken"))
            .Returns("authtoken");

        UserAuthenticationToken token = new UserAuthenticationToken(
            "authtoken",
            true,
            now.AddDays(-30),
            now.AddDays(30),
            "android",
            "1.1.1.1",
            "userid",
            "username");
        m_authenticationTokenRepository
            .Setup(x => x.GetByIdAsync("authtoken"))
            .ReturnsAsync(token);

        m_authenticationTokenRepository
            .Setup(x =>
                x.UpdateExpireTimeWhenNotExpiredAsync(
                    "authtoken",
                    now,
                    now.AddDays(60)))
            .Returns(Task.CompletedTask);
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "rawtoken",
            "1.1.1.1");
        
        // Assert.
        SuccessUpdatedAuthenticationResult successResult =
            Assert.IsType<SuccessUpdatedAuthenticationResult>(result);
        UserAuthenticationToken newToken = token with
        {
            ExpireAt = now.AddDays(60)
        };
        Assert.Equal(newToken, successResult.Token);
        Assert.Equal("rawtoken", successResult.RawToken);
    }

    [Fact]
    public async Task AuthenticateAsync_Token_UpdateIpAddress()
    {
        // Setup.
        DateTime now = DateTime.Parse("2023-11-13 01:00:00")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        
        m_authenticationStringGenerator
            .Setup(x => x.HashToken("rawtoken"))
            .Returns("authtoken");

        UserAuthenticationToken token = new UserAuthenticationToken(
            "authtoken",
            false,
            now.AddMinutes(-29),
            now.AddMinutes(31),
            "android",
            "1.1.1.1",
            "userid",
            "username");
        m_authenticationTokenRepository
            .Setup(x => x.GetByIdAsync("authtoken"))
            .ReturnsAsync(token);

        m_authenticationTokenRepository
            .Setup(x =>
                x.UpdateIpAddressAsync(
                    "authtoken",
                    "1.1.1.2"))
            .Returns(Task.CompletedTask);
        
        // Test.
        AuthenticationResult result = await m_service.AuthenticateAsync(
            "rawtoken",
            "1.1.1.2");
        
        // Assert.
        SuccessUpdatedAuthenticationResult successResult =
            Assert.IsType<SuccessUpdatedAuthenticationResult>(result);
        UserAuthenticationToken newToken = token with
        {
            IpAddress = "1.1.1.2"
        };
        Assert.Equal(newToken, successResult.Token);
        Assert.Equal("rawtoken", successResult.RawToken);

        m_authenticationTokenRepository.VerifyAll();
    }

    [Fact]
    public async Task DeauthenticateAsync_Test()
    {
        // Setup.
        m_authenticationTokenRepository
            .Setup(x => x.DeleteAsync("authtoken"))
            .Returns(Task.CompletedTask);
        
        // Test.
        await m_service.DeauthenticateAsync("authtoken");
        
        // Assert.
        m_authenticationTokenRepository.VerifyAll();
    }

    [Fact]
    public async Task DeauthenticateAsync_IgnoreErrors()
    {
        // Setup.
        m_authenticationTokenRepository
            .Setup(x => x.DeleteAsync("authtoken"))
            .ThrowsAsync(new Exception());
        
        // Test.
        await m_service.DeauthenticateAsync("authtoken");
        
        // Assert.
        m_authenticationTokenRepository.VerifyAll();
    }

    [Fact]
    public async Task ChangePasswordAsync_Test()
    {
        // Setup.
        m_authenticationRepository
            .Setup(x => x.GetByUserIdAsync("userid"))
            .ReturnsAsync(
                new UserAuth(
                    "userid",
                    "hash",
                    "pads"));

        m_authenticationStringGenerator
            .Setup(x =>
                x.GeneratePasswordHash("password", "pads"))
            .Returns("hash");

        m_authenticationStringGenerator
            .Setup(x => x.GeneratePads())
            .Returns("newpads");

        m_authenticationStringGenerator
            .Setup(x =>
                x.GeneratePasswordHash("newpassword", "newpads"))
            .Returns("newhash");

        m_authenticationRepository
            .Setup(x =>
                x.SetAuthHashPadsAsync(
                    "userid",
                    "newhash",
                    "newpads"))
            .Returns(Task.CompletedTask);
        
        // Test.
        bool result = await m_service.ChangePasswordAsync(
            "userid",
            "password",
            "newpassword");
        
        // Assert.
        Assert.True(result);
        m_authenticationRepository.VerifyAll();
    }

    [Fact]
    public async Task ChangePasswordAsync_InvalidUserId()
    {
        // Setup.
        m_authenticationRepository
            .Setup(x => x.GetByUserIdAsync("userid"))
            .ReturnsAsync(null as UserAuth);
        
        // Test.
        await Assert.ThrowsAsync<InvalidParameterException>(() =>
            m_service.ChangePasswordAsync(
                "userid",
                "password",
                "newpassword"));
    }

    [Fact]
    public async Task ChangePasswordAsync_InvalidOldPassword()
    {
        // Setup.
        m_authenticationRepository
            .Setup(x => x.GetByUserIdAsync("userid"))
            .ReturnsAsync(
                new UserAuth(
                    "userid",
                    "hash",
                    "pads"));

        m_authenticationStringGenerator
            .Setup(x =>
                x.GeneratePasswordHash("password", "pads"))
            .Returns("hashhash");
        
        // Test.
        bool result = await m_service.ChangePasswordAsync(
            "userid",
            "password",
            "newpassword");
        
        // Assert.
        Assert.False(result);
        m_authenticationRepository.VerifyAll();
    }
    
    
}
