using System;
using System.Threading.Tasks;
using home_server.Services.Cameras;
using Moq;
using Serilog;
using Xunit;

namespace home_server.Tests.Services.Cameras;

public class CameraServiceTest
{
    Mock<ICameraRepository> m_cameraRepository;
    Mock<ICameraRecordingService> m_cameraRecordingService;
    Mock<ICameraRecordingArchivalService> m_cameraRecordingArchivalService;
    CameraService m_service;

    public CameraServiceTest()
    {
        m_cameraRepository = new Mock<ICameraRepository>(MockBehavior.Strict);
        m_cameraRecordingService = new Mock<ICameraRecordingService>(MockBehavior.Strict);
        m_cameraRecordingArchivalService = new Mock<ICameraRecordingArchivalService>(MockBehavior.Strict);
        m_service = new CameraService(
            m_cameraRepository.Object,
            m_cameraRecordingService.Object,
            m_cameraRecordingArchivalService.Object,
            Log.Logger);
    }

    [Fact]
    public async Task CreateAsync_Test()
    {
        // Setup.
        MockSequence sequence = new MockSequence();
        
        m_cameraRepository
            .InSequence(sequence)
            .Setup(x => x.IsRtspUrlExistsAsync("rtsp://cam1"))
            .ReturnsAsync(false);

        m_cameraRepository
            .InSequence(sequence)
            .Setup(x => x.AddAsync("cam1", "rtsp://cam1"))
            .ReturnsAsync(
                new Camera(
                    "1",
                    "cam1",
                    "rtsp://cam1"));

        m_cameraRecordingService
            .InSequence(sequence)
            .Setup(x =>
                x.Start(
                    new Camera(
                        "1",
                        "cam1",
                        "rtsp://cam1")));
        
        // Test.
        CreateCameraResult result = await m_service.CreateAsync(
            " cam1 ",
            " rtsp://cam1 ");
        
        // Assert.
        SuccessCreateCameraResult successResult =
            Assert.IsType<SuccessCreateCameraResult>(result);
        Camera camera = new Camera(
            "1",
            "cam1",
            "rtsp://cam1");
        Assert.Equal(camera, successResult.Camera);

        m_cameraRepository.VerifyAll();
        m_cameraRecordingService.VerifyAll();
    }

    [Fact]
    public async Task CreateAsync_ValidateDuplicate()
    {
        // Setup.
        m_cameraRepository
            .Setup(x => x.IsRtspUrlExistsAsync("rtsp://cam1"))
            .ReturnsAsync(true);
        
        // Test.
        CreateCameraResult result = await m_service.CreateAsync(
            "cam1",
            "rtsp://cam1");
        
        // Assert.
        Assert.IsType<DuplicateUrlCreateCameraResult>(result);
    }

    [Fact]
    public async Task UpdateAsync_Test()
    {
        // Setup.
        m_cameraRepository
            .Setup(x => x.GetByIdAsync("1"))
            .ReturnsAsync(
                new Camera(
                    "1",
                    "cam1",
                    "rtsp://cam1"));

        m_cameraRepository
            .Setup(x =>
                x.UpdateAsync(
                    new Camera(
                        "1",
                        "cam11",
                        "rtsp://cam1")))
            .Returns(Task.CompletedTask);
        
        // Test.
        UpdateCameraResult result = await m_service.UpdateAsync(
            new Camera(
                "1",
                "cam11",
                "rtsp://cam1"));
        
        // Assert.
        Assert.IsType<SuccessUpdateCameraResult>(result);
        m_cameraRepository.VerifyAll();
    }

    [Fact]
    public async Task UpdateAsync_UpdateRtspUrl()
    {
        // Setup.
        MockSequence sequence = new MockSequence();
        
        m_cameraRepository
            .InSequence(sequence)
            .Setup(x => x.GetByIdAsync("1"))
            .ReturnsAsync(
                new Camera(
                    "1",
                    "cam1",
                    "rtsp://cam1"));

        m_cameraRepository
            .InSequence(sequence)
            .Setup(x => x.IsRtspUrlExistsAsync("rtsp://cam11"))
            .ReturnsAsync(false);
        
        m_cameraRepository
            .InSequence(sequence)
            .Setup(x =>
                x.UpdateAsync(
                    new Camera(
                        "1",
                        "cam11",
                        "rtsp://cam11")))
            .Returns(Task.CompletedTask);

        m_cameraRecordingService
            .InSequence(sequence)
            .Setup(x => x.StopAsync("1"))
            .Returns(Task.CompletedTask);

        m_cameraRecordingService
            .InSequence(sequence)
            .Setup(x =>
                x.Start(
                    new Camera(
                        "1",
                        "cam11",
                        "rtsp://cam11")));
        
        // Test.
        UpdateCameraResult result = await m_service.UpdateAsync(
            new Camera(
                "1",
                "cam11",
                "rtsp://cam11"));
        
        // Assert.
        Assert.IsType<SuccessUpdateCameraResult>(result);
        m_cameraRepository.VerifyAll();
        m_cameraRecordingService.VerifyAll();
    }

    [Fact]
    public async Task UpdateAsync_ValidateDuplicate()
    {
        // Setup.
        m_cameraRepository
            .Setup(x => x.GetByIdAsync("1"))
            .ReturnsAsync(
                new Camera(
                    "1",
                    "cam1",
                    "rtsp://cam1"));

        m_cameraRepository
            .Setup(x => x.IsRtspUrlExistsAsync("rtsp://cam11"))
            .ReturnsAsync(true);
        
        // Test.
        UpdateCameraResult result = await m_service.UpdateAsync(
            new Camera(
                "1",
                "cam1",
                "rtsp://cam11"));
        
        // Assert.
        Assert.IsType<DuplicateUrlUpdateCameraResult>(result);
        m_cameraRepository.VerifyAll();
    }

    [Fact]
    public async Task DeleteAsync_Test()
    {
        // Setup.
        m_cameraRepository
            .Setup(x => x.GetByIdAsync("1"))
            .ReturnsAsync(
                new Camera(
                    "1",
                    "cam1",
                    "rtsp://cam1"));

        MockSequence sequence = new MockSequence();

        m_cameraRecordingService
            .InSequence(sequence)
            .Setup(x => x.StopAsync("1"))
            .Returns(Task.CompletedTask);

        m_cameraRepository
            .InSequence(sequence)
            .Setup(x => x.DeleteAsync("1"))
            .Returns(Task.CompletedTask);

        m_cameraRecordingArchivalService
            .InSequence(sequence)
            .Setup(x => x.RemoveArchivesAsync("1"))
            .Returns(Task.CompletedTask);
        
        // Test.
        await m_service.DeleteAsync("1");
        
        // Assert.
        m_cameraRepository.VerifyAll();
        m_cameraRecordingService.VerifyAll();
        m_cameraRecordingArchivalService.VerifyAll();
    }

    [Fact]
    public async Task DeleteAsync_IgnoreStopErrors()
    {
        // Setup.
        m_cameraRepository
            .Setup(x => x.GetByIdAsync("1"))
            .ReturnsAsync(
                new Camera(
                    "1",
                    "cam1",
                    "rtsp://cam1"));

        MockSequence sequence = new MockSequence();

        m_cameraRecordingService
            .InSequence(sequence)
            .Setup(x => x.StopAsync("1"))
            .Throws(new InvalidOperationException());

        m_cameraRepository
            .InSequence(sequence)
            .Setup(x => x.DeleteAsync("1"))
            .Returns(Task.CompletedTask);

        m_cameraRecordingArchivalService
            .InSequence(sequence)
            .Setup(x => x.RemoveArchivesAsync("1"))
            .Returns(Task.CompletedTask);
        
        // Test.
        await m_service.DeleteAsync("1");
        
        // Assert.
        m_cameraRepository.VerifyAll();
        m_cameraRecordingService.VerifyAll();
        m_cameraRecordingArchivalService.VerifyAll();
    }

    [Fact]
    public async Task StartAllRecordingAsync_Test()
    {
        // Setup.
        m_cameraRepository
            .Setup(x => x.ListAllAsync())
            .ReturnsAsync(new[]
            {
                new Camera("1", "cam1", "rtsp://cam1"),
                new Camera("2", "cam2", "rtsp://cam2")
            });

        MockSequence sequence = new MockSequence();

        m_cameraRecordingService
            .InSequence(sequence)
            .Setup(x =>
                x.Start(
                    new Camera(
                        "1",
                        "cam1",
                        "rtsp://cam1")));
        
        m_cameraRecordingService
            .InSequence(sequence)
            .Setup(x =>
                x.Start(
                    new Camera(
                        "2",
                        "cam2",
                        "rtsp://cam2")));
        
        // Test.
        await m_service.StartAllRecordingAsync();
        
        // Assert.
        m_cameraRecordingService.VerifyAll();
    }
}
