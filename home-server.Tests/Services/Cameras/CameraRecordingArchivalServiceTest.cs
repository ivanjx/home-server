using System;
using System.Threading.Tasks;
using home_server.Services;
using home_server.Services.Cameras;
using Moq;
using Serilog;
using Xunit;

namespace home_server.Tests.Services.Cameras;

public class CameraRecordingArchivalServiceTest
{
    Mock<ICameraRepository> m_cameraRepository;
    Mock<IConfigurationService> m_configurationService;
    Mock<ITimeService> m_timeService;
    Mock<ICameraRecordingArchivalSourceService> m_sourceService;
    CameraRecordingArchivalService m_service;

    public CameraRecordingArchivalServiceTest()
    {
        m_cameraRepository = new Mock<ICameraRepository>(MockBehavior.Strict);
        m_configurationService = new Mock<IConfigurationService>(MockBehavior.Strict);
        m_timeService = new Mock<ITimeService>(MockBehavior.Strict);
        m_sourceService = new Mock<ICameraRecordingArchivalSourceService>(MockBehavior.Strict);
        m_service = new CameraRecordingArchivalService(
            m_cameraRepository.Object,
            m_configurationService.Object,
            m_timeService.Object,
            m_sourceService.Object,
            Log.Logger);
    }

    [Fact]
    public async Task CreateArchivesAsync_Test()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        
        m_cameraRepository
            .Setup(x => x.ListAllAsync())
            .ReturnsAsync(new[]
            {
                new Camera(
                    "123",
                    "cam1",
                    "rtsp1")
            });

        DateTime now = DateTime.Parse("2022-04-23 14:00:15")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        m_timeService
            .Setup(x => x.GetLocalNow())
            .Returns(now - TimeSpan.FromHours(1)); // For archive name.

        m_sourceService
            .Setup(x => x.Exists("/data/camera-archive/123/index.m3u8"))
            .Returns(true);
        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/index.m3u8"))
            .Returns(now.AddMinutes(-1));
        m_sourceService
            .Setup(x => x.ReadAsync("/data/camera-archive/123/index.m3u8"))
            .ReturnsAsync(
                "line1" + Environment.NewLine +
                "#EXT-X-PROGRAM-DATE-TIME" + Environment.NewLine +
                "line2" + Environment.NewLine +
                "#EXT-X-DISCONTINUITY" + Environment.NewLine +
                "line3" + Environment.NewLine);

        m_sourceService
            .Setup(x =>
                x.WriteAsync(
                    "/data/camera-archive/123/lists/20220423-13.m3u8",
                    "line1" + Environment.NewLine +
                    "#EXT-X-PROGRAM-DATE-TIME" + Environment.NewLine +
                    "line2" + Environment.NewLine +
                    "#EXT-X-DISCONTINUITY" + Environment.NewLine +
                    "line3" + Environment.NewLine +
                    "#EXT-X-ENDLIST" + Environment.NewLine))
            .Returns(Task.CompletedTask);
        
        // Test.
        await m_service.CreateArchivesAsync();
        
        // Assert.
        m_sourceService.VerifyAll();
    }

    [Fact]
    public async Task CreateArchivesAsync_FineTuneTime()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        
        m_cameraRepository
            .Setup(x => x.ListAllAsync())
            .ReturnsAsync(new[]
            {
                new Camera(
                    "123",
                    "cam1",
                    "rtsp1")
            });

        DateTime now = DateTime.Parse("2022-04-23 14:00:12")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        m_timeService
            .Setup(x => x.GetLocalNow())
            .Returns(now - TimeSpan.FromHours(1)); // For archive name.

        m_sourceService
            .Setup(x => x.Exists("/data/camera-archive/123/index.m3u8"))
            .Returns(true);
        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/index.m3u8"))
            .Returns(now.AddMinutes(-1));
        m_sourceService
            .Setup(x => x.ReadAsync("/data/camera-archive/123/index.m3u8"))
            .ReturnsAsync(
                "line1" + Environment.NewLine +
                "#EXT-X-PROGRAM-DATE-TIME" + Environment.NewLine +
                "line2" + Environment.NewLine +
                "#EXT-X-DISCONTINUITY" + Environment.NewLine +
                "line3" + Environment.NewLine);

        m_sourceService
            .Setup(x =>
                x.WriteAsync(
                    "/data/camera-archive/123/lists/20220423-12.m3u8", // Should still be 12.m3u8 even though it is 13:00:12 now.
                    "line1" + Environment.NewLine +
                    "#EXT-X-PROGRAM-DATE-TIME" + Environment.NewLine +
                    "line2" + Environment.NewLine +
                    "#EXT-X-DISCONTINUITY" + Environment.NewLine +
                    "line3" + Environment.NewLine +
                    "#EXT-X-ENDLIST" + Environment.NewLine))
            .Returns(Task.CompletedTask);
        
        // Test.
        await m_service.CreateArchivesAsync();
        
        // Assert.
        m_sourceService.VerifyAll();
    }

    [Fact]
    public async Task CreateArchivesAsync_NoEndlistAdjustment()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        
        m_cameraRepository
            .Setup(x => x.ListAllAsync())
            .ReturnsAsync(new[]
            {
                new Camera(
                    "123",
                    "cam1",
                    "rtsp1")
            });

        DateTime now = DateTime.Parse("2022-04-23 14:18:30")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        m_timeService
            .Setup(x => x.GetLocalNow())
            .Returns(now - TimeSpan.FromHours(1)); // For archive name.

        m_sourceService
            .Setup(x => x.Exists("/data/camera-archive/123/index.m3u8"))
            .Returns(true);
        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/index.m3u8"))
            .Returns(now.AddMinutes(-1));
        m_sourceService
            .Setup(x => x.ReadAsync("/data/camera-archive/123/index.m3u8"))
            .ReturnsAsync(
                "line1" + Environment.NewLine +
                "#EXT-X-PROGRAM-DATE-TIME" + Environment.NewLine +
                "line2" + Environment.NewLine +
                "#EXT-X-DISCONTINUITY" + Environment.NewLine +
                "line3" + Environment.NewLine +
                "#EXT-X-ENDLIST" + Environment.NewLine);

        m_sourceService
            .Setup(x =>
                x.WriteAsync(
                    "/data/camera-archive/123/lists/20220423-13.m3u8",
                    "line1" + Environment.NewLine +
                    "#EXT-X-PROGRAM-DATE-TIME" + Environment.NewLine +
                    "line2" + Environment.NewLine +
                    "#EXT-X-DISCONTINUITY" + Environment.NewLine +
                    "line3" + Environment.NewLine +
                    "#EXT-X-ENDLIST" + Environment.NewLine))
            .Returns(Task.CompletedTask);
        
        // Test.
        await m_service.CreateArchivesAsync();
        
        // Assert.
        m_sourceService.VerifyAll();
    }

    [Fact]
    public async Task CreateArchivesAsync_IgnoreNonExistentIndex()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        
        m_cameraRepository
            .Setup(x => x.ListAllAsync())
            .ReturnsAsync(new[]
            {
                new Camera(
                    "123",
                    "cam1",
                    "rtsp1")
            });

        DateTime now = DateTime.Parse("2022-04-23 14:18:30")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        m_timeService
            .Setup(x => x.GetLocalNow())
            .Returns(now - TimeSpan.FromHours(1)); // For archive name.

        m_sourceService
            .Setup(x => x.Exists("/data/camera-archive/123/index.m3u8"))
            .Returns(false);
        
        // Test.
        await m_service.CreateArchivesAsync();
        
        // Assert.
        m_sourceService.VerifyAll();
    }

    [Fact]
    public async Task CreateArchivesAsync_IgnoreOldArchive()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        
        m_cameraRepository
            .Setup(x => x.ListAllAsync())
            .ReturnsAsync(new[]
            {
                new Camera(
                    "123",
                    "cam1",
                    "rtsp1")
            });

        DateTime now = DateTime.Parse("2022-04-23 14:18:30")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);
        m_timeService
            .Setup(x => x.GetLocalNow())
            .Returns(now - TimeSpan.FromHours(1)); // For archive name.

        m_sourceService
            .Setup(x => x.Exists("/data/camera-archive/123/index.m3u8"))
            .Returns(true);
        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/index.m3u8"))
            .Returns(now.AddMinutes(-1).AddSeconds(-1));
        
        // Test.
        await m_service.CreateArchivesAsync();
        
        // Assert.
        m_sourceService.VerifyAll();
    }

    [Fact]
    public async Task CleanArchivesAsync_Test()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        m_configurationService
            .SetupGet(x => x.CameraArchivalDays)
            .Returns(3);

        DateTime now = DateTime.Parse("2022-04-23 14:18:30")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);

        m_cameraRepository
            .Setup(x => x.ListAllAsync())
            .ReturnsAsync(new[]
            {
                new Camera(
                    "123",
                    "cam1",
                    "rtsp1")
            });

        m_sourceService
            .Setup(x => x.List("/data/camera-archive/123/lists"))
            .Returns(new[]
            {
                "/data/camera-archive/123/lists/20220420-12.m3u8",
                "/data/camera-archive/123/lists/20220420-13.m3u8",
                "/data/camera-archive/123/lists/20220420-14.m3u8",
            });

        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/lists/20220420-12.m3u8"))
            .Returns(now.AddDays(-3).AddHours(-3)); // Deleted.
        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/lists/20220420-13.m3u8"))
            .Returns(now.AddDays(-3).AddHours(-2)); // Deleted
        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/lists/20220420-14.m3u8"))
            .Returns(now.AddDays(-3).AddHours(-1)); // Not deleted.

        m_sourceService
            .Setup(x => x.ReadAsync("/data/camera-archive/123/lists/20220420-12.m3u8"))
            .ReturnsAsync(
                "#EXTM3U" + Environment.NewLine +
                "#EXT-X-VERSION:3" + Environment.NewLine +
                "#EXTINF:4.019078," + Environment.NewLine +
                "#EXT-X-PROGRAM-DATE-TIME:1970-01-01T07:00:10.036+0000" + Environment.NewLine +
                "/api/camera/archive/621e5d38fa39c42824a36a9a/0.ts" + Environment.NewLine +
                "#EXTINF:6.016956," + Environment.NewLine +
                "#EXT-X-PROGRAM-DATE-TIME:1970-01-01T07:00:10.036+0000" + Environment.NewLine +
                "/api/camera/archive/621e5d38fa39c42824a36a9a/1.ts" + Environment.NewLine +
                "#EXT-X-DISCONTINUITY" + Environment.NewLine +
                "#EXT-X-ENDLIST" + Environment.NewLine);
        m_sourceService
            .Setup(x => x.ReadAsync("/data/camera-archive/123/lists/20220420-13.m3u8"))
            .ReturnsAsync(
                "#EXTM3U" + Environment.NewLine +
                "#EXT-X-VERSION:3" + Environment.NewLine +
                "#EXTINF:4.019078," + Environment.NewLine +
                "/api/camera/archive/621e5d38fa39c42824a36a9a/2.ts" + Environment.NewLine +
                "/api/camera/archive/621e5d38fa39c42824a36a9a/3.ts" + Environment.NewLine +
                "#EXT-X-ENDLIST" + Environment.NewLine);

        m_sourceService
            .Setup(x => x.Delete("/data/camera-archive/123/0.ts"));
        m_sourceService
            .Setup(x => x.Delete("/data/camera-archive/123/1.ts"));
        m_sourceService
            .Setup(x => x.Delete("/data/camera-archive/123/2.ts"));
        m_sourceService
            .Setup(x => x.Delete("/data/camera-archive/123/3.ts"));
        
        m_sourceService
            .Setup(x => x.Delete("/data/camera-archive/123/lists/20220420-12.m3u8"));
        m_sourceService
            .Setup(x => x.Delete("/data/camera-archive/123/lists/20220420-13.m3u8"));
        
        // Test.
        await m_service.CleanArchivesAsync();
        
        // Assert.
        m_sourceService.VerifyAll();
    }

    [Fact]
    public async Task ListArchivesAsync_Test()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        m_configurationService
            .SetupGet(x => x.CameraArchivalDays)
            .Returns(3);

        DateTime now = DateTime.Parse("2022-04-23 12:00:00")
            .ToLocalTime()
            .ToUniversalTime();
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);

        m_sourceService
            .Setup(x => x.List("/data/camera-archive/123/lists"))
            .Returns(new[]
            {
                "/data/camera-archive/123/lists/0.m3u8",
                "/data/camera-archive/123/lists/1.m3u8",
                "/data/camera-archive/123/lists/2.m3u8",
                "/data/camera-archive/123/lists/3.m3u8"
            });

        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/lists/0.m3u8"))
            .Returns(now.AddDays(-3).AddHours(-1));
        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/lists/1.m3u8"))
            .Returns(now.AddDays(-3));
        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/lists/2.m3u8"))
            .Returns(now.AddDays(-2).AddHours(-23));
        m_sourceService
            .Setup(x => x.GetModifyTime("/data/camera-archive/123/lists/3.m3u8"))
            .Returns(now.AddDays(-2).AddHours(-22));
        
        // Test.
        string[] result = await m_service.ListArchivesAsync("123");
        
        // Assert.
        string[] archives = new[]
        {
            "3", "2", "1"
        };
        Assert.Equal(archives, result);
    }

    [Fact]
    public async Task RemoveArchivesAsync_Test()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");

        m_sourceService
            .Setup(x => x.Delete("/data/camera-archive/123"));
        
        // Test.
        await m_service.RemoveArchivesAsync("123");
        
        // Assert.
        m_sourceService.VerifyAll();
    }

    [Fact]
    public async Task GetArchiveIndexAsync_Test()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");

        m_sourceService
            .Setup(x => x.Exists("/data/camera-archive/123/lists/1.m3u8"))
            .Returns(true);
        m_sourceService
            .Setup(x => x.ReadAsync("/data/camera-archive/123/lists/1.m3u8"))
            .ReturnsAsync("abcde");
        
        // Test.
        string? result = await m_service.GetArchiveIndexAsync("123", "1");
        
        // Assert.
        Assert.Equal("abcde", result);
        m_sourceService.VerifyAll();
    }

    [Fact]
    public async Task GetArchiveIndexAsync_DoesNotExist()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");

        m_sourceService
            .Setup(x => x.Exists("/data/camera-archive/123/lists/1.m3u8"))
            .Returns(false);
        
        // Test.
        string? result = await m_service.GetArchiveIndexAsync("123", "1");
        
        // Assert.
        Assert.Null(result);
        m_sourceService.VerifyAll();
    }

    [Fact]
    public void GetArchiveChunkPath_Test()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");

        m_sourceService
            .Setup(x => x.Exists("/data/camera-archive/123/1.ts"))
            .Returns(true);
        
        // Test.
        string? result = m_service.GetArchiveChunkPath("123", "1.ts");
        
        // Assert.
        Assert.Equal("/data/camera-archive/123/1.ts", result);
    }

    [Fact]
    public void GetArchiveChunkPath_DoesNotExist()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");

        m_sourceService
            .Setup(x => x.Exists("/data/camera-archive/123/1.ts"))
            .Returns(false);
        
        // Test.
        string? result = m_service.GetArchiveChunkPath("123", "1.ts");
        
        // Assert.
        Assert.Null(result);
    }
}
