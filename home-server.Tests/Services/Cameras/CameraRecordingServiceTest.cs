using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using home_server.Services;
using home_server.Services.Cameras;
using Moq;
using Serilog;
using Xunit;

namespace home_server.Tests.Services.Cameras;

public class CameraRecordingServiceTest
{
    CameraRecordingState m_state;
    Mock<ICameraRecordingProcessService> m_processService;
    CameraRecordingService m_service;

    public CameraRecordingServiceTest()
    {
        m_state = new CameraRecordingState();
        m_processService = new Mock<ICameraRecordingProcessService>(MockBehavior.Strict);
        m_service = new CameraRecordingService(
            m_state,
            m_processService.Object,
            Log.Logger);
    }

    [Fact]
    public void Start_Test()
    {
        // Setup.
        Camera camera = new Camera(
            "123",
            "cam1",
            "rtsp1");
        Action<CameraRecordingSegment> callback = _ => { };
        m_processService
            .Setup(x =>
                x.ProcessAsync(
                    camera,
                    It.IsAny<Action<CameraRecordingSegment>>(),
                    It.IsAny<CancellationToken>()))
            .Callback<Camera, Action<CameraRecordingSegment>, CancellationToken>((_, action, _) =>
            {
                callback = action;
            })
            .Returns(Task.CompletedTask);
        
        // Test.
        m_service.Start(camera);
        
        // Assert.
        m_processService.VerifyAll();
        Assert.Contains("123", m_state.Recordings.Keys);
        Assert.Equal(m_state.Recordings["123"].HandleNewSegment, callback);
    }

    [Fact]
    public void Start_ValidateDuplicate()
    {
        // Setup.
        Camera camera = new Camera(
            "123",
            "cam1",
            "rtsp1");
        m_state.Recordings.Add(
            "123",
            new CameraRecording());
        
        // Test.
        Assert.Throws<Exception>(() =>
            m_service.Start(camera));
    }

    [Fact]
    public async Task StopAsync_Test()
    {
        // Setup.
        CameraRecording rec1 = new CameraRecording();
        CameraRecording rec2 = new CameraRecording();
        m_state.Recordings.Add("123", rec1);
        m_state.Recordings.Add("1234", rec2);
        
        // Test.
        await m_service.StopAsync("123");
        
        // Assert.
        Assert.Throws<ObjectDisposedException>(() =>
            rec1.CancellationToken.IsCancellationRequested);
        Assert.False(rec2.CancellationToken.IsCancellationRequested);
        Assert.Single(m_state.Recordings);
        Assert.Equal("1234", m_state.Recordings.Keys.First());
    }

    [Fact]
    public async Task StopAllAsync_Test()
    {
        // Setup.
        CameraRecording rec1 = new CameraRecording();
        CameraRecording rec2 = new CameraRecording();
        m_state.Recordings.Add("123", rec1);
        m_state.Recordings.Add("1234", rec2);
        
        // Test.
        await m_service.StopAllAsync();
        
        // Assert.
        Assert.Throws<ObjectDisposedException>(() =>
            rec1.CancellationToken.IsCancellationRequested);
        Assert.Throws<ObjectDisposedException>(() =>
            rec2.CancellationToken.IsCancellationRequested);
        Assert.Empty(m_state.Recordings);
    }

    [Fact]
    public async Task ServeAsync_Test()
    {
        // Setup.
        CameraRecording recording = new CameraRecording();
        recording.Task = Task.Delay(1000);
        m_state.Recordings.Add("123", recording);

        MockSequence sequence = new MockSequence();
        CancellationTokenSource cts = new CancellationTokenSource();
        Mock<IWebSocket> socket = new Mock<IWebSocket>(MockBehavior.Strict);

        socket
            .SetupGet(x => x.ConnectionId)
            .Returns("123");

        socket
            .InSequence(sequence)
            .SetupGet(x => x.IsOpen)
            .Returns(true);

        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("CODEC"),
                    It.IsAny<CancellationToken>()));
        
        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("MOOF"),
                    It.IsAny<CancellationToken>()));
        
        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("DATA1"),
                    It.IsAny<CancellationToken>()));
        
        socket
            .InSequence(sequence)
            .SetupGet(x => x.IsOpen)
            .Returns(true);
        
        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("DATA2"),
                    It.IsAny<CancellationToken>()))
            .Callback(() => cts.Cancel()); // Stop test.

        // Test.
        Task t = m_service.ServeAsync(
            "123",
            socket.Object,
            cts.Token);
        recording.HandleNewSegment(
            new CameraRecordingSegment(
                "CODEC",
                Encoding.ASCII.GetBytes("MOOF"),
                Encoding.ASCII.GetBytes("DATA1")));
        recording.HandleNewSegment(
            new CameraRecordingSegment(
                "CODEC",
                Encoding.ASCII.GetBytes("MOOF"),
                Encoding.ASCII.GetBytes("DATA2")));
        recording.HandleNewSegment(
            new CameraRecordingSegment(
                "CODEC",
                Encoding.ASCII.GetBytes("MOOF"),
                Encoding.ASCII.GetBytes("DATA3"))); // Will not be sent.
        await t;
        
        // Assert.
        socket.VerifyAll();
    }

    [Fact]
    public async Task ServeAsync_SocketClosed()
    {
        // Setup.
        CameraRecording recording = new CameraRecording();
        recording.Task = Task.Delay(1000);
        m_state.Recordings.Add("123", recording);

        MockSequence sequence = new MockSequence();
        Mock<IWebSocket> socket = new Mock<IWebSocket>(MockBehavior.Strict);

        socket
            .SetupGet(x => x.ConnectionId)
            .Returns("123");

        socket
            .InSequence(sequence)
            .SetupGet(x => x.IsOpen)
            .Returns(true);

        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("CODEC"),
                    It.IsAny<CancellationToken>()));
        
        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("MOOF"),
                    It.IsAny<CancellationToken>()));
        
        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("DATA1"),
                    It.IsAny<CancellationToken>()));
        
        socket
            .InSequence(sequence)
            .SetupGet(x => x.IsOpen)
            .Returns(true);

        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("DATA2"),
                    It.IsAny<CancellationToken>()));

        socket
            .InSequence(sequence)
            .SetupGet(x => x.IsOpen)
            .Returns(false);

        // Test.
        Task t = m_service.ServeAsync(
            "123",
            socket.Object,
            default);
        recording.HandleNewSegment(
            new CameraRecordingSegment(
                "CODEC",
                Encoding.ASCII.GetBytes("MOOF"),
                Encoding.ASCII.GetBytes("DATA1")));
        recording.HandleNewSegment(
            new CameraRecordingSegment(
                "CODEC",
                Encoding.ASCII.GetBytes("MOOF"),
                Encoding.ASCII.GetBytes("DATA2")));
        recording.HandleNewSegment(
            new CameraRecordingSegment(
                "CODEC",
                Encoding.ASCII.GetBytes("MOOF"),
                Encoding.ASCII.GetBytes("DATA3"))); // Will not be sent.
        await t;
        
        // Assert.
        socket.VerifyAll();
    }

    [Fact]
    public async Task ServeAsync_RecordingStopped()
    {
        // Setup.
        CameraRecording recording = new CameraRecording();
        recording.Task = Task.Delay(1000);
        m_state.Recordings.Add("123", recording);

        MockSequence sequence = new MockSequence();
        CancellationTokenSource cts = new CancellationTokenSource();
        Mock<IWebSocket> socket = new Mock<IWebSocket>(MockBehavior.Strict);

        socket
            .SetupGet(x => x.ConnectionId)
            .Returns("123");

        socket
            .InSequence(sequence)
            .SetupGet(x => x.IsOpen)
            .Returns(true);

        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("CODEC"),
                    It.IsAny<CancellationToken>()));
        
        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("MOOF"),
                    It.IsAny<CancellationToken>()));
        
        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("DATA1"),
                    It.IsAny<CancellationToken>()));
        
        socket
            .InSequence(sequence)
            .SetupGet(x => x.IsOpen)
            .Returns(true);
        
        socket
            .InSequence(sequence)
            .Setup(x =>
                x.SendAsync(
                    Encoding.ASCII.GetBytes("DATA2"),
                    It.IsAny<CancellationToken>()))
            .Callback(() => recording.Task = Task.CompletedTask); // Stop test.

        // Test.
        Task t = m_service.ServeAsync(
            "123",
            socket.Object,
            cts.Token);
        recording.HandleNewSegment(
            new CameraRecordingSegment(
                "CODEC",
                Encoding.ASCII.GetBytes("MOOF"),
                Encoding.ASCII.GetBytes("DATA1")));
        recording.HandleNewSegment(
            new CameraRecordingSegment(
                "CODEC",
                Encoding.ASCII.GetBytes("MOOF"),
                Encoding.ASCII.GetBytes("DATA2")));
        recording.HandleNewSegment(
            new CameraRecordingSegment(
                "CODEC",
                Encoding.ASCII.GetBytes("MOOF"),
                Encoding.ASCII.GetBytes("DATA3"))); // Will not be sent.
        await t;
        
        // Assert.
        socket.VerifyAll();
    }
}
