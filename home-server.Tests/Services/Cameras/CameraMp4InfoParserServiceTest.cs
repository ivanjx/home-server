using System;
using home_server.Services.Cameras;
using Xunit;

namespace home_server.Tests.Services.Cameras;

public class CameraMp4InfoParserServiceTest
{
    CameraMp4InfoParserService m_service;

    public CameraMp4InfoParserServiceTest()
    {
        m_service = new CameraMp4InfoParserService();
    }

    [Fact]
    public void Parse_Test()
    {
      // Test.
      string[] result = m_service.Parse(SAMPLE_OUTPUT);

      // Assert.
      Assert.Equal(2, result.Length);
      Assert.Equal("avc1.640029", result[0]);
      Assert.Equal("mp4a.40.2", result[1]);
    }
    
    const string SAMPLE_OUTPUT = @"
File:
  major brand:      iso5
  minor version:    200
  compatible brand: iso5
  compatible brand: iso6
  compatible brand: mp41
  fast start:       yes

Movie:
  duration:   0 (media timescale units)
  duration:   0 (ms)
  time scale: 1000
  fragments:  yes

Found 2 Tracks
Track 1:
  flags:        3 ENABLED IN-MOVIE
  id:           1
  type:         Video
  duration: 0 ms
  language: und
  media:
    sample count: 0
    timescale:    90000
    duration:     0 (media timescale units)
    duration:     0 (ms)
    bitrate (computed): 0.000 Kbps
    sample count with fragments: 0
    duration with fragments:     0
    duration with fragments:     0 (ms)
  display width:  1920.000000
  display height: 1080.000000
  Sample Description 0
    Coding:       avc1 (H.264)
    Codec String: avc1.640029
    AVC Profile:          100 (High)
    AVC Profile Compat:   0
    AVC Level:            41
    AVC NALU Length Size: 4
    AVC SPS: [67640029ac1ad00f0044fcb3701010140000030004000003007a3c508a80]
    AVC PPS: [68ee04492240]
    Width:       1920
    Height:      1080
    Depth:       24
Track 2:
  flags:        3 ENABLED IN-MOVIE
  id:           2
  type:         Audio
  duration: 0 ms
  language: und
  media:
    sample count: 0
    timescale:    8000
    duration:     0 (media timescale units)
    duration:     0 (ms)
    bitrate (computed): 0.000 Kbps
    sample count with fragments: 0
    duration with fragments:     0
    duration with fragments:     0 (ms)
  Sample Description 0
    Coding:       mp4a (MPEG-4 Audio)
    Codec String: mp4a.40.2
    Stream Type: Audio
    Object Type: MPEG-4 Audio
    Max Bitrate: 48000
    Avg Bitrate: 48000
    Buffer Size: 0
    MPEG-4 Audio Object Type: 2 (AAC Low Complexity)
    MPEG-4 Audio Decoder Config:
      Sampling Frequency: 8000
      Channels: 1
      Extension:
        Object Type: Spectral Band Replication
        SBR Present: no
        PS Present:  no
        Sampling Frequency: 0
    Sample Rate: 8000
    Sample Size: 16
    Channels:    2
";
}
