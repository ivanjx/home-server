using System;
using System.Threading;
using System.Threading.Tasks;
using home_server.Services;
using home_server.Services.Cameras;
using Moq;
using Serilog;
using Xunit;

namespace home_server.Tests.Services.Cameras;

public class CameraRecordingProcessServiceTest
{
    Mock<IConfigurationService> m_configurationService;
    Mock<ICameraRecordingProcessSourceService> m_sourceService;
    Mock<IFfmpegProcessService> m_ffmpegProcessService;
    Mock<IMp4InfoService> m_mp4InfoService;
    Mock<ICameraMp4InfoParserService> m_mp4InfoParserService;
    Mock<ITimeService> m_timeService;
    CameraRecordingProcessService m_service;
    Mock<IFfmpegProcess> m_ffmpegProcess;

    public CameraRecordingProcessServiceTest()
    {
        m_configurationService = new Mock<IConfigurationService>(MockBehavior.Strict);
        m_sourceService = new Mock<ICameraRecordingProcessSourceService>(MockBehavior.Strict);
        m_ffmpegProcessService = new Mock<IFfmpegProcessService>(MockBehavior.Strict);
        m_mp4InfoService = new Mock<IMp4InfoService>(MockBehavior.Strict);
        m_mp4InfoParserService = new Mock<ICameraMp4InfoParserService>(MockBehavior.Strict);
        m_timeService = new Mock<ITimeService>(MockBehavior.Strict);
        m_service = new CameraRecordingProcessService(
            m_configurationService.Object,
            m_sourceService.Object,
            m_ffmpegProcessService.Object,
            m_mp4InfoService.Object,
            m_mp4InfoParserService.Object,
            m_timeService.Object,
            Log.Logger);
        m_ffmpegProcess = new Mock<IFfmpegProcess>(MockBehavior.Strict);
    }

    [Fact]
    public async Task ProcessAsync_Test()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        m_configurationService
            .SetupGet(x => x.RtspListenMinPort)
            .Returns(2000);
        m_configurationService
            .SetupGet(x => x.RtspListenMaxPort)
            .Returns(2010);
        m_configurationService
            .SetupGet(x => x.BasePath)
            .Returns("/api");
        
        MockSequence sequence = new MockSequence();
        
        m_sourceService
            .InSequence(sequence)
            .Setup(x => x.CreateDirectory("/data/camera-archive/123"));

        m_ffmpegProcessService
            .InSequence(sequence)
            .Setup(x => x.Start(
                "-hide_banner " +
                "-loglevel warning " +
                "-fflags +genpts " +
                "-i rtspurl " +
                "-min_port 2000 " +
                "-max_port 2010 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f hls " +
                "-hls_time 10 " +
                "-hls_segment_filename /data/camera-archive/123/%d.ts " +
                "-hls_segment_type mpegts " +
                "-hls_list_size 361 " +
                "-hls_flags append_list " +
                "-hls_base_url /api/camera/archive/123/chunk/ " +
                "/data/camera-archive/123/index.m3u8 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f mp4 " +
                "-movflags +frag_keyframe+empty_moov+default_base_moof " +
                "-frag_duration 100000 " +
                "pipe:1"))
            .Returns(m_ffmpegProcess.Object);

        byte[] headerBuff = new byte[1300];
        Random.Shared.NextBytes(headerBuff);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff.CopyTo(buff);
            })
            .ReturnsAsync(headerBuff.Length);

        m_mp4InfoService
            .InSequence(sequence)
            .Setup(x => x.GetInfoAsync(headerBuff))
            .ReturnsAsync("MP4");

        m_mp4InfoParserService
            .InSequence(sequence)
            .Setup(x => x.Parse("MP4"))
            .Returns(new[]
            {
                "avc1.640029",
                "mp4a.40.2"
            });

        byte[] dataBuff = new byte[100];
        Random.Shared.NextBytes(dataBuff);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                dataBuff.CopyTo(buff);
            })
            .ReturnsAsync(dataBuff.Length);

        CancellationTokenSource cts = new CancellationTokenSource();
        Mock<Action<CameraRecordingSegment>> callback = new Mock<Action<CameraRecordingSegment>>(MockBehavior.Strict);
        callback
            .InSequence(sequence)
            .Setup(x => x.Invoke(It.IsAny<CameraRecordingSegment>()))
            .Callback<CameraRecordingSegment>(segment =>
            {
                Assert.Equal(
                    "video/mp4; codecs=\"avc1.640029,mp4a.40.2\"",
                    segment.Codec);
                Assert.Equal(headerBuff, segment.Header);
                Assert.Equal(dataBuff, segment.Data);
                
                cts.Cancel(); // Stop the test.
            });

        m_ffmpegProcess
            .InSequence(sequence)
            .SetupGet(x => x.IsRunning)
            .Returns(true);
        
        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.StopAsync())
            .Returns(Task.CompletedTask);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.Dispose());

        // Test.
        Camera camera = new Camera(
            "123",
            "camera",
            "rtspurl");
        await m_service.ProcessAsync(
            camera,
            callback.Object,
            cts.Token);

        // Assert.
        m_sourceService.VerifyAll();
        m_ffmpegProcessService.VerifyAll();
        m_ffmpegProcess.VerifyAll();
        callback.VerifyAll();
    }

    [Fact]
    public async Task ProcessAsync_ReadMoreHeader()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        m_configurationService
            .SetupGet(x => x.RtspListenMinPort)
            .Returns(2000);
        m_configurationService
            .SetupGet(x => x.RtspListenMaxPort)
            .Returns(2010);
        m_configurationService
            .SetupGet(x => x.BasePath)
            .Returns("/api");
        
        MockSequence sequence = new MockSequence();
        
        m_sourceService
            .InSequence(sequence)
            .Setup(x => x.CreateDirectory("/data/camera-archive/123"));

        m_ffmpegProcessService
            .InSequence(sequence)
            .Setup(x => x.Start(
                "-hide_banner " +
                "-loglevel warning " +
                "-fflags +genpts " +
                "-i rtspurl " +
                "-min_port 2000 " +
                "-max_port 2010 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f hls " +
                "-hls_time 10 " +
                "-hls_segment_filename /data/camera-archive/123/%d.ts " +
                "-hls_segment_type mpegts " +
                "-hls_list_size 361 " +
                "-hls_flags append_list " +
                "-hls_base_url /api/camera/archive/123/chunk/ " +
                "/data/camera-archive/123/index.m3u8 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f mp4 " +
                "-movflags +frag_keyframe+empty_moov+default_base_moof " +
                "-frag_duration 100000 " +
                "pipe:1"))
            .Returns(m_ffmpegProcess.Object);

        byte[] headerBuff = new byte[1300];
        Random.Shared.NextBytes(headerBuff);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff.AsMemory().Slice(0, 90).CopyTo(buff);
            })
            .ReturnsAsync(90);
        
        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff.AsMemory().Slice(90).CopyTo(buff);
            })
            .ReturnsAsync(headerBuff.Length - 90);

        m_mp4InfoService
            .InSequence(sequence)
            .Setup(x => x.GetInfoAsync(headerBuff))
            .ReturnsAsync("MP4");

        m_mp4InfoParserService
            .InSequence(sequence)
            .Setup(x => x.Parse("MP4"))
            .Returns(new[]
            {
                "avc1.640029",
                "mp4a.40.2"
            });

        byte[] dataBuff = new byte[100];
        Random.Shared.NextBytes(dataBuff);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                dataBuff.CopyTo(buff);
            })
            .ReturnsAsync(dataBuff.Length);

        CancellationTokenSource cts = new CancellationTokenSource();
        Mock<Action<CameraRecordingSegment>> callback = new Mock<Action<CameraRecordingSegment>>(MockBehavior.Strict);
        callback
            .InSequence(sequence)
            .Setup(x => x.Invoke(It.IsAny<CameraRecordingSegment>()))
            .Callback<CameraRecordingSegment>(segment =>
            {
                Assert.Equal(
                    "video/mp4; codecs=\"avc1.640029,mp4a.40.2\"",
                    segment.Codec);
                Assert.Equal(headerBuff, segment.Header);
                Assert.Equal(dataBuff, segment.Data);
                
                cts.Cancel(); // Stop the test.
            });

        m_ffmpegProcess
            .InSequence(sequence)
            .SetupGet(x => x.IsRunning)
            .Returns(true);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.StopAsync())
            .Returns(Task.CompletedTask);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.Dispose());

        // Test.
        Camera camera = new Camera(
            "123",
            "camera",
            "rtspurl");
        await m_service.ProcessAsync(
            camera,
            callback.Object,
            cts.Token);

        // Assert.
        m_sourceService.VerifyAll();
        m_ffmpegProcessService.VerifyAll();
        m_ffmpegProcess.VerifyAll();
        callback.VerifyAll();
    }
    
    [Fact]
    public async Task ProcessAsync_NoMoreHeader()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        m_configurationService
            .SetupGet(x => x.RtspListenMinPort)
            .Returns(2000);
        m_configurationService
            .SetupGet(x => x.RtspListenMaxPort)
            .Returns(2010);
        m_configurationService
            .SetupGet(x => x.BasePath)
            .Returns("/api");
        
        MockSequence sequence = new MockSequence();
        
        m_sourceService
            .InSequence(sequence)
            .Setup(x => x.CreateDirectory("/data/camera-archive/123"));

        m_ffmpegProcessService
            .InSequence(sequence)
            .Setup(x => x.Start(
                "-hide_banner " +
                "-loglevel warning " +
                "-fflags +genpts " +
                "-i rtspurl " +
                "-min_port 2000 " +
                "-max_port 2010 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f hls " +
                "-hls_time 10 " +
                "-hls_segment_filename /data/camera-archive/123/%d.ts " +
                "-hls_segment_type mpegts " +
                "-hls_list_size 361 " +
                "-hls_flags append_list " +
                "-hls_base_url /api/camera/archive/123/chunk/ " +
                "/data/camera-archive/123/index.m3u8 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f mp4 " +
                "-movflags +frag_keyframe+empty_moov+default_base_moof " +
                "-frag_duration 100000 " +
                "pipe:1"))
            .Returns(m_ffmpegProcess.Object);

        byte[] headerBuff = new byte[1300];
        Random.Shared.NextBytes(headerBuff);
        
        CancellationTokenSource cts = new CancellationTokenSource();

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff.AsMemory().Slice(0, 90).CopyTo(buff);
            })
            .ReturnsAsync(90); // Insufficient header.
        
        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff.AsMemory().Slice(90, 5).CopyTo(buff);
            })
            .ReturnsAsync(5); // Still insufficient header.

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.StopAsync())
            .Returns(Task.CompletedTask);

        m_timeService
            .Setup(x => x.Delay(TimeSpan.FromSeconds(5)))
            .Callback(() => cts.Cancel()) // Cancel the test.
            .Returns(Task.CompletedTask);

        m_ffmpegProcess
            .InSequence(sequence)
            .SetupGet(x => x.IsRunning)
            .Returns(false);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.Dispose());

        // Test.
        Camera camera = new Camera(
            "123",
            "camera",
            "rtspurl");
        Mock<Action<CameraRecordingSegment>> callback = new Mock<Action<CameraRecordingSegment>>(MockBehavior.Strict);
        await m_service.ProcessAsync(
            camera,
            callback.Object,
            cts.Token);

        // Assert.
        m_sourceService.VerifyAll();
        m_ffmpegProcessService.VerifyAll();
        m_ffmpegProcess.VerifyAll();
        m_timeService.VerifyAll();
        callback.VerifyAll();
    }
    
    [Fact]
    public async Task ProcessAsync_HeaderTooBig_Restart()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        m_configurationService
            .SetupGet(x => x.RtspListenMinPort)
            .Returns(2000);
        m_configurationService
            .SetupGet(x => x.RtspListenMaxPort)
            .Returns(2010);
        m_configurationService
            .SetupGet(x => x.BasePath)
            .Returns("/api");
        
        MockSequence sequence = new MockSequence();
        
        m_sourceService
            .InSequence(sequence)
            .Setup(x => x.CreateDirectory("/data/camera-archive/123"));

        // 1st round.
        m_ffmpegProcessService
            .InSequence(sequence)
            .Setup(x => x.Start(
                "-hide_banner " +
                "-loglevel warning " +
                "-fflags +genpts " +
                "-i rtspurl " +
                "-min_port 2000 " +
                "-max_port 2010 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f hls " +
                "-hls_time 10 " +
                "-hls_segment_filename /data/camera-archive/123/%d.ts " +
                "-hls_segment_type mpegts " +
                "-hls_list_size 361 " +
                "-hls_flags append_list " +
                "-hls_base_url /api/camera/archive/123/chunk/ " +
                "/data/camera-archive/123/index.m3u8 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f mp4 " +
                "-movflags +frag_keyframe+empty_moov+default_base_moof " +
                "-frag_duration 100000 " +
                "pipe:1"))
            .Returns(m_ffmpegProcess.Object);

        byte[] headerBuff = new byte[3000];
        Random.Shared.NextBytes(headerBuff);
        
        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff.CopyTo(buff);
            })
            .ReturnsAsync(headerBuff.Length); // Too big.

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.StopAsync())
            .Returns(Task.CompletedTask);

        m_timeService
            .Setup(x => x.Delay(TimeSpan.FromSeconds(5)))
            .Returns(Task.CompletedTask);

        m_ffmpegProcess
            .InSequence(sequence)
            .SetupGet(x => x.IsRunning)
            .Returns(false);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.Dispose());
        
        // 2nd round.
        m_ffmpegProcessService
            .InSequence(sequence)
            .Setup(x => x.Start(
                "-hide_banner " +
                "-loglevel warning " +
                "-fflags +genpts " +
                "-i rtspurl " +
                "-min_port 2000 " +
                "-max_port 2010 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f hls " +
                "-hls_time 10 " +
                "-hls_segment_filename /data/camera-archive/123/%d.ts " +
                "-hls_segment_type mpegts " +
                "-hls_list_size 361 " +
                "-hls_flags append_list " +
                "-hls_base_url /api/camera/archive/123/chunk/ " +
                "/data/camera-archive/123/index.m3u8 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f mp4 " +
                "-movflags +frag_keyframe+empty_moov+default_base_moof " +
                "-frag_duration 100000 " +
                "pipe:1"))
            .Returns(m_ffmpegProcess.Object);

        byte[] headerBuff2 = new byte[1300];
        Random.Shared.NextBytes(headerBuff2);
        
        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff2.CopyTo(buff);
            })
            .ReturnsAsync(headerBuff2.Length);

        m_mp4InfoService
            .InSequence(sequence)
            .Setup(x => x.GetInfoAsync(headerBuff2))
            .ReturnsAsync("MP4");

        m_mp4InfoParserService
            .InSequence(sequence)
            .Setup(x => x.Parse("MP4"))
            .Returns(new[]
            {
                "avc1.640029",
                "mp4a.40.2"
            });

        byte[] dataBuff = new byte[100];
        Random.Shared.NextBytes(dataBuff);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                dataBuff.CopyTo(buff);
            })
            .ReturnsAsync(dataBuff.Length);

        CancellationTokenSource cts = new CancellationTokenSource();
        Mock<Action<CameraRecordingSegment>> callback = new Mock<Action<CameraRecordingSegment>>(MockBehavior.Strict);
        callback
            .InSequence(sequence)
            .Setup(x => x.Invoke(It.IsAny<CameraRecordingSegment>()))
            .Callback<CameraRecordingSegment>(segment =>
            {
                Assert.Equal(
                    "video/mp4; codecs=\"avc1.640029,mp4a.40.2\"",
                    segment.Codec);
                Assert.Equal(headerBuff2, segment.Header);
                Assert.Equal(dataBuff, segment.Data);
                
                cts.Cancel(); // Stop the test.
            });

        m_ffmpegProcess
            .InSequence(sequence)
            .SetupGet(x => x.IsRunning)
            .Returns(true);
        
        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.StopAsync())
            .Returns(Task.CompletedTask);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.Dispose());

        // Test.
        Camera camera = new Camera(
            "123",
            "camera",
            "rtspurl");
        await m_service.ProcessAsync(
            camera,
            callback.Object,
            cts.Token);

        // Assert.
        m_sourceService.VerifyAll();
        m_ffmpegProcessService.VerifyAll();
        m_ffmpegProcess.VerifyAll();
        m_timeService.VerifyAll();
        callback.VerifyAll();
    }
    
    [Fact]
    public async Task ProcessAsync_InvalidCodec_Restart()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        m_configurationService
            .SetupGet(x => x.RtspListenMinPort)
            .Returns(2000);
        m_configurationService
            .SetupGet(x => x.RtspListenMaxPort)
            .Returns(2010);
        m_configurationService
            .SetupGet(x => x.BasePath)
            .Returns("/api");
        
        MockSequence sequence = new MockSequence();
        
        m_sourceService
            .InSequence(sequence)
            .Setup(x => x.CreateDirectory("/data/camera-archive/123"));

        // 1st round.
        m_ffmpegProcessService
            .InSequence(sequence)
            .Setup(x => x.Start(
                "-hide_banner " +
                "-loglevel warning " +
                "-fflags +genpts " +
                "-i rtspurl " +
                "-min_port 2000 " +
                "-max_port 2010 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f hls " +
                "-hls_time 10 " +
                "-hls_segment_filename /data/camera-archive/123/%d.ts " +
                "-hls_segment_type mpegts " +
                "-hls_list_size 361 " +
                "-hls_flags append_list " +
                "-hls_base_url /api/camera/archive/123/chunk/ " +
                "/data/camera-archive/123/index.m3u8 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f mp4 " +
                "-movflags +frag_keyframe+empty_moov+default_base_moof " +
                "-frag_duration 100000 " +
                "pipe:1"))
            .Returns(m_ffmpegProcess.Object);

        byte[] headerBuff = new byte[1300];
        Random.Shared.NextBytes(headerBuff);
        
        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff.CopyTo(buff);
            })
            .ReturnsAsync(headerBuff.Length);

        m_mp4InfoService
            .InSequence(sequence)
            .Setup(x => x.GetInfoAsync(headerBuff))
            .ReturnsAsync("MP4");

        m_mp4InfoParserService
            .InSequence(sequence)
            .Setup(x => x.Parse("MP4"))
            .Returns(Array.Empty<string>());

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.StopAsync())
            .Returns(Task.CompletedTask);

        m_timeService
            .Setup(x => x.Delay(TimeSpan.FromSeconds(5)))
            .Returns(Task.CompletedTask);

        m_ffmpegProcess
            .InSequence(sequence)
            .SetupGet(x => x.IsRunning)
            .Returns(false);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.Dispose());
        
        // 2nd round.
        m_ffmpegProcessService
            .InSequence(sequence)
            .Setup(x => x.Start(
                "-hide_banner " +
                "-loglevel warning " +
                "-fflags +genpts " +
                "-i rtspurl " +
                "-min_port 2000 " +
                "-max_port 2010 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f hls " +
                "-hls_time 10 " +
                "-hls_segment_filename /data/camera-archive/123/%d.ts " +
                "-hls_segment_type mpegts " +
                "-hls_list_size 361 " +
                "-hls_flags append_list " +
                "-hls_base_url /api/camera/archive/123/chunk/ " +
                "/data/camera-archive/123/index.m3u8 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f mp4 " +
                "-movflags +frag_keyframe+empty_moov+default_base_moof " +
                "-frag_duration 100000 " +
                "pipe:1"))
            .Returns(m_ffmpegProcess.Object);

        byte[] headerBuff2 = new byte[1400];
        Random.Shared.NextBytes(headerBuff2);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff2.CopyTo(buff);
            })
            .ReturnsAsync(headerBuff2.Length);

        m_mp4InfoService
            .InSequence(sequence)
            .Setup(x => x.GetInfoAsync(headerBuff2))
            .ReturnsAsync("MP42");

        m_mp4InfoParserService
            .InSequence(sequence)
            .Setup(x => x.Parse("MP42"))
            .Returns(new[]
            {
                "avc1.640029",
                "mp4a.40.2"
            });

        byte[] dataBuff = new byte[100];
        Random.Shared.NextBytes(dataBuff);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                dataBuff.CopyTo(buff);
            })
            .ReturnsAsync(dataBuff.Length);

        CancellationTokenSource cts = new CancellationTokenSource();
        Mock<Action<CameraRecordingSegment>> callback = new Mock<Action<CameraRecordingSegment>>(MockBehavior.Strict);
        callback
            .InSequence(sequence)
            .Setup(x => x.Invoke(It.IsAny<CameraRecordingSegment>()))
            .Callback<CameraRecordingSegment>(segment =>
            {
                Assert.Equal(
                    "video/mp4; codecs=\"avc1.640029,mp4a.40.2\"",
                    segment.Codec);
                Assert.Equal(headerBuff2, segment.Header);
                Assert.Equal(dataBuff, segment.Data);
                
                cts.Cancel(); // Stop the test.
            });

        m_ffmpegProcess
            .InSequence(sequence)
            .SetupGet(x => x.IsRunning)
            .Returns(true);
        
        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.StopAsync())
            .Returns(Task.CompletedTask);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.Dispose());

        // Test.
        Camera camera = new Camera(
            "123",
            "camera",
            "rtspurl");
        await m_service.ProcessAsync(
            camera,
            callback.Object,
            cts.Token);

        // Assert.
        m_sourceService.VerifyAll();
        m_ffmpegProcessService.VerifyAll();
        m_ffmpegProcess.VerifyAll();
        m_timeService.VerifyAll();
        callback.VerifyAll();
    }
    
    [Fact]
    public async Task ProcessAsync_ReadError_Restart()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.CameraArchivalDir)
            .Returns("/data/camera-archive");
        m_configurationService
            .SetupGet(x => x.RtspListenMinPort)
            .Returns(2000);
        m_configurationService
            .SetupGet(x => x.RtspListenMaxPort)
            .Returns(2010);
        m_configurationService
            .SetupGet(x => x.BasePath)
            .Returns("/api");
        
        MockSequence sequence = new MockSequence();
        
        m_sourceService
            .InSequence(sequence)
            .Setup(x => x.CreateDirectory("/data/camera-archive/123"));

        // 1st round.
        m_ffmpegProcessService
            .InSequence(sequence)
            .Setup(x => x.Start(
                "-hide_banner " +
                "-loglevel warning " +
                "-fflags +genpts " +
                "-i rtspurl " +
                "-min_port 2000 " +
                "-max_port 2010 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f hls " +
                "-hls_time 10 " +
                "-hls_segment_filename /data/camera-archive/123/%d.ts " +
                "-hls_segment_type mpegts " +
                "-hls_list_size 361 " +
                "-hls_flags append_list " +
                "-hls_base_url /api/camera/archive/123/chunk/ " +
                "/data/camera-archive/123/index.m3u8 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f mp4 " +
                "-movflags +frag_keyframe+empty_moov+default_base_moof " +
                "-frag_duration 100000 " +
                "pipe:1"))
            .Returns(m_ffmpegProcess.Object);

        byte[] headerBuff = new byte[1300];
        Random.Shared.NextBytes(headerBuff);
        
        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff.CopyTo(buff);
            })
            .ReturnsAsync(headerBuff.Length);

        m_mp4InfoService
            .InSequence(sequence)
            .Setup(x => x.GetInfoAsync(headerBuff))
            .ReturnsAsync("MP4");

        m_mp4InfoParserService
            .InSequence(sequence)
            .Setup(x => x.Parse("MP4"))
            .Returns(new[]
            {
                "avc1.640029",
                "mp4a.40.2"
            });

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .ThrowsAsync(new Exception()); // Read error.

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.StopAsync())
            .Returns(Task.CompletedTask);

        m_timeService
            .Setup(x => x.Delay(TimeSpan.FromSeconds(5)))
            .Returns(Task.CompletedTask);

        m_ffmpegProcess
            .InSequence(sequence)
            .SetupGet(x => x.IsRunning)
            .Returns(false);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.Dispose());
        
        // 2nd round.
        m_ffmpegProcessService
            .InSequence(sequence)
            .Setup(x => x.Start(
                "-hide_banner " +
                "-loglevel warning " +
                "-fflags +genpts " +
                "-i rtspurl " +
                "-min_port 2000 " +
                "-max_port 2010 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f hls " +
                "-hls_time 10 " +
                "-hls_segment_filename /data/camera-archive/123/%d.ts " +
                "-hls_segment_type mpegts " +
                "-hls_list_size 361 " +
                "-hls_flags append_list " +
                "-hls_base_url /api/camera/archive/123/chunk/ " +
                "/data/camera-archive/123/index.m3u8 " +
                "-c:v copy " +
                "-c:a aac " +
                "-f mp4 " +
                "-movflags +frag_keyframe+empty_moov+default_base_moof " +
                "-frag_duration 100000 " +
                "pipe:1"))
            .Returns(m_ffmpegProcess.Object);

        byte[] headerBuff2 = new byte[1400];
        Random.Shared.NextBytes(headerBuff2);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                headerBuff2.CopyTo(buff);
            })
            .ReturnsAsync(headerBuff2.Length);

        m_mp4InfoService
            .InSequence(sequence)
            .Setup(x => x.GetInfoAsync(headerBuff2))
            .ReturnsAsync("MP42");

        m_mp4InfoParserService
            .InSequence(sequence)
            .Setup(x => x.Parse("MP42"))
            .Returns(new[]
            {
                "avc1.640029",
                "mp4a.40.2"
            });

        byte[] dataBuff = new byte[100];
        Random.Shared.NextBytes(dataBuff);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.ReadAsync(It.IsAny<Memory<byte>>()))
            .Callback<Memory<byte>>(buff =>
            {
                dataBuff.CopyTo(buff);
            })
            .ReturnsAsync(dataBuff.Length);

        CancellationTokenSource cts = new CancellationTokenSource();
        Mock<Action<CameraRecordingSegment>> callback = new Mock<Action<CameraRecordingSegment>>(MockBehavior.Strict);
        callback
            .InSequence(sequence)
            .Setup(x => x.Invoke(It.IsAny<CameraRecordingSegment>()))
            .Callback<CameraRecordingSegment>(segment =>
            {
                Assert.Equal(
                    "video/mp4; codecs=\"avc1.640029,mp4a.40.2\"",
                    segment.Codec);
                Assert.Equal(headerBuff2, segment.Header);
                Assert.Equal(dataBuff, segment.Data);
                
                cts.Cancel(); // Stop the test.
            });

        m_ffmpegProcess
            .InSequence(sequence)
            .SetupGet(x => x.IsRunning)
            .Returns(true);
        
        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.StopAsync())
            .Returns(Task.CompletedTask);

        m_ffmpegProcess
            .InSequence(sequence)
            .Setup(x => x.Dispose());

        // Test.
        Camera camera = new Camera(
            "123",
            "camera",
            "rtspurl");
        await m_service.ProcessAsync(
            camera,
            callback.Object,
            cts.Token);

        // Assert.
        m_sourceService.VerifyAll();
        m_ffmpegProcessService.VerifyAll();
        m_ffmpegProcess.VerifyAll();
        m_timeService.VerifyAll();
        callback.VerifyAll();
    }
    
    
}
