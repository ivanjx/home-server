using System;
using System.Threading.Tasks;
using home_server.Services;
using home_server.Services.Users;
using Microsoft.Data.Sqlite;
using Xunit;

namespace home_server.Tests.Db.Services.Users;

public class UserRepositoryTest
{
    DbConn m_conn;
    UserRepository m_repository;

    public UserRepositoryTest()
    {
        m_conn = new DbConn(
            new FakeConfigurationService());
        m_repository = new UserRepository(m_conn);
    }

    [Fact]
    public async Task AddAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();
        
        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (id, username, created_at)
                VALUES ('1', 'user1', 11);
                INSERT INTO user_basic_credentials (
                    user_id, password_hash, password_pads
                ) VALUES (
                    '1', 'passwordhash1', 'passwordpads1'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime now = epoch.AddSeconds(123);
            string result = await m_repository.AddAsync(
                "user2",
                "passwordhash2",
                "passwordpads2",
                now);
            
            // Assert.
            Assert.True(Guid.TryParse(result, out _));

            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT
                        u.id, u.username, u.created_at,
                        ubc.password_hash, ubc.password_pads
                    FROM users AS u
                    INNER JOIN user_basic_credentials AS ubc
                        ON ubc.user_id = u.id";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("1", reader.GetString(0));
                Assert.Equal("user1", reader.GetString(1));
                Assert.Equal(11, reader.GetInt32(2));
                Assert.Equal("passwordhash1", reader.GetString(3));
                Assert.Equal("passwordpads1", reader.GetString(4));

                Assert.True(await reader.ReadAsync());
                Assert.Equal(result, reader.GetString(0));
                Assert.Equal("user2", reader.GetString(1));
                Assert.Equal(123, reader.GetInt32(2));
                Assert.Equal("passwordhash2", reader.GetString(3));
                Assert.Equal("passwordpads2", reader.GetString(4));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_basic_credentials;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task CountAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();
        
        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (id, username, created_at)
                VALUES ('1', 'user1', 11);
                INSERT INTO user_basic_credentials (
                    user_id, password_hash, password_pads
                ) VALUES (
                    '1', 'passwordhash1', 'passwordpads1'
                );
                INSERT INTO users (id, username, created_at)
                VALUES ('2', 'user2', 22);
                INSERT INTO user_basic_credentials (
                    user_id, password_hash, password_pads
                ) VALUES (
                    '2', 'passwordhash2', 'passwordpads2'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            long result = await m_repository.CountAsync();
            
            // Assert.
            Assert.Equal(2, result);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_basic_credentials;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task IsUsernameExistsAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();
        
        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (id, username, created_at)
                VALUES ('1', 'user1', 11);
                INSERT INTO user_basic_credentials (
                    user_id, password_hash, password_pads
                ) VALUES (
                    '1', 'passwordhash1', 'passwordpads1'
                );
                INSERT INTO users (id, username, created_at)
                VALUES ('2', 'user2', 22);
                INSERT INTO user_basic_credentials (
                    user_id, password_hash, password_pads
                ) VALUES (
                    '2', 'passwordhash2', 'passwordpads2'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }
        
        try
        {
            // Test.
            bool result1 = await m_repository.IsUsernameExistsAsync("user1");
            bool result2 = await m_repository.IsUsernameExistsAsync("user2");
            bool result3 = await m_repository.IsUsernameExistsAsync("user3");
            
            // Assert.
            Assert.True(result1);
            Assert.True(result2);
            Assert.False(result3);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_basic_credentials;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }
}
