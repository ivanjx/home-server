using System;
using System.Linq;
using System.Threading.Tasks;
using home_server.Services;
using home_server.Services.Cameras;
using Microsoft.Data.Sqlite;
using Moq;
using Xunit;

namespace home_server.Tests.Db.Services.Cameras;

public class CameraRepositoryTest
{
    DbConn m_conn;
    Mock<ITimeService> m_timeService;
    CameraRepository m_repository;

    public CameraRepositoryTest()
    {
        m_conn = new DbConn(
            new FakeConfigurationService());
        m_timeService = new Mock<ITimeService>(MockBehavior.Strict);
        m_repository = new CameraRepository(
            m_conn,
            m_timeService.Object);
    }

    [Fact]
    public async Task GetByIdAsync_Test()
    {
        // Setup.
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO cameras (id, name, rtsp_url, created_at)
                VALUES
                    ('1', 'cam1', 'rtsp://cam1', 11),
                    ('2', 'cam2', 'rtsp://cam2', 22)";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            Camera? result1 = await m_repository.GetByIdAsync("1");
            Camera? result2 = await m_repository.GetByIdAsync("2");
            Camera? result3 = await m_repository.GetByIdAsync("3");
            
            // Assert.
            Camera camera1 = new Camera("1", "cam1", "rtsp://cam1");
            Assert.Equal(camera1, result1);
            
            Camera camera2 = new Camera("2", "cam2", "rtsp://cam2");
            Assert.Equal(camera2, result2);
            
            Assert.Null(result3);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM cameras;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task ListAllAsync_Test()
    {
        // Setup.
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO cameras (id, name, rtsp_url, created_at)
                VALUES
                    ('1', 'cam1', 'rtsp://cam1', 11),
                    ('2', 'cam2', 'rtsp://cam2', 22)";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            var result = await m_repository.ListAllAsync();
            
            // Assert.
            Camera camera1 = new Camera("1", "cam1", "rtsp://cam1");
            Assert.Equal(camera1, result.ToArray()[0]);
            
            Camera camera2 = new Camera("2", "cam2", "rtsp://cam2");
            Assert.Equal(camera2, result.ToArray()[1]);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM cameras;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task IsRtspUrlExistsAsync_Test()
    {
        // Setup.
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO cameras (id, name, rtsp_url, created_at)
                VALUES
                    ('1', 'cam1', 'rtsp://cam1', 11),
                    ('2', 'cam2', 'rtsp://cam2', 22)";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            bool result1 = await m_repository.IsRtspUrlExistsAsync("rtsp://cam1");
            bool result2 = await m_repository.IsRtspUrlExistsAsync("rtsp://cam2");
            bool result3 = await m_repository.IsRtspUrlExistsAsync("rtsp://cam3");
            
            // Assert.
            Assert.True(result1);
            Assert.True(result2);
            Assert.False(result3);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM cameras;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task AddAsync_Test()
    {
        // Setup.
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO cameras (id, name, rtsp_url, created_at)
                VALUES
                    ('1', 'cam1', 'rtsp://cam1', 11)";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }
        
        DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        DateTime now = epoch.AddSeconds(123);
        m_timeService
            .Setup(x => x.GetUtcNow())
            .Returns(now);

        try
        {
            // Test.
            Camera result = await m_repository.AddAsync(
                "cam2",
                "rtsp://cam2");
            
            // Assert.
            Assert.True(Guid.TryParse(result.Id, out _));
            Assert.Equal("cam2", result.Name);
            Assert.Equal("rtsp://cam2", result.RtspUrl);

            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT id, name, rtsp_url, created_at
                    FROM cameras";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("1", reader.GetString(0));
                Assert.Equal("cam1", reader.GetString(1));
                Assert.Equal("rtsp://cam1", reader.GetString(2));
                Assert.Equal(11, reader.GetInt32(3));

                Assert.True(await reader.ReadAsync());
                Assert.Equal(result.Id, reader.GetString(0));
                Assert.Equal("cam2", reader.GetString(1));
                Assert.Equal("rtsp://cam2", reader.GetString(2));
                Assert.Equal(123, reader.GetInt32(3));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM cameras;";
            await command.ExecuteNonQueryAsync();
        }
    }
    
    [Fact]
    public async Task DeleteAsync_Test()
    {
        // Setup.
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO cameras (id, name, rtsp_url, created_at)
                VALUES
                    ('1', 'cam1', 'rtsp://cam1', 11),
                    ('2', 'cam2', 'rtsp://cam2', 22)";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            await m_repository.DeleteAsync("2");
            
            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT id, name, rtsp_url, created_at
                    FROM cameras";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("1", reader.GetString(0));
                Assert.Equal("cam1", reader.GetString(1));
                Assert.Equal("rtsp://cam1", reader.GetString(2));
                Assert.Equal(11, reader.GetInt32(3));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM cameras;";
            await command.ExecuteNonQueryAsync();
        }
    }
    
    [Fact]
    public async Task UpdateAsync_Test()
    {
        // Setup.
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO cameras (id, name, rtsp_url, created_at)
                VALUES
                    ('1', 'cam1', 'rtsp://cam1', 11),
                    ('2', 'cam2', 'rtsp://cam2', 22)";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            await m_repository.UpdateAsync(
                new Camera(
                    "1",
                    "cam11",
                    "rtsp://cam11"));
            
            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT id, name, rtsp_url, created_at
                    FROM cameras";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("1", reader.GetString(0));
                Assert.Equal("cam11", reader.GetString(1));
                Assert.Equal("rtsp://cam11", reader.GetString(2));
                Assert.Equal(11, reader.GetInt32(3));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("2", reader.GetString(0));
                Assert.Equal("cam2", reader.GetString(1));
                Assert.Equal("rtsp://cam2", reader.GetString(2));
                Assert.Equal(22, reader.GetInt32(3));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM cameras;";
            await command.ExecuteNonQueryAsync();
        }
    }
}
