using System;
using System.Threading.Tasks;
using home_server.Services;
using home_server.Services.Authentication;
using Microsoft.Data.Sqlite;
using Xunit;

namespace home_server.Tests.Db.Services.Authentication;

public class UserAuthenticationTokenRepositoryTest
{
    DbConn m_conn;
    UserAuthenticationTokenRepository m_repository;

    public UserAuthenticationTokenRepositoryTest()
    {
        m_conn = new DbConn(
            new FakeConfigurationService());
        m_repository = new UserAuthenticationTokenRepository(m_conn);
    }

    [Fact]
    public async Task AddAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (
                    id, username, created_at
                ) VALUES (
                    'user1', 'username', 11
                )";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            DateTime now = DateTime.Parse("2023-11-13 01:00:00")
                .ToLocalTime()
                .ToUniversalTime();
            await m_repository.AddAsync(
                new UserAuthenticationToken(
                    "1",
                    true,
                    now.AddSeconds(1),
                    now.AddMonths(1),
                    "android",
                    "1.1.1.1",
                    "user1",
                    "username"));
            await m_repository.AddAsync(
                new UserAuthenticationToken(
                    "2",
                    false,
                    now,
                    now.AddHours(1),
                    "iphone",
                    "1.1.1.2",
                    "user1",
                    "username"));
            
            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT id, user_id, is_long_lived,
                           created_at, expire_at, device_name, ip_address
                    FROM user_authentication_tokens";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("1", reader.GetString(0));
                Assert.Equal("user1", reader.GetString(1));
                Assert.True(reader.GetBoolean(2));
                Assert.Equal(1699837201, reader.GetInt32(3));
                Assert.Equal(1702429200, reader.GetInt32(4));
                Assert.Equal("android", reader.GetString(5));
                Assert.Equal("1.1.1.1", reader.GetString(6));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("2", reader.GetString(0));
                Assert.Equal("user1", reader.GetString(1));
                Assert.False(reader.GetBoolean(2));
                Assert.Equal(1699837200, reader.GetInt32(3));
                Assert.Equal(1699840800, reader.GetInt32(4));
                Assert.Equal("iphone", reader.GetString(5));
                Assert.Equal("1.1.1.2", reader.GetString(6));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_authentication_tokens;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task DeleteAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (
                    id, username, created_at
                ) VALUES (
                    'user1', 'username', 11
                );
                
                INSERT INTO user_authentication_tokens (
                    id, user_id, is_long_lived, created_at, expire_at,
                    device_name, ip_address
                ) VALUES
                (
                    't1', 'user1', true, 1699837201, 1702429200,
                    'android', '1.1.1.1'
                ),
                (
                    't2', 'user1', false, 1699837200, 1702429201,
                    'iphone', '1.1.1.2'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            await m_repository.DeleteAsync("t2");
            
            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT id, user_id, is_long_lived, created_at,
                           expire_at, device_name, ip_address
                    FROM user_authentication_tokens";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("t1", reader.GetString(0));
                Assert.Equal("user1", reader.GetString(1));
                Assert.True(reader.GetBoolean(2));
                Assert.Equal(1699837201, reader.GetInt32(3));
                Assert.Equal(1702429200, reader.GetInt32(4));
                Assert.Equal("android", reader.GetString(5));
                Assert.Equal("1.1.1.1", reader.GetString(6));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_authentication_tokens;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task GetByIdAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (
                    id, username, created_at
                ) VALUES
                (
                    'user1', 'username1', 11
                ),
                (
                    'user2', 'username2', 12
                );
                
                INSERT INTO user_authentication_tokens (
                    id, user_id, is_long_lived, created_at, expire_at,
                    device_name, ip_address
                ) VALUES
                (
                    't1', 'user1', true, 1699837201, 1702429200,
                    'android', '1.1.1.1'
                ),
                (
                    't2', 'user2', false, 1699837200, 1702429201,
                    'iphone', '1.1.1.2'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            UserAuthenticationToken? result1 = await m_repository.GetByIdAsync("t1");
            UserAuthenticationToken? result2 = await m_repository.GetByIdAsync("t2");
            UserAuthenticationToken? result3 = await m_repository.GetByIdAsync("t3");
            
            // Assert.
            UserAuthenticationToken token1 = new UserAuthenticationToken(
                "t1",
                true,
                DateTime.Parse("2023-11-13 01:00:01").ToLocalTime().ToUniversalTime(),
                DateTime.Parse("2023-12-13 01:00:00").ToLocalTime().ToUniversalTime(),
                "android",
                "1.1.1.1",
                "user1",
                "username1");
            UserAuthenticationToken token2 = new UserAuthenticationToken(
                "t2",
                false,
                DateTime.Parse("2023-11-13 01:00:00").ToLocalTime().ToUniversalTime(),
                DateTime.Parse("2023-12-13 01:00:01").ToLocalTime().ToUniversalTime(),
                "iphone",
                "1.1.1.2",
                "user2",
                "username2");
            Assert.Equal(token1, result1);
            Assert.Equal(token2, result2);
            Assert.Null(result3);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_authentication_tokens;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task UpdateIpAddressAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (
                    id, username, created_at
                ) VALUES (
                    'user1', 'username', 11
                );
                
                INSERT INTO user_authentication_tokens (
                    id, user_id, is_long_lived, created_at, expire_at,
                    device_name, ip_address
                ) VALUES
                (
                    't1', 'user1', true, 1699837201, 1702429200,
                    'android', '1.1.1.1'
                ),
                (
                    't2', 'user1', false, 1699837200, 1702429201,
                    'iphone', '1.1.1.2'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            await m_repository.UpdateIpAddressAsync("t2", "1.1.1.3");
            
            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT id, user_id, is_long_lived, created_at,
                           expire_at, device_name, ip_address
                    FROM user_authentication_tokens";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("t1", reader.GetString(0));
                Assert.Equal("user1", reader.GetString(1));
                Assert.True(reader.GetBoolean(2));
                Assert.Equal(1699837201, reader.GetInt32(3));
                Assert.Equal(1702429200, reader.GetInt32(4));
                Assert.Equal("android", reader.GetString(5));
                Assert.Equal("1.1.1.1", reader.GetString(6));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("t2", reader.GetString(0));
                Assert.Equal("user1", reader.GetString(1));
                Assert.False(reader.GetBoolean(2));
                Assert.Equal(1699837200, reader.GetInt32(3));
                Assert.Equal(1702429201, reader.GetInt32(4));
                Assert.Equal("iphone", reader.GetString(5));
                Assert.Equal("1.1.1.3", reader.GetString(6));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_authentication_tokens;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task UpdateExpireTimeWhenNotExpiredAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (
                    id, username, created_at
                ) VALUES (
                    'user1', 'username', 11
                );
                
                INSERT INTO user_authentication_tokens (
                    id, user_id, is_long_lived, created_at, expire_at,
                    device_name, ip_address
                ) VALUES
                (
                    't1', 'user1', true, 1699837200, 1699840800,
                    'android', '1.1.1.1'
                ),
                (
                    't2', 'user1', true, 1699837200, 1702429200,
                    'iphone', '1.1.1.2'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            DateTime now = DateTime.Parse("2023-11-22 00:00:00")
                .ToLocalTime()
                .ToUniversalTime();
            await m_repository.UpdateExpireTimeWhenNotExpiredAsync(
                "t1",
                now,
                now.AddDays(1));
            await m_repository.UpdateExpireTimeWhenNotExpiredAsync(
                "t2",
                now,
                now.AddDays(1));

            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT id, user_id, is_long_lived, created_at,
                           expire_at, device_name, ip_address
                    FROM user_authentication_tokens";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("t1", reader.GetString(0));
                Assert.Equal("user1", reader.GetString(1));
                Assert.True(reader.GetBoolean(2));
                Assert.Equal(1699837200, reader.GetInt32(3));
                Assert.Equal(1699840800, reader.GetInt32(4));
                Assert.Equal("android", reader.GetString(5));
                Assert.Equal("1.1.1.1", reader.GetString(6));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("t2", reader.GetString(0));
                Assert.Equal("user1", reader.GetString(1));
                Assert.True(reader.GetBoolean(2));
                Assert.Equal(1699837200, reader.GetInt32(3));
                Assert.Equal(1700697600, reader.GetInt32(4));
                Assert.Equal("iphone", reader.GetString(5));
                Assert.Equal("1.1.1.2", reader.GetString(6));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_authentication_tokens;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }
}
