using System;
using System.Threading.Tasks;
using home_server.Services;
using home_server.Services.Authentication;
using Microsoft.Data.Sqlite;
using Xunit;

namespace home_server.Tests.Db.Services.Authentication;

public class UserAuthenticationRepositoryTest
{
    DbConn m_conn;
    UserAuthenticationRepository m_repository;

    public UserAuthenticationRepositoryTest()
    {
        m_conn = new DbConn(
            new FakeConfigurationService());
        m_repository = new UserAuthenticationRepository(m_conn);
    }

    [Fact]
    public async Task GetByUserIdAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();
        
        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (id, username, created_at)
                VALUES ('1', 'user1', 11);
                INSERT INTO user_basic_credentials (
                    user_id, password_hash, password_pads
                ) VALUES (
                    '1', 'passwordhash1', 'passwordpads1'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            UserAuth? result1 = await m_repository.GetByUserIdAsync("1");
            UserAuth? result2 = await m_repository.GetByUserIdAsync("2");
            
            // Assert.
            UserAuth auth = new UserAuth(
                "1",
                "passwordhash1",
                "passwordpads1");
            Assert.Equal(auth, result1);
            Assert.Null(result2);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_basic_credentials;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task GetByUsernameAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();
        
        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (id, username, created_at)
                VALUES ('1', 'user1', 11);
                INSERT INTO user_basic_credentials (
                    user_id, password_hash, password_pads
                ) VALUES (
                    '1', 'passwordhash1', 'passwordpads1'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            UserAuth? result1 = await m_repository.GetByUsernameAsync("user1");
            UserAuth? result2 = await m_repository.GetByUsernameAsync("user2");
            
            // Assert.
            UserAuth auth = new UserAuth(
                "1",
                "passwordhash1",
                "passwordpads1");
            Assert.Equal(auth, result1);
            Assert.Null(result2);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_basic_credentials;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task SetAuthHashPadsAsync_Test()
    {
        // Setup.
        SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();
        
        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO users (id, username, created_at)
                VALUES
                    ('1', 'user1', 11),
                    ('2', 'user2', 12);
                INSERT INTO user_basic_credentials (
                    user_id, password_hash, password_pads
                ) VALUES (
                    '1', 'passwordhash1', 'passwordpads1'
                );
                INSERT INTO user_basic_credentials (
                    user_id, password_hash, password_pads
                ) VALUES (
                    '2', 'passwordhash2', 'passwordpads2'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            await m_repository.SetAuthHashPadsAsync(
                "2",
                "newpasswordhash2",
                "newpasswordpads2");
            
            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT user_id, password_hash, password_pads
                    FROM user_basic_credentials";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("1", reader.GetString(0));
                Assert.Equal("passwordhash1", reader.GetString(1));
                Assert.Equal("passwordpads1", reader.GetString(2));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("2", reader.GetString(0));
                Assert.Equal("newpasswordhash2", reader.GetString(1));
                Assert.Equal("newpasswordpads2", reader.GetString(2));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM user_basic_credentials;
                DELETE FROM users;";
            await command.ExecuteNonQueryAsync();
        }
    }
}
