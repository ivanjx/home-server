using System;
using home_server.Services;

namespace home_server.Tests.Db;

public class FakeConfigurationService : IConfigurationService
{
    public string BasePath => "";
    public string ListenEndpoint => "";
    public string DatabasePath => "InMemorySample;Mode=Memory;Cache=Shared";
    public string? SeqEndpoint => null;
    public string? SeqApiKey => null;
    public int RtspListenMinPort => 0;
    public int RtspListenMaxPort => 0;
    public string CameraArchivalDir => "";
    public int CameraArchivalDays => 0;
    public string CacheDir => "";
}
