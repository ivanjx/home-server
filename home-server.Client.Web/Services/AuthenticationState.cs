using System;

namespace home_server.Client.Web.Services;

public record User
{
    public string Id
    {
        get;
        init;
    }

    public string Username
    {
        get;
        init;
    }

    public User()
    {
        Id = "";
        Username = "";
    }
}

public class AuthenticationState
{
    public Action<User?>? UserChanged;

    User? m_user;

    public User? User
    {
        get => m_user;
        set
        {
            m_user = value;
            UserChanged?.Invoke(value);
        }
    }
}
