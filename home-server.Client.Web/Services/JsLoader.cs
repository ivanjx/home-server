using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace home_server.Client.Web.Services;

public class JsLoader
{
    const string PATH = "./js/";

    IJSRuntime m_js;

    public JsLoader(IJSRuntime js)
    {
        m_js = js;
    }

    public ValueTask<IJSObjectReference> LoadAsync(string name)
    {
        return m_js.InvokeAsync<IJSObjectReference>(
            "import",
            PATH + name + ".js");
    }
}
