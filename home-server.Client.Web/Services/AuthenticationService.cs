using System;
using System.Net.Http;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace home_server.Client.Web.Services;

public interface IAuthenticationService
{
    Task LoadAuthenticationStateAsync();
    Task CheckAuthenticationAsync();
    Task AuthenticateAsync(
        string username,
        string password,
        bool rememberLogin);
    Task DeauthenticateAsync();
    Task ChangePasswordAsync(
        string oldPassword,
        string newPassword);
}

public class AuthenticationService : IAuthenticationService
{
    AuthenticationState m_state;
    IDeviceNameReaderService m_deviceNameReaderService;
    HttpClient m_client;

    public AuthenticationService(
        AuthenticationState state,
        IDeviceNameReaderService deviceNameReaderService,
        HttpClient client)
    {
        m_state = state;
        m_deviceNameReaderService = deviceNameReaderService;
        m_client = client;
    }

    /// <summary>
    /// This will be called on app load.
    /// </summary>
    public async Task LoadAuthenticationStateAsync()
    {
        while (true)
        {
            try
            {
                Console.WriteLine("Sending check authentication request");
                ApiResult result = await m_client.GetAsync(
                    "/authentication/check",
                    AuthenticationServiceContext.Default.AuthenticateApiResponse);

                if (result is ErrorApiResult error)
                {
                    if (error.Error.Error == "not_authenticated")
                    {
                        Console.WriteLine("No longer authenticated");
                        m_state.User = null;
                        return;
                    }

                    throw new Exception($"Something went wrong. Trace: {error.Error.TraceId}.");
                }
                
                if (result is not SuccessApiResult<AuthenticateApiResponse> success ||
                    success.Result == null ||
                    string.IsNullOrEmpty(success.Result.UserId) ||
                    string.IsNullOrEmpty(success.Result.Username))
                {
                    throw new Exception("Invalid response");
                }

                Console.WriteLine("Setting authentication state");
                m_state.User = new User()
                {
                    Id = success.Result.UserId,
                    Username = success.Result.Username
                };

                break;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading authentication state:");
                Console.WriteLine(ex.ToString());
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }
    }

    /// <summary>
    /// This will be called periodically by a background task.
    /// </summary>
    public async Task CheckAuthenticationAsync()
    {
        if (m_state.User == null)
        {
            Console.WriteLine("Check authentication request ignored");
            return;
        }

        try
        {
            Console.WriteLine("Sending check authentication request");
            ApiResult result = await m_client.GetAsync(
                "/authentication/check",
                AuthenticationServiceContext.Default.AuthenticateApiResponse);

            if (result is ErrorApiResult error)
            {
                if (error.Error.Error == "not_authenticated")
                {
                    Console.WriteLine("No longer authenticated");
                    m_state.User = null;
                    return;
                }

                throw new Exception($"Something went wrong. Trace: {error.Error.TraceId}.");
            }
            
            if (result is not SuccessApiResult<AuthenticateApiResponse> success ||
                success.Result == null ||
                string.IsNullOrEmpty(success.Result.UserId) ||
                string.IsNullOrEmpty(success.Result.Username))
            {
                throw new Exception("Invalid response");
            }

            Console.WriteLine("Setting authentication state");
            m_state.User = new User()
            {
                Id = success.Result.UserId,
                Username = success.Result.Username
            };
        }
        catch (Exception ex)
        {
            Console.WriteLine("Check authentication error:");
            Console.WriteLine(ex.ToString());
        }
    }

    /// <summary>
    /// This will be called by login page.
    /// </summary>
    public async Task AuthenticateAsync(string username, string password, bool rememberLogin)
    {
        Console.WriteLine("Sending authentication request");
        AuthenticateApiRequest request = new AuthenticateApiRequest()
        {
            Username = username,
            Password = password,
            DeviceName = await m_deviceNameReaderService.ReadAsync(),
            IsLongLived = rememberLogin
        };
        ApiResult result;

        try
        {
            result = await m_client.PostAsync(
                "/authentication/login",
                request,
                AuthenticationServiceContext.Default.AuthenticateApiResponse,
                AuthenticationServiceContext.Default.AuthenticateApiRequest);
        }
        catch (Exception ex)
        {
            throw new Exception("Something went wrong", ex);
        }

        if (result is ErrorApiResult error)
        {
            if (error.Error.Error == "invalid_authentication")
            {
                throw new Exception("Invalid username/password combination");
            }

            throw new Exception($"Something went wrong. Trace: {error.Error.TraceId}.");
        }

        if (result is not SuccessApiResult<AuthenticateApiResponse> success ||
            success.Result == null ||
            string.IsNullOrEmpty(success.Result.UserId) ||
            string.IsNullOrEmpty(success.Result.Username))
        {
            throw new Exception("Invalid response");
        }

        Console.WriteLine("Setting authentication state");
        m_state.User = new User()
        {
            Id = success.Result.UserId,
            Username = success.Result.Username
        };
    }

    public async Task DeauthenticateAsync()
    {
        try
        {
            Console.WriteLine("Sending deauthentication request");
            await m_client.PostAsync("/authentication/logout");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Deauthentication error:");
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            Console.WriteLine("Setting authentication state");
            m_state.User = null;
        }
    }

    public async Task ChangePasswordAsync(string oldPassword, string newPassword)
    {
        Console.WriteLine("Sending authentication request");
        ChangePasswordApiRequest request = new ChangePasswordApiRequest(
            oldPassword,
            newPassword);
        ApiResult result;

        try
        {
            result = await m_client.PostAsync(
                "/authentication/change-password",
                request,
                AuthenticationServiceContext.Default.ChangePasswordApiRequest);
        }
        catch (Exception ex)
        {
            throw new Exception("Something went wrong", ex);
        }

        if (result is ErrorApiResult error)
        {
            if (error.Error.Error == "invalid_authentication")
            {
                throw new Exception("Invalid current password");
            }
                
            throw new Exception($"Something went wrong. Trace: {error.Error.TraceId}.");
        }
    }

    public record AuthenticateApiRequest
    {
        [JsonPropertyName("username")]
        public string Username
        {
            get;
            init;
        }

        [JsonPropertyName("password")]
        public string Password
        {
            get;
            init;
        }

        [JsonPropertyName("deviceName")]
        public string DeviceName
        {
            get;
            init;
        }

        [JsonPropertyName("isLongLived")]
        public bool IsLongLived
        {
            get;
            init;
        }

        public AuthenticateApiRequest()
        {
            Username = "";
            Password = "";
            DeviceName = "";
        }
    }

    public record AuthenticateApiResponse
    {
        [JsonPropertyName("userId")]
        public string? UserId
        {
            get;
            init;
        }
        
        [JsonPropertyName("username")]
        public string? Username
        {
            get;
            init;
        }
    }

    public record ChangePasswordApiRequest
    {
        [JsonPropertyName("oldPassword")]
        public string OldPassword
        {
            get;
            set;
        }

        [JsonPropertyName("newPassword")]
        public string NewPassword
        {
            get;
            set;
        }

        public ChangePasswordApiRequest(string oldPassword, string newPassword)
        {
            OldPassword = oldPassword;
            NewPassword = newPassword;
        }
    }
}

[JsonSerializable(typeof(AuthenticationService.AuthenticateApiRequest))]
[JsonSerializable(typeof(AuthenticationService.AuthenticateApiResponse))]
[JsonSerializable(typeof(AuthenticationService.ChangePasswordApiRequest))]
public partial class AuthenticationServiceContext : JsonSerializerContext
{
}
