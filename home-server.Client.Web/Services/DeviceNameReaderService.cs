using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace home_server.Client.Web.Services;

public interface IDeviceNameReaderService
{
    Task<string> ReadAsync();
}

public class DeviceNameReaderService : IDeviceNameReaderService, IAsyncDisposable
{
    JsLoader m_js;
    IJSObjectReference? m_module;

    public DeviceNameReaderService(JsLoader js)
    {
        m_js = js;
    }

    public async Task<string> ReadAsync()
    {
        if (m_module == null)
        {
            m_module = await m_js.LoadAsync(nameof(DeviceNameReaderService));
        }

        return await m_module.InvokeAsync<string>("getDeviceName");
    }

    public ValueTask DisposeAsync()
    {
        if (m_module != null)
        {
            return m_module.DisposeAsync();
        }
        
        return ValueTask.CompletedTask;
    }
}