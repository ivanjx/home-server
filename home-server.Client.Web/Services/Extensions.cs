using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Json.Serialization.Metadata;
using System.Threading.Tasks;

namespace home_server.Client.Web.Services;

public record ApiError(
    string Error,
    string TraceId);

public abstract record ApiResult();

public record ErrorApiResult(
    ApiError Error) :
    ApiResult;

public record SuccessApiResult() :
    ApiResult;

public record SuccessApiResult<T>(
    T? Result) :
    ApiResult;

public static class Extensions
{    
    static string GetApi(string path)
    {
#if DEBUG
        return "http://localhost:5000/api" + path;
#else
        return "/api" + path;
#endif
    }

    static async Task<ApiError> HandleErrorAsync(HttpResponseMessage response)
    {
        string? responseRaw = null;
        ApiErrorResponse? error;

        try
        {
            responseRaw = await response.Content.ReadAsStringAsync();
            error = JsonSerializer.Deserialize(
                responseRaw,
                HttpClientExtensionsContext.Default.ApiErrorResponse);
            
            if (error == null ||
                error.Error == null ||
                error.TraceId == null)
            {
                throw new Exception("Invalid error response object");
            }

            return new ApiError(
                error.Error,
                error.TraceId);
        }
        catch (Exception ex)
        {
            throw new Exception("Http request error: {0}" + responseRaw, ex);
        }
    }

    public static async Task<ApiResult> GetAsync<T>(
        this HttpClient http,
        string path,
        JsonTypeInfo<T> tinfo)
    {
        HttpResponseMessage response = await http.GetAsync(
            GetApi(path));
        
        if (!response.IsSuccessStatusCode)
        {
            ApiError error = await HandleErrorAsync(response);
            return new ErrorApiResult(error);
        }

        T? result = JsonSerializer.Deserialize(
            await response.Content.ReadAsStringAsync(),
            tinfo);
        return new SuccessApiResult<T>(result);
    }

    static string GetParameter(Dictionary<string, string> query)
    {
        StringBuilder resultBuilder = new StringBuilder();

        foreach (string key in query.Keys)
        {
            resultBuilder.AppendFormat(
                "{0}={1}&",
                key,
                Uri.EscapeDataString(query[key]));
        }

        return resultBuilder.ToString(
            0,
            resultBuilder.Length - 1);
    }

    public static async Task<ApiResult> GetAsync<T>(
        this HttpClient http,
        string path,
        Dictionary<string, string> query,
        JsonTypeInfo<T> tinfo)
    {
        HttpResponseMessage response = await http.GetAsync(
            GetApi(path) + "?" + GetParameter(query));
        
        if (!response.IsSuccessStatusCode)
        {
            ApiError error = await HandleErrorAsync(response);
            return new ErrorApiResult(error);
        }

        T? result = JsonSerializer.Deserialize(
            await response.Content.ReadAsStringAsync(),
            tinfo);
        return new SuccessApiResult<T>(result);
    }

    public static async Task<ApiResult> PostAsync(
        this HttpClient http,
        string path)
    {
        HttpResponseMessage response = await http.PostAsync(
            GetApi(path),
            null);
        
        if (!response.IsSuccessStatusCode)
        {
            ApiError error = await HandleErrorAsync(response);
            return new ErrorApiResult(error);
        }

        return new SuccessApiResult();
    }

    public static async Task<ApiResult> PostAsync<D>(
        this HttpClient http,
        string path,
        D data,
        JsonTypeInfo<D> dinfo)
    {
        HttpContent request = new StringContent(
            JsonSerializer.Serialize(data, dinfo),
            Encoding.UTF8,
            "application/json");
        HttpResponseMessage response = await http.PostAsync(
            GetApi(path),
            request);
        
        if (!response.IsSuccessStatusCode)
        {
            ApiError error = await HandleErrorAsync(response);
            return new ErrorApiResult(error);
        }

        return new SuccessApiResult();
    }

    public static async Task<ApiResult> PostAsync<T, D>(
        this HttpClient http,
        string path,
        D data,
        JsonTypeInfo<T> tinfo,
        JsonTypeInfo<D> dinfo)
    {
        HttpContent request = new StringContent(
            JsonSerializer.Serialize(data, dinfo),
            Encoding.UTF8,
            "application/json");
        HttpResponseMessage response = await http.PostAsync(
            GetApi(path),
            request);
        
        if (!response.IsSuccessStatusCode)
        {
            ApiError error = await HandleErrorAsync(response);
            return new ErrorApiResult(error);
        }
        
        T? result = JsonSerializer.Deserialize(
            await response.Content.ReadAsStringAsync(),
            tinfo);
        return new SuccessApiResult<T>(result);
    }

    public record ApiErrorResponse
    {
        [JsonPropertyName("error")]
        public string? Error
        {
            get;
            set;
        }

        [JsonPropertyName("traceId")]
        public string? TraceId
        {
            get;
            set;
        }
    }
}

[JsonSerializable(typeof(Extensions.ApiErrorResponse))]
public partial class HttpClientExtensionsContext : JsonSerializerContext
{
}
