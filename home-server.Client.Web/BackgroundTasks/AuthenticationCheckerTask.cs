using System;
using System.Threading;
using System.Threading.Tasks;
using home_server.Client.Web.Services;

namespace home_server.Client.Web.BackgroundTasks;

public class AuthenticationCheckerTask
{
    IAuthenticationService m_authenticationService;

    public AuthenticationCheckerTask(
        IAuthenticationService authenticationService)
    {
        m_authenticationService = authenticationService;
    }

    async Task StartAsync(CancellationToken cancellationToken)
    {
        Console.WriteLine("Starting authentication checker task");

        while (!cancellationToken.IsCancellationRequested)
        {
            await Task.Delay(TimeSpan.FromMinutes(1));
            await m_authenticationService.CheckAuthenticationAsync();
        }
    }

    public void Start(CancellationToken cancellationToken)
    {
        _ = StartAsync(cancellationToken);
    }
}
