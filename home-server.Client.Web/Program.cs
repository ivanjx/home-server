using home_server.Client.Web;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;
using System;
using home_server.Client.Web.Services;
using home_server.Client.Web.BackgroundTasks;

// Init.
WebAssemblyHostBuilder builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services
    // Register states.
    .AddSingleton<AuthenticationState>()

    // Register services.
    .AddSingleton(sp => new HttpClient
    {
        BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)
    })
    .AddSingleton<JsLoader>()
    .AddSingleton<IDeviceNameReaderService, DeviceNameReaderService>()
    .AddSingleton<IAuthenticationService, AuthenticationService>()
    
    // Register background tasks.
    .AddSingleton<AuthenticationCheckerTask>();

// Run.
await builder.Build().RunAsync();
