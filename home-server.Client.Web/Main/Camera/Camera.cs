using System;
using System.Text.Json.Serialization;

namespace home_server.Client.Web.Main.Camera;

public record CameraResponse
{
    [JsonPropertyName("id")]
    public string? Id
    {
        get;
        set;
    }

    [JsonPropertyName("name")]
    public string? Name
    {
        get;
        set;
    }
}
