using System;
using System.Text.Json.Serialization;

namespace home_server.Client.Web.Main.Camera;

[JsonSerializable(typeof(string[]))]
[JsonSerializable(typeof(CameraResponse))]
[JsonSerializable(typeof(CameraResponse[]))]
public partial class CameraJsonContext : JsonSerializerContext
{
}
