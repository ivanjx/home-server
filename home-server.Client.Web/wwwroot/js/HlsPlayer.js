export function initPlayer(id, url) {
    if (!Hls.isSupported()) {
        throw new Error("Hls not supported");
    }

    return new Promise((resolve, reject) => {
        const video = document.getElementById(id);
        const hls = new Hls({
            maxBufferLength: 10 * 60 // 10 minutes of buffer.
        });
        hls.attachMedia(video);
        hls.on(Hls.Events.MEDIA_ATTACHED, function () {
            hls.loadSource(url);
            hls.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
                resolve();
            });
            hls.on(Hls.Events.MANIFEST_LOAD_ERROR, function (event, data) {
                reject("Load error");
            });
            hls.on(Hls.Events.MANIFEST_PARSING_ERROR, function (event, data) {
                reject("Parse error");
            });
        });
    });
}

export function play(id, playbackRate) {
    const video = document.getElementById(id);
    video.play();
    video.focus();
    video.playbackRate = playbackRate;
}
