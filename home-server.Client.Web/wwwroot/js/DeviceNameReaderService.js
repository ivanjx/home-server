export function getDeviceName() {
    if (!"UAParser" in window) {
        throw new Error("UAParser not initialized");
    }

    let parser = new UAParser(window.navigator.userAgent);
    let browser = parser.getBrowser();
    let os = parser.getOS();

    return os.name + " " + os.version + " (" + browser.name + " " + browser.version + ")";
}
