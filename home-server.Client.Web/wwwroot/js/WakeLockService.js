let sentinel = null;

export async function lock() {
    sentinel = await navigator.wakeLock.request("screen");
}

export async function unlock() {
    await sentinel.release();
    sentinel = null;
}

document.addEventListener("visibilitychange", async () => {
    if (sentinel !== null &&
        document.visibilityState === "visible") {
        await lock();
    }
});
