const _reftable = {};
//{
//    srcBuffer
//    srcBufferResolve
//}

function getSourceBuffer(video, src, codec) {
    const srcBuffer = src.addSourceBuffer(codec);
    srcBuffer.mode = "sequence";
    srcBuffer.addEventListener(
        "updateend",
        function() {
            if (_reftable[video.id].srcBufferResolve == null) {
                console.warn("missing src buffer resolve state");
                return;
            }

            // Ready for the next buffer.
            _reftable[video.id].srcBufferResolve();
            _reftable[video.id].srcBufferResolve = null;

            if (srcBuffer.buffered.length > 0) {
                // Seek to end automatically.
                const delta = srcBuffer.buffered.end(0) - video.currentTime;

                if (delta > 1) {
                    video.currentTime += 0.5;
                }
            }
        }
    );
    return srcBuffer;
}

export function initMediaSource(videoId, codec) {
    return new Promise(function(resolve, reject) {
        // Get video element.
        const video = document.getElementById(videoId);
        
        if (video === undefined)
        {
            reject("Video element not found");
            return;
        }

        // Init media source.
        const src = new MediaSource();
        src.addEventListener(
            "sourceopen",
            function() {
                // Init media source buffer.
                const srcBuffer = getSourceBuffer(
                    video,
                    src,
                    codec
                );

                // Register to reftable.
                _reftable[video.id] = {
                    srcBuffer: srcBuffer,
                    srcBufferResolve: null
                };

                // Play video.
                video.play();

                // Init done.
                resolve();
            }
        );

        // Hook src buffer to video element.
        video.src = window.URL.createObjectURL(src);
    });
}

export function updateMediaSourceBuffer(videoId, data) {
    return new Promise(function(resolve, reject) {
        if (_reftable[videoId] === undefined) {
            reject("Video element is not initialized");
            return;
        }

        if (_reftable[videoId].srcBuffer.updating) {
            reject("Source buffer is still updating");
            return;
        }

        _reftable[videoId].srcBufferResolve = resolve; // See: getSourceBuffer updateend.
        _reftable[videoId].srcBuffer.appendBuffer(data);
    });
}

export function disposeMediaSourceBuffer(videoId) {
    delete _reftable[videoId];
}
