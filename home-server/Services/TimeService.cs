using System;
using System.Threading.Tasks;

namespace home_server.Services;

public interface ITimeService
{
    DateTime GetUtcNow();
    DateTime GetLocalNow();
    Task Delay(TimeSpan delay);
}

public class TimeService : ITimeService
{
    public Task Delay(TimeSpan delay)
    {
        return Task.Delay(delay);
    }

    public DateTime GetLocalNow()
    {
        return DateTime.Now;
    }

    public DateTime GetUtcNow()
    {
        return DateTime.UtcNow;
    }
}
