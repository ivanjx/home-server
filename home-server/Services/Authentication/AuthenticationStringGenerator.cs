using System;
using System.Security.Cryptography;
using System.Text;

namespace home_server.Services.Authentication;

public interface IAuthenticationStringGenerator
{
    /// <summary>
    /// Generates password pads that consists of 256-character string.
    /// </summary>
    /// <returns></returns>
    string GeneratePads();

    /// <summary>
    /// Generates password pads that consists of 128-character string
    /// to be used as an authentication token.
    /// </summary>
    /// <returns></returns>
    string GenerateToken();

    /// <summary>
    /// Calculates hash of an authentication token.
    /// </summary>
    /// <returns></returns>
    string HashToken(string rawToken);

    /// <summary>
    /// Calculates a hash combination of password hash and its padding.
    /// </summary>
    /// <returns></returns>
    string GeneratePasswordHash(string input, string pads);
}

public class AuthenticationStringGenerator : IAuthenticationStringGenerator
{
    public string GeneratePads()
    {
        byte[] randBuff = RandomNumberGenerator.GetBytes(128);
        return Convert.ToBase64String(randBuff);
    }

    public string GenerateToken()
    {
        byte[] randBuff = RandomNumberGenerator.GetBytes(64);
        return Convert.ToBase64String(randBuff);
    }

    public string HashToken(string rawToken)
    {
        byte[] hashBuff = SHA256.HashData(
            Encoding.ASCII.GetBytes(rawToken));
        StringBuilder resultBuilder = new StringBuilder();

        for (int i = 0; i < hashBuff.Length; ++i)
        {
            resultBuilder.AppendFormat("{0:X2}", hashBuff[i]);
        }

        return resultBuilder.ToString();
    }

    public string GeneratePasswordHash(string input, string pads)
    {
        byte[] inputBuff = Encoding.UTF8.GetBytes(
            input + pads);

        using SHA256 hash = SHA256.Create();
        byte[] hashBuff = hash.ComputeHash(inputBuff);
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < hashBuff.Length; ++i)
        {
            sb.AppendFormat("{0:X2}", hashBuff[i]);
        }

        return sb.ToString();
    }
}
