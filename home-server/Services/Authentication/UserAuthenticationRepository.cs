using System;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace home_server.Services.Authentication;

public record UserAuth(
    string UserId,
    string Hash,
    string Pads);

public interface IUserAuthenticationRepository
{
    /// <summary>
    /// Gets user authentication information (id, password hash, pads)
    /// by the username.
    /// If username is not recognized, this will return null.
    /// </summary>
    /// <returns></returns>
    Task<UserAuth?> GetByUsernameAsync(string username);

    /// <summary>
    /// Gets user authentication information (id, password hash, pads) using user id.
    /// If id is not recognized, this will return null.
    /// </summary>
    /// <returns></returns>
    Task<UserAuth?> GetByUserIdAsync(string userId);

    /// <summary>
    /// Sets user's password pads and hash.
    /// </summary>
    /// <returns></returns>
    Task SetAuthHashPadsAsync(
        string userId,
        string passwordHash,
        string passwordPads);
}

public class UserAuthenticationRepository : IUserAuthenticationRepository
{
    DbConn m_conn;

    public UserAuthenticationRepository(
        DbConn conn)
    {
        m_conn = conn;
    }

    public async Task<UserAuth?> GetByUserIdAsync(string userId)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT
                uc.password_hash,
                uc.password_pads
            FROM user_basic_credentials as uc
            WHERE user_id = $id";
        command.Parameters.AddWithValue("$id", userId);
        await command.PrepareAsync();
        using SqliteDataReader reader = await command.ExecuteReaderAsync();

        if (await reader.ReadAsync())
        {
            return new UserAuth(
                userId,
                reader.GetString(0),
                reader.GetString(1));
        }

        return null;
    }

    public async Task<UserAuth?> GetByUsernameAsync(string username)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT
                u.id,
                uc.password_hash,
                uc.password_pads
            FROM users AS u
            JOIN user_basic_credentials uc
            ON u.id == uc.user_id
            WHERE username = $username";
        command.Parameters.AddWithValue("$username", username);
        await command.PrepareAsync();
        using SqliteDataReader reader = await command.ExecuteReaderAsync();

        if (await reader.ReadAsync())
        {
            return new UserAuth(
                reader.GetString(0),
                reader.GetString(1),
                reader.GetString(2));
        }

        return null;
    }

    public async Task SetAuthHashPadsAsync(string userId, string passwordHash, string passwordPads)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            UPDATE user_basic_credentials
            SET
                password_hash = $hash,
                password_pads = $pads
            WHERE user_id = $id";
        command.Parameters.AddWithValue("$id", userId);
        command.Parameters.AddWithValue("$hash", passwordHash);
        command.Parameters.AddWithValue("$pads", passwordPads);
        await command.PrepareAsync();
        int count = await command.ExecuteNonQueryAsync();

        if (count != 1)
        {
            throw new Exception("Unable to set auth hash and pads");
        }
    }
}
