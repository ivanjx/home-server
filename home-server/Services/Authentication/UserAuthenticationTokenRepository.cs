using System;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace home_server.Services.Authentication;

public interface IUserAuthenticationTokenRepository
{
    /// <summary>
    /// Gets authentication token details by its id.
    /// </summary>
    /// <returns></returns>
    Task<UserAuthenticationToken?> GetByIdAsync(string id);

    /// <summary>
    /// Adds a new authentication token to the database.
    /// </summary>
    /// <returns></returns>
    Task<UserAuthenticationToken> AddAsync(
        UserAuthenticationToken token);
    
    /// <summary>
    /// Updates the ip address field of a token.
    /// </summary>
    /// <returns></returns>
    Task UpdateIpAddressAsync(
        string id,
        string ipAddress);
    
    /// <summary>
    /// Updates the expire time of a token if the token is still not expired.
    /// </summary>
    /// <returns></returns>
    Task UpdateExpireTimeWhenNotExpiredAsync(
        string id,
        DateTime nowTime,
        DateTime newExpireTime);
    
    /// <summary>
    /// Deletes an authentication token.
    /// </summary>
    /// <returns></returns>
    Task DeleteAsync(string id);
}

public class UserAuthenticationTokenRepository : IUserAuthenticationTokenRepository
{
    DbConn m_conn;

    public UserAuthenticationTokenRepository(DbConn dbConn)
    {
        m_conn = dbConn;
    }

    public async Task<UserAuthenticationToken> AddAsync(UserAuthenticationToken token)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            INSERT INTO user_authentication_tokens (
                id,
                user_id,
                is_long_lived,
                created_at,
                expire_at,
                device_name,
                ip_address)
            VALUES (
                $id,
                $userId,
                $isLongLived,
                $createdAt,
                $expireAt,
                $deviceName,
                $ipAddress)";
        command.Parameters.AddWithValue("$id", token.Token);
        command.Parameters.AddWithValue("$userId", token.UserId);
        command.Parameters.AddWithValue(
            "$isLongLived",
            token.IsLongLived ? 1 : 0);
        command.Parameters.AddWithValue(
            "$createdAt",
            new DateTimeOffset(token.CreatedAt).ToUnixTimeSeconds());
        command.Parameters.AddWithValue(
            "$expireAt",
            new DateTimeOffset(token.ExpireAt).ToUnixTimeSeconds());
        command.Parameters.AddWithValue("$deviceName", token.DeviceName);
        command.Parameters.AddWithValue("$ipAddress", token.IpAddress);
        await command.PrepareAsync();
        int count = await command.ExecuteNonQueryAsync();

        if (count == 0)
        {
            throw new Exception("Unable to add user authentication token");
        }

        return token;
    }

    public async Task DeleteAsync(string id)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = "DELETE FROM user_authentication_tokens WHERE id=$id;";
        command.Parameters.AddWithValue("$id", id);
        await command.PrepareAsync();
        await command.ExecuteNonQueryAsync();
    }

    public async Task<UserAuthenticationToken?> GetByIdAsync(string id)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT
                uat.id,
                uat.is_long_lived,
                uat.created_at,
                uat.expire_at,
                uat.device_name,
                uat.ip_address,
                uat.user_id,
                u.username
            FROM user_authentication_tokens AS uat
            INNER JOIN users AS u
            ON u.id = uat.user_id
            WHERE uat.id = $id";
        command.Parameters.AddWithValue("$id", id);
        command.Prepare();
        using SqliteDataReader reader = command.ExecuteReader();

        if (await reader.ReadAsync())
        {
            UserAuthenticationToken result = new UserAuthenticationToken(
                reader.GetString(0), // Token.
                reader.GetInt32(1) == 1, // Is long lived.
                DateTimeOffset.FromUnixTimeSeconds(reader.GetInt64(2)).UtcDateTime, // Created at.
                DateTimeOffset.FromUnixTimeSeconds(reader.GetInt64(3)).UtcDateTime, // Expire at.
                reader.GetString(4), // Device name.
                reader.GetString(5), // Ip address.
                reader.GetString(6), // User id.
                reader.GetString(7)); // Username.
            return result;
        }

        return null;
    }

    public async Task UpdateIpAddressAsync(string id, string ipAddress)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            UPDATE user_authentication_tokens
            SET ip_address=$ip
            WHERE id=$id";
        command.Parameters.AddWithValue("$id", id);
        command.Parameters.AddWithValue("$ip", ipAddress);
        await command.PrepareAsync();
        int count = await command.ExecuteNonQueryAsync();

        if (count == 0)
        {
            throw new Exception("Unable to set ip address");
        }
    }

    public async Task UpdateExpireTimeWhenNotExpiredAsync(
        string id,
        DateTime nowTime,
        DateTime newExpireTime)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            UPDATE user_authentication_tokens
            SET expire_at = $newExpireTime
            WHERE
                id = $id AND
                expire_at > $nowTime";
        command.Parameters.AddWithValue("$id", id);
        command.Parameters.AddWithValue(
            "$newExpireTime",
            new DateTimeOffset(newExpireTime).ToUnixTimeSeconds());
        command.Parameters.AddWithValue(
            "$nowTime",
            new DateTimeOffset(nowTime).ToUnixTimeSeconds());
        await command.PrepareAsync();
        await command.ExecuteNonQueryAsync();
    }
}
