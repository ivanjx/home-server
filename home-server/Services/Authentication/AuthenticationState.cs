using System;
using System.Threading;

namespace home_server.Services.Authentication;

public record AuthenticationUser(
    string Id,
    string Username);

public interface IAuthenticationState
{
    /// <summary>
    /// Gets current context's authentication token.
    /// This should only be accessed inside controllers with
    /// <see cref="Controllers.AuthenticateAttribute"/>.
    /// </summary>
    string RawToken
    {
        get;
    }

    /// <summary>
    /// Gets current context's basic user information.
    /// This should only be accessed inside controllers with
    /// <see cref="Controllers.AuthenticateAttribute"/>.
    /// </summary>
    AuthenticationUser User
    {
        get;
    }
}

public class AuthenticationState : IAuthenticationState
{
    AsyncLocal<string?> m_rawToken;
    AsyncLocal<AuthenticationUser?> m_user;

    public string RawToken
    {
        get =>
            m_rawToken.Value ??
            throw new Exception("Token is null"); // Impossible.
        set => m_rawToken.Value = value;
    }

    public AuthenticationUser User
    {
        get =>
            m_user.Value ??
            throw new Exception("User is null"); // Impossible.
        set => m_user.Value = value;
    }

    public AuthenticationState()
    {
        m_rawToken = new AsyncLocal<string?>();
        m_user = new AsyncLocal<AuthenticationUser?>();
    }
}
