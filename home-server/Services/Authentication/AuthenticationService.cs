using System;
using System.Security;
using System.Threading.Tasks;
using Serilog;

namespace home_server.Services.Authentication;

public record UserAuthenticationToken(
    string Token,
    bool IsLongLived,
    DateTime CreatedAt,
    DateTime ExpireAt,
    string DeviceName,
    string IpAddress,
    string UserId,
    string Username);

public abstract record AuthenticationResult();
public record InvalidAuthenticationResult()
    : AuthenticationResult;
public record InvalidTokenAuthentcationResult()
    : AuthenticationResult;
public record SuccessAuthenticationResult(
    UserAuthenticationToken Token,
    string RawToken)
    : AuthenticationResult;
public record SuccessUpdatedAuthenticationResult(
    UserAuthenticationToken Token,
    string RawToken)
    : SuccessAuthenticationResult(
        Token,
        RawToken);

public interface IAuthenticationService
{
    /// <summary>
    /// Creates a new token if provided username and password are valid.
    /// Long lived tokens age are 60 days, short lived ones are 1 hour.
    /// </summary>
    /// <returns></returns>
    Task<AuthenticationResult> AuthenticateAsync(
        string username,
        string password,
        string deviceName,
        string ipAddress,
        bool isLongLived);
    
    /// <summary>
    /// Checks if token is valid.
    /// </summary>
    /// <returns></returns>
    Task<AuthenticationResult> AuthenticateAsync(
        string tokenId,
        string ipAddress);
    
    /// <summary>
    /// Deletes an authentication token.
    /// </summary>
    /// <returns></returns>
    Task DeauthenticateAsync(string tokenId);

    /// <summary>
    /// Sets new password hash and pads based on the provided new password.
    /// Old password will be validated first. This will return false if old password is wrong.
    /// </summary>
    /// <returns></returns>
    Task<bool> ChangePasswordAsync(
        string userId,
        string oldPassword,
        string newPassword);
}

public class AuthenticationService : IAuthenticationService
{
    const int LONG_TOKEN_DAYS = 60;
    const int SHORT_TOKEN_HOURS = 1;

    IUserAuthenticationRepository m_authenticationRepository;
    IUserAuthenticationTokenRepository m_authenticationTokenRepository;
    IAuthenticationStringGenerator m_authenticationStringGenerator;
    ITimeService m_timeService;
    ILogger m_logger;

    public AuthenticationService(
        IUserAuthenticationRepository authenticationRepository,
        IUserAuthenticationTokenRepository authenticationTokenRepository,
        IAuthenticationStringGenerator randomStringGeneratorService,
        ITimeService timeService,
        ILogger logger)
    {
        m_authenticationRepository = authenticationRepository;
        m_authenticationTokenRepository = authenticationTokenRepository;
        m_authenticationStringGenerator = randomStringGeneratorService;
        m_timeService = timeService;
        m_logger = logger.ForContext<AuthenticationService>();
    }

    public async Task<AuthenticationResult> AuthenticateAsync(
        string username,
        string password,
        string deviceName,
        string ipAddress,
        bool isLongLived)
    {
        ILogger logger = m_logger.ForContext("username", username);
        logger.Information("Authenticating user");
        UserAuth? auth =
            await m_authenticationRepository.GetByUsernameAsync(username);

        if (auth == null)
        {
            logger.Error("Username not found");
            return new InvalidAuthenticationResult();
        }

        // Checking password input against password hash.
        string inputPasswordHash = m_authenticationStringGenerator.GeneratePasswordHash(
            password,
            auth.Pads);
        
        if (inputPasswordHash != auth.Hash)
        {
            logger.Error("Wrong password");
            return new InvalidAuthenticationResult();
        }
        
        logger.Information("Creating authentication token");
        DateTime now = m_timeService.GetUtcNow();
        DateTime expireTime = isLongLived ?
            now.AddDays(LONG_TOKEN_DAYS) :
            now.AddHours(SHORT_TOKEN_HOURS);
        string rawToken = m_authenticationStringGenerator.GenerateToken();
        deviceName = SecurityElement.Escape(deviceName);
        ipAddress = SecurityElement.Escape(ipAddress);
        UserAuthenticationToken token = new UserAuthenticationToken(
            m_authenticationStringGenerator.HashToken(rawToken),
            isLongLived,
            now,
            expireTime,
            deviceName,
            ipAddress,
            auth.UserId,
            username);
        token = await m_authenticationTokenRepository.AddAsync(token);
        logger = logger.ForContext("token", token);
        logger.Information("Authentication token created");

        // Done.
        return new SuccessAuthenticationResult(token, rawToken);
    }

    bool IsTokenNeedToExtend(
        UserAuthenticationToken token,
        out DateTime newExpireTime)
    {
        // Token needs to be extended after at least a half of its lifetime.
        TimeSpan expireDelta = token.ExpireAt - m_timeService.GetUtcNow();
        TimeSpan longHalf = TimeSpan.FromDays(LONG_TOKEN_DAYS / 2.0);
        TimeSpan shortHalf = TimeSpan.FromHours(SHORT_TOKEN_HOURS / 2.0);

        if (token.IsLongLived && expireDelta <= longHalf)
        {
            newExpireTime = m_timeService.GetUtcNow().AddDays(LONG_TOKEN_DAYS);
            return true;
        }
        else if (!token.IsLongLived && expireDelta <= shortHalf)
        {
            newExpireTime = m_timeService.GetUtcNow().AddHours(SHORT_TOKEN_HOURS);
            return true;
        }
        else
        {
            newExpireTime = default;
            return false;
        }
    }

    public async Task<AuthenticationResult> AuthenticateAsync(string tokenId, string ipAddress)
    {
        string rawToken = tokenId;
        tokenId = m_authenticationStringGenerator.HashToken(tokenId);
        UserAuthenticationToken? token = await m_authenticationTokenRepository.GetByIdAsync(tokenId);

        if (token == null)
        {
            m_logger
                .ForContext("tokenId", tokenId)
                .Information("Token not found");
            return new InvalidTokenAuthentcationResult();
        }

        ILogger logger = m_logger.ForContext("token", token);

        if (token.ExpireAt <= m_timeService.GetUtcNow())
        {
            logger.Information("Token has expired. Deleting token");
            
            try
            {
                await m_authenticationTokenRepository.DeleteAsync(token.Token);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unable to delete expired token");
            }

            return new InvalidTokenAuthentcationResult();
        }

        bool updated = false;

        if (IsTokenNeedToExtend(token, out DateTime newExpireTime))
        {
            logger
                .ForContext("newExpireTime", newExpireTime)
                .Information("Extending token expire time");
            await m_authenticationTokenRepository.UpdateExpireTimeWhenNotExpiredAsync(
                token.Token,
                m_timeService.GetUtcNow(),
                newExpireTime);
            token = token with
            {
                ExpireAt = newExpireTime
            };
            updated = true;
        }

        ipAddress = SecurityElement.Escape(ipAddress);

        if (token.IpAddress != ipAddress)
        {
            logger
                .ForContext("newIpAddress", ipAddress)
                .Information("Updating token IP address");
            await m_authenticationTokenRepository.UpdateIpAddressAsync(
                token.Token,
                ipAddress);
            token = token with
            {
                IpAddress = ipAddress
            };
            updated = true;
        }

        if (updated)
        {
            return new SuccessUpdatedAuthenticationResult(token, rawToken);
        }

        return new SuccessAuthenticationResult(token, rawToken);
    }

    public async Task DeauthenticateAsync(string tokenId)
    {
        try
        {
            await m_authenticationTokenRepository.DeleteAsync(tokenId);
        }
        catch (Exception ex)
        {
            m_logger
                .ForContext("tokenId", tokenId)
                .Error(ex, "Unable to delete token");
        }
    }

    public async Task<bool> ChangePasswordAsync(string userId, string oldPassword, string newPassword)
    {
        ILogger log = m_logger.ForContext("userId", userId);
        log.Information("Validating old password");
        UserAuth? auth = await m_authenticationRepository.GetByUserIdAsync(userId);

        if (auth == null)
        {
            throw new InvalidParameterException("Invalid user id");
        }

        string oldHash = m_authenticationStringGenerator.GeneratePasswordHash(
            oldPassword,
            auth.Pads);
        
        if (oldHash != auth.Hash)
        {
            log.Information("Wrong old password");
            return false;
        }

        log.Information("Saving new auth information");
        string newPads = m_authenticationStringGenerator.GeneratePads();
        string newHash = m_authenticationStringGenerator.GeneratePasswordHash(
            newPassword,
            newPads);
        await m_authenticationRepository.SetAuthHashPadsAsync(
            userId,
            newHash,
            newPads);
        
        return true;
    }
}
