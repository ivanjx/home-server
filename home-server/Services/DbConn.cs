using System;
using System.Threading.Tasks;
using home_server.Services;
using Microsoft.Data.Sqlite;

namespace home_server.Services;

public class DbConn
{
    IConfigurationService m_configurationService;

    public DbConn(
        IConfigurationService configurationService)
    {
        m_configurationService = configurationService;
    }

    public async Task<SqliteConnection> CreateAsync()
    {
        SqliteConnection conn = new SqliteConnection(
            string.Format(
                "Data Source={0};",
                m_configurationService.DatabasePath));
        
        try
        {
            await conn.OpenAsync();
            return conn;
        }
        catch
        {
            conn.Dispose();
            throw;
        }
    }

    public async Task InitAsync()
    {
        string query = @"
CREATE TABLE IF NOT EXISTS `cameras` (
	`id`			TEXT NOT NULL,
	`name`			TEXT NOT NULL UNIQUE,
	`rtsp_url`		TEXT NOT NULL UNIQUE,
    `created_at`    INTEGER NOT NULL,
	PRIMARY KEY		(`id`)
);

CREATE TABLE IF NOT EXISTS `users` (
	`id`			TEXT NOT NULL,
	`username`		TEXT NOT NULL UNIQUE,
    `created_at`    INTEGER NOT NULL,
	PRIMARY KEY		(`id`)
);

CREATE TABLE IF NOT EXISTS `user_basic_credentials` (
    `user_id` 		TEXT NOT NULL UNIQUE,
    `password_hash` TEXT NOT NULL UNIQUE,
    `password_pads` TEXT NOT NULL UNIQUE,
    FOREIGN KEY		(`user_id`) REFERENCES `users`(`id`)
);

CREATE TABLE IF NOT EXISTS `user_authentication_tokens` (
	`id` 			TEXT NOT NULL,
	`user_id`		TEXT NOT NULL,
	`is_long_lived` INTEGER NOT NULL,
	`created_at`	INTEGER NOT NULL,
	`expire_at`	    INTEGER NOT NULL,
	`device_name`	TEXT NOT NULL,
	`ip_address` 	TEXT NOT NULL,
	PRIMARY KEY		(`id`),
	FOREIGN KEY		(`user_id`) REFERENCES `users`(`id`)
);
CREATE INDEX IF NOT EXISTS
	`user_authentication_tokens_user_id`
	ON `user_authentication_tokens` (`user_id`, `expire_at` DESC);
";
        using SqliteConnection conn = await CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = query;
        await command.ExecuteNonQueryAsync();
    }
}
