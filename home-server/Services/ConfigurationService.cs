using System;

namespace home_server.Services;

public interface IConfigurationService
{
    string BasePath
    {
        get;
    }

    string ListenEndpoint
    {
        get;
    }

    string DatabasePath
    {
        get;
    }

    string? SeqEndpoint
    {
        get;
    }

    string? SeqApiKey
    {
        get;
    }

    int RtspListenMinPort
    {
        get;
    }

    int RtspListenMaxPort
    {
        get;
    }

    string CameraArchivalDir
    {
        get;
    }

    int CameraArchivalDays
    {
        get;
    }

    string CacheDir
    {
        get;
    }
}

public class ConfigurationService : IConfigurationService
{
    public string BasePath
    {
        get
        {
            return "/api";
        }
    }

    public string ListenEndpoint
    {
        get;
        private set;
    }

    public string DatabasePath
    {
        get;
        private set;
    }

    public string? SeqEndpoint
    {
        get;
        private set;
    }

    public string? SeqApiKey
    {
        get;
        private set;
    }

    public int RtspListenMinPort
    {
        get;
        private set;
    }

    public int RtspListenMaxPort
    {
        get;
        private set;
    }

    public string CameraArchivalDir
    {
        get;
        private set;
    }

    public int CameraArchivalDays
    {
        get;
        private set;
    }

    public string CacheDir
    {
        get;
        private set;
    }

    string GetEnv(string env)
    {
        string? result = Environment.GetEnvironmentVariable(env);

        if (string.IsNullOrEmpty(result))
        {
            throw new Exception("Environment variable is undefined: " + env);
        }

        return result;
    }

    int GetEnvInt(string env)
    {
        string str = GetEnv(env);
        return int.Parse(str);
    }

    public ConfigurationService()
    {
        ListenEndpoint = GetEnv("HOME_SERVER_LISTEN_ENDPOINT");
        DatabasePath = GetEnv("HOME_SERVER_DB_PATH");
        SeqEndpoint = Environment.GetEnvironmentVariable("HOME_SERVER_SEQ_ENDPOINT");
        SeqApiKey = Environment.GetEnvironmentVariable("HOME_SERVER_SEQ_API_KEY");
        RtspListenMinPort = GetEnvInt("HOME_SERVER_CAMERA_RTSP_LISTEN_MIN_PORT");
        RtspListenMaxPort = GetEnvInt("HOME_SERVER_CAMERA_RTSP_LISTEN_MAX_PORT");
        CameraArchivalDir = GetEnv("HOME_SERVER_CAMERA_ARCHIVAL_DIR");
        CameraArchivalDays = GetEnvInt("HOME_SERVER_CAMERA_ARCHIVAL_DAYS");
        CacheDir = GetEnv("HOME_SERVER_CACHE_DIR");
    }
}
