using System;

namespace home_server.Services;

public class InvalidParameterException : Exception
{
    public InvalidParameterException(string message) : base(message)
    {
    }
}
