using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using home_server.Services.Authentication;
using Serilog;

namespace home_server.Services.Users;

public abstract record RegistrationResult();
public record DuplicateUsernameRegistrationResult()
    : RegistrationResult;
public record SuccessRegistrationResult(
    string UserId)
    : RegistrationResult;

public interface IUserRegistrationService
{
    /// <summary>
    /// Registers a new user with username and password.
    /// This will automatically hash the password for security.
    /// </summary>
    /// <returns></returns>
    Task<RegistrationResult> RegisterAsync(
        string username,
        string password);
}

public partial class UserRegistrationService : IUserRegistrationService
{
    IUserRepository m_userRepository;
    IAuthenticationStringGenerator m_authenticationStringGenerator;
    ITimeService m_timeService;
    ILogger m_logger;

    public UserRegistrationService(
        IUserRepository userRepository,
        IAuthenticationStringGenerator authenticationStringGenerator,
        ITimeService timeService,
        ILogger logger)
    {
        m_userRepository = userRepository;
        m_authenticationStringGenerator = authenticationStringGenerator;
        m_timeService = timeService;
        m_logger = logger.ForContext<UserRegistrationService>();
    }

    [GeneratedRegex(@"^[a-zA-Z0-9]{3,20}$")]
    private partial Regex UsernameRegex();

    public async Task<RegistrationResult> RegisterAsync(string username, string password)
    {
        if (!UsernameRegex().IsMatch(username))
        {
            throw new InvalidParameterException("Invalid username");
        }

        ILogger logger = m_logger
            .ForContext("username", username);
        logger.Information("Checking for duplicate username");

        if (await m_userRepository.IsUsernameExistsAsync(username))
        {
            return new DuplicateUsernameRegistrationResult();
        }

        logger.Information("Adding to user repository");
        string passwordPads = m_authenticationStringGenerator.GeneratePads();
        string passwordHash = m_authenticationStringGenerator.GeneratePasswordHash(
            password,
            passwordPads);
        string newUserId = await m_userRepository.AddAsync(
            username,
            passwordHash,
            passwordPads,
            m_timeService.GetUtcNow());
        return new SuccessRegistrationResult(newUserId);
    }
}
