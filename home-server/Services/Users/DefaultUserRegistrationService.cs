using System;
using System.Threading.Tasks;
using Serilog;

namespace home_server.Services.Users;

public interface IDefaultUserRegistrationService
{
    /// <summary>
    /// Creates a new user if no users exist with default username and password.
    /// </summary>
    /// <returns></returns>
    Task CreateDefaultUserAsync();
}

public class DefaultUserRegistrationService : IDefaultUserRegistrationService
{
    const string DEFAULT_USERNAME = "admin";
    const string DEFAULT_PASSWORD = "admin";

    IUserRepository m_userRepository;
    IUserRegistrationService m_userRegistrationService;
    ILogger m_logger;

    public DefaultUserRegistrationService(
        IUserRepository userRepository,
        IUserRegistrationService userRegistrationService,
        ILogger logger)
    {
        m_userRepository = userRepository;
        m_userRegistrationService = userRegistrationService;
        m_logger = logger.ForContext<DefaultUserRegistrationService>();
    }

    public async Task CreateDefaultUserAsync()
    {
        m_logger.Information("Checking user count");

        if (await m_userRepository.CountAsync() > 0)
        {
            m_logger.Information("One or more account(s) already exists");
            return;
        }

        m_logger.Information("Creating default user account");
        RegistrationResult result = await m_userRegistrationService.RegisterAsync(
            DEFAULT_USERNAME,
            DEFAULT_PASSWORD);
        
        if (result is not SuccessRegistrationResult successResult)
        {
            // Impossible.
            throw new Exception("Unable to create default user");
        }

        m_logger
            .ForContext("id", successResult.UserId)
            .Information("Created default user account");
    }
}
