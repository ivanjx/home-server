using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace home_server.Services.Users;

public interface IUserRepository
{
    /// <summary>
    /// Counts all available users.
    /// </summary>
    /// <returns></returns>
    Task<long> CountAsync();

    /// <summary>
    /// Checks whether username exists or not.
    /// </summary>
    /// <param name="username"></param>
    /// <returns></returns>
    Task<bool> IsUsernameExistsAsync(string username);

    /// <summary>
    /// Adds a new user (and its auth info) into the database.
    /// This will return the new user id.
    /// </summary>
    /// <returns></returns>
    Task<string> AddAsync(
        string username,
        string passwordHash,
        string passwordPads,
        DateTime now);
}

public class UserRepository : IUserRepository
{
    DbConn m_conn;

    public UserRepository(DbConn conn)
    {
        m_conn = conn;
    }

    public async Task<string> AddAsync(string username, string passwordHash, string passwordPads, DateTime now)
    {
        Guid id = Guid.NewGuid();
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteTransaction transaction = conn.BeginTransaction();

        try
        {
            using SqliteCommand userCommand = conn.CreateCommand();
            userCommand.Transaction = transaction;
            userCommand.CommandText = @"
                INSERT INTO users (
                    id,
                    username,
                    created_at
                )
                VALUES (
                    $id,
                    $username,
                    $createdAt
                )";
            userCommand.Parameters.AddWithValue("$id", id.ToString());
            userCommand.Parameters.AddWithValue("$username", username);
            userCommand.Parameters.AddWithValue(
                "$createdAt",
                new DateTimeOffset(now).ToUnixTimeSeconds());
            await userCommand.PrepareAsync();
            int count = await userCommand.ExecuteNonQueryAsync();

            if (count != 1)
            {
                throw new Exception("Unable to add user");
            }

            using SqliteCommand authCommand = conn.CreateCommand();
            authCommand.Transaction = transaction;
            authCommand.CommandText = @"
                INSERT INTO user_basic_credentials (
                    user_id,
                    password_hash,
                    password_pads
                )
                VALUES (
                    $id,
                    $hash,
                    $pads
                )";
            authCommand.Parameters.AddWithValue("$id", id.ToString());
            authCommand.Parameters.AddWithValue("$hash", passwordHash);
            authCommand.Parameters.AddWithValue("$pads", passwordPads);
            await authCommand.PrepareAsync();
            count = await authCommand.ExecuteNonQueryAsync();

            if (count != 1)
            {
                throw new Exception("Unable to add user basic credential");
            }

            await transaction.CommitAsync();
            return id.ToString();
        }
        catch
        {
            await transaction.RollbackAsync();
            throw;
        }
    }

    public async Task<long> CountAsync()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT COUNT(*)
            FROM users";
        object? result = await command.ExecuteScalarAsync();

        if (result == null)
        {
            throw new Exception("Unable to count");
        }

        return (long)result;
    }

    public async Task<bool> IsUsernameExistsAsync(string username)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT COUNT(*)
            FROM users
            WHERE username=$username";
        command.Parameters.AddWithValue("$username", username);
        await command.PrepareAsync();
        object? result = await command.ExecuteScalarAsync();

        if (result == null)
        {
            throw new Exception("Unable to count");
        }

        return (long)result != 0;
    }
}
