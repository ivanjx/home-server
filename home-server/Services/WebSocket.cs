using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using WS = System.Net.WebSockets.WebSocket;

namespace home_server.Services;

public interface IWebSocket : IDisposable
{
    string ConnectionId
    {
        get;
    }

    bool IsOpen
    {
        get;
    }
    
    Task SendAsync(
        Memory<byte> data,
        CancellationToken cancellationToken);
}

public class WebSocket : IWebSocket
{
    const int TIMEOUT_SECS = 30;

    WS m_socket;

    public string ConnectionId
    {
        get;
    }

    public bool IsOpen
    {
        get => m_socket.State == WebSocketState.Open;
    }

    public WebSocket(WS socket, string connectionId)
    {
        ConnectionId = connectionId;
        m_socket = socket;
    }

    public async Task SendAsync(
        Memory<byte> data,
        CancellationToken cancellationToken)
    {
        if (!IsOpen)
        {
            throw new Exception("Socket already closed");
        }
        
        CancellationTokenSource cts = CancellationTokenSource.CreateLinkedTokenSource(
            cancellationToken);
        Task sendTask = m_socket.SendAsync(
            data,
            WebSocketMessageType.Binary,
            true,
            cts.Token)
            .AsTask();
        await Task.WhenAny(
            sendTask,
            Task.Delay(
                TimeSpan.FromSeconds(TIMEOUT_SECS),
                cts.Token));
        
        if (!sendTask.IsCompleted)
        {
            cts.Cancel();
            throw new TimeoutException();
        }
    }

    public async void Dispose()
    {
        if (IsOpen)
        {
            try
            {
                await m_socket.CloseAsync(
                    WebSocketCloseStatus.Empty,
                    null,
                    default);
            }
            catch { }
        }

        m_socket.Dispose();
    }
}
