using System;
using System.IO;

namespace home_server.Services.Cameras;

public interface ICameraRecordingProcessSourceService
{
    void CreateDirectory(string path);
}

public class CameraRecordingProcessSourceService : ICameraRecordingProcessSourceService
{
    public void CreateDirectory(string path)
    {
        Directory.CreateDirectory(path);
    }
}
