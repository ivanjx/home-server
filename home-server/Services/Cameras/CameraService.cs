using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Serilog;

namespace home_server.Services.Cameras;

public abstract record CreateCameraResult();
public record DuplicateUrlCreateCameraResult()
    : CreateCameraResult;
public record SuccessCreateCameraResult(
    Camera Camera)
    : CreateCameraResult;

public abstract record UpdateCameraResult();
public record DuplicateUrlUpdateCameraResult()
    : UpdateCameraResult;
public record SuccessUpdateCameraResult()
    : UpdateCameraResult;

public interface ICameraService
{
    Task<CreateCameraResult> CreateAsync(
        string name,
        string url);
    Task<UpdateCameraResult> UpdateAsync(Camera camera);
    Task DeleteAsync(string cameraId);
    Task StartAllRecordingAsync();
}

public class CameraService : ICameraService
{
    ICameraRepository m_cameraRepository;
    ICameraRecordingService m_cameraRecordingService;
    ICameraRecordingArchivalService m_cameraRecordingArchivalService;
    ILogger m_logger;

    public CameraService(
        ICameraRepository cameraRepository,
        ICameraRecordingService cameraRecordingService,
        ICameraRecordingArchivalService cameraRecordingArchivalService,
        ILogger logger)
    {
        m_cameraRepository = cameraRepository;
        m_cameraRecordingService = cameraRecordingService;
        m_cameraRecordingArchivalService = cameraRecordingArchivalService;
        m_logger = logger.ForContext<CameraService>();
    }

    public async Task<CreateCameraResult> CreateAsync(string name, string url)
    {
        name = name.Trim();
        url = url.Trim();

        if (!url.StartsWith("rtsp://"))
        {
            throw new InvalidParameterException("Invalid RTSP url");
        }
        
        if (await m_cameraRepository.IsRtspUrlExistsAsync(url))
        {
            return new DuplicateUrlCreateCameraResult();
        }

        ILogger logger = m_logger.ForContext("camera", name);
        logger.Information("Adding camera to the database");
        Camera camera = await m_cameraRepository.AddAsync(name, url);

        logger.Information("Starting camera recording");
        m_cameraRecordingService.Start(camera);

        return new SuccessCreateCameraResult(camera);
    }

    public async Task<UpdateCameraResult> UpdateAsync(Camera camera)
    {
        // Validating.
        camera = camera with
        {
            Name = camera.Name.Trim(),
            RtspUrl = camera.RtspUrl.Trim()
        };

        if (!camera.RtspUrl.StartsWith("rtsp://"))
        {
            throw new InvalidParameterException("Invalid RTSP url");
        }

        Camera? originalCamera = await m_cameraRepository.GetByIdAsync(camera.Id);

        if (originalCamera == null)
        {
            throw new InvalidParameterException("Invalid camera id");
        }

        if (originalCamera.RtspUrl != camera.RtspUrl &&
            await m_cameraRepository.IsRtspUrlExistsAsync(camera.RtspUrl))
        {
            return new DuplicateUrlUpdateCameraResult();
        }

        ILogger logger = m_logger
            .ForContext("camera", originalCamera)
            .ForContext("update", camera);
        logger.Information("Updating camera in the database");
        await m_cameraRepository.UpdateAsync(camera);

        if (originalCamera.RtspUrl != camera.RtspUrl)
        {
            logger.Information("Restarting camera recording");
            await m_cameraRecordingService.StopAsync(originalCamera.Id);
            m_cameraRecordingService.Start(camera);
        }

        return new SuccessUpdateCameraResult();
    }

    public async Task DeleteAsync(string cameraId)
    {
        Camera? camera = await m_cameraRepository.GetByIdAsync(cameraId);

        if (camera == null)
        {
            throw new InvalidParameterException("Invalid camera id");
        }

        ILogger logger = m_logger.ForContext("camera", camera);
        
        try
        {
            logger.Information("Stopping camera recording");
            await m_cameraRecordingService.StopAsync(camera.Id);
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Unable to stop camera recording");
        }

        logger.Information("Removing camera from the database");
        await m_cameraRepository.DeleteAsync(camera.Id);

        logger.Information("Removing camera archives");
        await m_cameraRecordingArchivalService.RemoveArchivesAsync(camera.Id);
    }

    public async Task StartAllRecordingAsync()
    {
        IEnumerable<Camera> cameras = await m_cameraRepository.ListAllAsync();

        foreach (Camera camera in cameras)
        {
            m_logger
                .ForContext("camera", camera)
                .Information("Starting camera recording");
            m_cameraRecordingService.Start(camera);
        }
    }
}
