using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace home_server.Services.Cameras;

public interface ICameraRecordingArchivalService
{
    /// <summary>
    /// Creates a new archive of the past 1 hour of the camera stream.
    /// This is only called by a background worker/service.
    /// </summary>
    /// <returns></returns>
    Task CreateArchivesAsync();

    /// <summary>
    /// Deletes old camera archives.
    /// This is only called by a background worker/service.
    /// </summary>
    /// <returns></returns>
    Task CleanArchivesAsync();

    /// <summary>
    /// Lists all available archive names (hourly) of a camera.
    /// The result is sorted in a descending order.
    /// </summary>
    /// <returns></returns>
    Task<string[]> ListArchivesAsync(string cameraId);

    /// <summary>
    /// Deletes all archives of a camera.
    /// </summary>
    /// <returns></returns>
    Task RemoveArchivesAsync(string cameraId);

    /// <summary>
    /// Gets raw archive file (.m3u8) content of a camera.
    /// </summary>
    /// <returns></returns>
    Task<string?> GetArchiveIndexAsync(string cameraId, string indexName);

    /// <summary>
    /// Gets the real full path of a camera archive chunk (.ts).
    /// </summary>
    /// <returns></returns>
    string? GetArchiveChunkPath(string cameraId, string chunkName);
}

public class CameraRecordingArchivalService : ICameraRecordingArchivalService
{
    const int ARCHIVE_AGE_BUFFER_HOURS = 2;
    const string ENDLIST = "#EXT-X-ENDLIST";

    ICameraRepository m_cameraRepository;
    IConfigurationService m_configurationService;
    ITimeService m_timeService;
    ICameraRecordingArchivalSourceService m_sourceService;
    ILogger m_logger;

    public CameraRecordingArchivalService(
        ICameraRepository cameraRepository,
        IConfigurationService configurationService,
        ITimeService timeService,
        ICameraRecordingArchivalSourceService sourceService,
        ILogger logger)
    {
        m_cameraRepository = cameraRepository;
        m_configurationService = configurationService;
        m_timeService = timeService;
        m_sourceService = sourceService;
        m_logger = logger.ForContext<CameraRecordingArchivalService>();
    }

    public async Task CreateArchivesAsync()
    {
        IEnumerable<Camera> cameras = await m_cameraRepository.ListAllAsync();
        DateTime now = m_timeService.GetUtcNow();
        DateTime localNow = m_timeService.GetLocalNow().AddSeconds(-15);

        foreach (Camera camera in cameras)
        {
            ILogger logger = m_logger.ForContext("camera", camera);
            string indexPath =
                Path.Combine(
                    m_configurationService.CameraArchivalDir,
                    camera.Id,
                    "index.m3u8")
                .Replace("\\", "/");

            if (!m_sourceService.Exists(indexPath))
            {
                logger.Error("Index file camera does not exist");
                continue;
            }

            TimeSpan indexAge = now - m_sourceService.GetModifyTime(indexPath);

            if (indexAge > TimeSpan.FromMinutes(1))
            {
                logger.Error("Camera recording index too old: {0}", indexAge);
                continue;
            }

            string index = await m_sourceService.ReadAsync(indexPath);
            StringBuilder indexBuilder = new StringBuilder();
            indexBuilder.Append(index);

            if (!index.TrimEnd().EndsWith(ENDLIST))
            {
                indexBuilder.AppendLine(ENDLIST);
            }
            
            string archiveIndexPath =
                Path.Combine(
                    m_configurationService.CameraArchivalDir,
                    camera.Id,
                    "lists",
                    localNow.ToString("yyyyMMdd-HH") + ".m3u8")
                .Replace("\\", "/");
            await m_sourceService.WriteAsync(
                archiveIndexPath,
                indexBuilder.ToString());
        }
    }

    async Task<string[]> ListChunksAsync(string archivePath)
    {
        string list = await m_sourceService.ReadAsync(archivePath);
        StringReader reader = new StringReader(list);
        List<string> result = new List<string>();

        while (true)
        {
            string? line = await reader.ReadLineAsync();

            if (line == null)
            {
                break;
            }

            if (line.EndsWith(".ts"))
            {
                result.Add(Path.GetFileName(line));
            }
        }

        return result.ToArray();
    }

    public async Task CleanArchivesAsync()
    {
        IEnumerable<Camera> cameras = await m_cameraRepository.ListAllAsync();
        DateTime now = m_timeService.GetUtcNow();

        foreach (Camera camera in cameras)
        {
            string recordingDir =
                Path.Combine(
                    m_configurationService.CameraArchivalDir,
                    camera.Id)
                .Replace("\\", "/");
            string archiveDir = Path
                .Combine(recordingDir, "lists")
                .Replace("\\", "/");
            string[] archivePaths = m_sourceService.List(archiveDir);

            TimeSpan maxAge = TimeSpan
                .FromDays(m_configurationService.CameraArchivalDays)
                .Add(TimeSpan.FromHours(ARCHIVE_AGE_BUFFER_HOURS));
            archivePaths = archivePaths
                .Where(x =>
                {
                    DateTime archiveTime = m_sourceService.GetModifyTime(x);
                    return now - archiveTime >= maxAge;
                })
                .ToArray();
            
            foreach (string archivePath in archivePaths)
            {
                ILogger logger = m_logger
                    .ForContext("camera", camera)
                    .ForContext("archiveFile", archivePath);
                logger.Information("Deleting old camera archives");
                string[] chunks = await ListChunksAsync(archivePath);

                foreach (string chunk in chunks)
                {
                    string chunkPath = Path
                        .Combine(recordingDir, chunk)
                        .Replace("\\", "/");
                    m_sourceService.Delete(chunkPath);
                }

                m_sourceService.Delete(archivePath);
                logger
                    .ForContext("count", chunks.Length)
                    .Information("Done deleting old camera archives");
            }
        }
    }

    public Task<string[]> ListArchivesAsync(string cameraId)
    {
        DateTime now = m_timeService.GetUtcNow();
        string archiveDir =
            Path.Combine(
                m_configurationService.CameraArchivalDir,
                cameraId,
                "lists")
            .Replace("\\", "/");
        string[] archivePaths = m_sourceService.List(archiveDir);
        archivePaths = archivePaths
            .Where(x =>
            {
                DateTime archiveTime = m_sourceService.GetModifyTime(x);
                return now - archiveTime <= TimeSpan.FromDays(m_configurationService.CameraArchivalDays);
            })
            .OrderByDescending(m_sourceService.GetModifyTime)
            .Select(x => Path.GetFileNameWithoutExtension(x))
            .ToArray();
        return Task.FromResult(archivePaths);
    }

    public Task RemoveArchivesAsync(string cameraId)
    {
        string archiveDir =
            Path.Combine(
                m_configurationService.CameraArchivalDir,
                cameraId)
            .Replace("\\", "/");
        m_sourceService.Delete(archiveDir);
        return Task.CompletedTask;
    }

    public Task<string?> GetArchiveIndexAsync(string cameraId, string indexName)
    {
        string indexPath =
            Path.Combine(
                m_configurationService.CameraArchivalDir,
                cameraId,
                "lists",
                indexName + ".m3u8")
            .Replace("\\", "/");
        
        if (!m_sourceService.Exists(indexPath))
        {
            return Task.FromResult<string?>(null);
        }

        return m_sourceService.ReadAsync(indexPath)!;
    }

    public string? GetArchiveChunkPath(string cameraId, string chunkName)
    {
        string path =
            Path.Combine(
                m_configurationService.CameraArchivalDir,
                cameraId,
                chunkName)
            .Replace("\\", "/");

        if (!m_sourceService.Exists(path))
        {
            return null;
        }

        return path;
    }
}
