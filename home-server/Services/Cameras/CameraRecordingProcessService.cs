using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace home_server.Services.Cameras;

public interface ICameraRecordingProcessService
{
    Task ProcessAsync(
        Camera camera,
        Action<CameraRecordingSegment> newSegmentAction,
        CancellationToken cancellationToken);
}

public class CameraRecordingProcessService : ICameraRecordingProcessService
{
    const int CAMERA_RESTART_DELAY_SECS = 5;

    IConfigurationService m_configurationService;
    ICameraRecordingProcessSourceService m_sourceService;
    IFfmpegProcessService m_ffmpegProcessService;
    IMp4InfoService m_mp4InfoService;
    ICameraMp4InfoParserService m_mp4InfoParserService;
    ITimeService m_timeService;
    ILogger m_logger;

    public CameraRecordingProcessService(
        IConfigurationService configurationService,
        ICameraRecordingProcessSourceService sourceService,
        IFfmpegProcessService ffmpegProcessService,
        IMp4InfoService mp4InfoService,
        ICameraMp4InfoParserService mp4InfoParserService,
        ITimeService timeService,
        ILogger logger)
    {
        m_configurationService = configurationService;
        m_sourceService = sourceService;
        m_ffmpegProcessService = ffmpegProcessService;
        m_mp4InfoService = mp4InfoService;
        m_mp4InfoParserService = mp4InfoParserService;
        m_timeService = timeService;
        m_logger = logger.ForContext<CameraRecordingProcessService>();
    }

    string GenerateParameter(Camera camera)
    {
        string segmentPath =
            Path.Combine(
                m_configurationService.CameraArchivalDir,
                camera.Id,
                "%d.ts")
            .Replace("\\", "/");
        string url = string.Format(
            "{0}/camera/archive/{1}/chunk/",
            m_configurationService.BasePath,
            camera.Id);
        string indexPath =
            Path.Combine(
                m_configurationService.CameraArchivalDir,
                camera.Id,
                "index.m3u8")
            .Replace("\\", "/");
        
         return
            // Source.
            "-hide_banner " +
            "-loglevel warning " +
            "-fflags +genpts " +
            $"-i {camera.RtspUrl} " +
            $"-min_port {m_configurationService.RtspListenMinPort} " +
            $"-max_port {m_configurationService.RtspListenMaxPort} " +

            // Archive.
            "-c:v copy " +
            "-c:a aac " +
            "-f hls " +
            "-hls_time 10 " +
            $"-hls_segment_filename {segmentPath} " +
            "-hls_segment_type mpegts " +
            "-hls_list_size 361 " + // 1 hour (plus 1 segment).
            "-hls_flags append_list " +
            $"-hls_base_url {url} " +
            $"{indexPath} " +

            // Live.
            "-c:v copy " +
            "-c:a aac " +
            "-f mp4 " +
            "-movflags +frag_keyframe+empty_moov+default_base_moof " +
            "-frag_duration 100000 " +
            "pipe:1";
    }

    async Task<byte[]?> ReadHeaderAsync(
        IFfmpegProcess ffmpeg,
        ILogger logger)
    {
        logger.Information("Reading header");
        Memory<byte> readBuff = new byte[3000];
        int headerRead = await ffmpeg.ReadAsync(readBuff);

        if (headerRead < 100)
        {
            logger
                .ForContext("headerSize", headerRead)
                .Information("Got smaller header than expected. Reading more header");
            int moreHeaderRead = await ffmpeg.ReadAsync(readBuff.Slice(headerRead));
            headerRead += moreHeaderRead;
        }
        
        if (headerRead < 100)
        {
            logger
                .ForContext("headerSize", headerRead)
                .Error("Header too small. Stopping recording process");
            return null;
        }
        
        if (headerRead > 2000)
        {
            logger
                .ForContext("headerSize", headerRead)
                .Error("Header too big. Stopping recording process");
            return null;
        }

        logger.Information("Header size: {0} bytes", headerRead);
        return readBuff.Slice(0, headerRead).ToArray();
    }

    public async Task ProcessAsync(
        Camera camera,
        Action<CameraRecordingSegment> newSegmentAction,
        CancellationToken cancellationToken)
    {
        ILogger logger = m_logger.ForContext("camera", camera);
        byte[]? header = null;
        string? codec = null;
        byte[] readBuff = new byte[1024 * 200];
        IFfmpegProcess? ffmpeg = null;
        
        m_sourceService.CreateDirectory(
            Path.Combine(
                m_configurationService.CameraArchivalDir,
                camera.Id)
            .Replace("\\", "/"));
        
        while (true)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                if (ffmpeg != null &&
                    ffmpeg.IsRunning)
                {
                    logger.Information("Cancellation requested. Stopping recording process");
                    await ffmpeg.StopAsync();
                }

                if (ffmpeg != null)
                {
                    ffmpeg.Dispose();
                }
                    
                break;
            }
            
            if (ffmpeg == null ||
                !ffmpeg.IsRunning)
            {
                if (ffmpeg != null)
                {
                    ffmpeg.Dispose();
                }
                    
                string ffmpegParam = GenerateParameter(camera);
                logger
                    .ForContext("ffmpegParam", ffmpegParam)
                    .Information("Starting recording process");
                ffmpeg = m_ffmpegProcessService.Start(ffmpegParam);
                header = null;
                codec = null;
            }
            
            try
            {
                if (header == null ||
                    codec == null)
                {
                    header = await ReadHeaderAsync(ffmpeg, logger);

                    if (header == null)
                    {
                        // Invalid header.
                        await ffmpeg.StopAsync();
                        await m_timeService.Delay(
                            TimeSpan.FromSeconds(CAMERA_RESTART_DELAY_SECS));
                        continue;
                    }

                    logger.Information("Reading codec");
                    string mp4info = await m_mp4InfoService.GetInfoAsync(header);
                    string[] codecs = m_mp4InfoParserService.Parse(mp4info);

                    if (codecs.Length == 0)
                    {
                        logger.Information("Invalid mp4info codecs result");
                        await ffmpeg.StopAsync();
                        await m_timeService.Delay(
                            TimeSpan.FromSeconds(CAMERA_RESTART_DELAY_SECS));
                        continue;
                    }

                    codec = string.Join(",", codecs);
                    codec = $"video/mp4; codecs=\"{codec}\"";
                    logger.Information("Codec: {0}", codec);
                }

                int read = await ffmpeg.ReadAsync(readBuff);
                byte[] segment = readBuff.AsMemory()
                    .Slice(0, read)
                    .ToArray();
                newSegmentAction(
                    new CameraRecordingSegment(
                        codec,
                        header,
                        segment));
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Recording process error. Stopping");
                await ffmpeg.StopAsync();
                await m_timeService.Delay(
                    TimeSpan.FromSeconds(CAMERA_RESTART_DELAY_SECS));
            }
        }
        
        logger.Information("Recording process has stopped");
    }
}
