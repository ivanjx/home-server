using System;
using System.IO;
using System.Threading.Tasks;

namespace home_server.Services.Cameras;

public interface ICameraRecordingArchivalSourceService
{
    bool Exists(string path);
    DateTime GetModifyTime(string path);
    string[] List(string dirPath);
    void Delete(string path);
    Task<string> ReadAsync(string path);
    Task WriteAsync(string path, string content);
}

public class CameraRecordingArchivalSourceService : ICameraRecordingArchivalSourceService
{
    public void Delete(string path)
    {
        File.Delete(path);
    }

    public bool Exists(string path)
    {
        return File.Exists(path);
    }

    public DateTime GetModifyTime(string path)
    {
        return new FileInfo(path).LastWriteTimeUtc;
    }

    public string[] List(string dirPath)
    {
        return Directory.GetFiles(dirPath);
    }

    public Task<string> ReadAsync(string path)
    {
        return File.ReadAllTextAsync(path);
    }

    public Task WriteAsync(string path, string content)
    {
        string? dir = Path.GetDirectoryName(path);

        if (dir != null)
        {
            Directory.CreateDirectory(dir);
        }

        return File.WriteAllTextAsync(path, content);
    }
}
