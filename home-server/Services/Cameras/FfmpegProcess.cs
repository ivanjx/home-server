using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Serilog;

namespace home_server.Services.Cameras;

public interface IFfmpegProcess : IDisposable
{
    bool IsRunning
    {
        get;
    }

    Task StopAsync();
    Task<int> ReadAsync(Memory<byte> buffer);
}

public class FfmpegProcess : IFfmpegProcess
{
    const int FFMPEG_SIGINT_TIMEOUT_SECS = 5;
    const int FFMPEG_READ_TIMEOUT_SECS = 5;
    const int SIGINT = 2;

    ILogger m_logger;
    Process? m_process;

    public bool IsRunning
    {
        get
        {
            if (m_process == null)
            {
                return false;
            }

            try
            {
                return !m_process.HasExited; // May throw error.
            }
            catch
            {
                return false;
            }
        }
    }

    public FfmpegProcess(ILogger logger)
    {
        m_logger = logger.ForContext<FfmpegProcess>();
    }

    public void Start(string parameter)
    {
        if (IsRunning)
        {
            throw new InvalidOperationException("FFMPEG process already started");
        }

        m_process = new Process();
        m_process.StartInfo.FileName = "ffmpeg";
        m_process.StartInfo.Arguments = parameter;
        m_process.StartInfo.UseShellExecute = false;
        m_process.StartInfo.CreateNoWindow = true;
        m_process.StartInfo.RedirectStandardOutput = true;
        m_process.StartInfo.RedirectStandardError = true;
        m_process.Start();
        m_logger = m_logger
            .ForContext("parameter", parameter)
            .ForContext("pid", m_process.Id);
        _ = ConsumeFfmpegLogs();
    }

    async Task ConsumeFfmpegLogs()
    {
        while (m_process != null && IsRunning)
        {
            string? line = await m_process.StandardError.ReadLineAsync();

            if (line == null)
            {
                break;
            }

            m_logger.Information("FFMPEG: {0}", line);
        }
    }

    public async Task StopAsync()
    {
        if (m_process == null || !IsRunning)
        {
            m_logger.Information("FFMPEG process already exited");
            return;
        }

        try
        {
            m_logger.Information("Sending SIGINT to FFMPEG process");
            Native.kill(
                m_process.Id,
                SIGINT);

            m_logger.Information("Waiting for FFMPEG to exit");
            int timeout = (int)TimeSpan.FromSeconds(FFMPEG_SIGINT_TIMEOUT_SECS).TotalMilliseconds;
            await Task.Run(() => m_process.WaitForExit(timeout));

            if (!m_process.HasExited)
            {
                throw new Exception("SIGINT timeout");
            }

            m_logger.Information("FFMPEG process exited");
        }
        catch (Exception ex)
        {
            m_logger.Error(ex, "Error sending SIGINT. Sending SIGKILL instead");
            m_process.Kill();
        }
        finally
        {
            Dispose();
        }
    }

    public async Task<int> ReadAsync(Memory<byte> buffer)
    {
        if (m_process == null || !IsRunning)
        {
            throw new InvalidOperationException("FFMPEG process not started");
        }

        ValueTask<int> readTask =
            m_process.StandardOutput.BaseStream.ReadAsync(buffer);
        await Task.WhenAny(
            readTask.AsTask(),
            Task.Delay(TimeSpan.FromSeconds(FFMPEG_READ_TIMEOUT_SECS)));
        
        if (!readTask.IsCompleted)
        {
            throw new TimeoutException("FFMPEG process read timed out");
        }

        return await readTask;
    }

    public void Dispose()
    {
        if (m_process == null)
        {
            return;
        }

        try
        {
            m_process.StandardOutput.Close();
            m_process.StandardError.Close();
        }
        catch { }

        m_process.Dispose();
        m_process = null;
    }

    static class Native
    {
        [DllImport("libc")]
        public static extern int kill(int pid, int sig);
    }
}
