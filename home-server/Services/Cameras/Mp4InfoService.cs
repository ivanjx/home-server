using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace home_server.Services.Cameras;

public interface IMp4InfoService
{
    Task<string> GetInfoAsync(byte[] moovData);
}

public class Mp4InfoService : IMp4InfoService
{
    IConfigurationService m_configurationService;

    public Mp4InfoService(
        IConfigurationService configurationService)
    {
        m_configurationService = configurationService;
    }

    public async Task<string> GetInfoAsync(byte[] moovData)
    {
        Directory.CreateDirectory(
            m_configurationService.CacheDir);
        string tempPath = Path.Combine(
            m_configurationService.CacheDir,
            Guid.NewGuid().ToString());
        await File.WriteAllBytesAsync(tempPath, moovData);

        try
        {
            using Process process = new Process();
            process.StartInfo.FileName = "mp4info";
            process.StartInfo.Arguments = tempPath;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            StringBuilder resultBuilder = new StringBuilder();

            while (true)
            {
                string? line = await process.StandardOutput.ReadLineAsync();

                if (line == null)
                {
                    break;
                }

                resultBuilder.AppendLine(line);
            }

            await process.WaitForExitAsync();
            return resultBuilder.ToString();
        }
        finally
        {
            File.Delete(tempPath);
        }
    }
}
