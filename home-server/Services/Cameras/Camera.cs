using System;

namespace home_server.Services.Cameras;

public record Camera(
    string Id,
    string Name,
    string RtspUrl);
