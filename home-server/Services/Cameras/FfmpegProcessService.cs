using System;
using Serilog;

namespace home_server.Services.Cameras;

public interface IFfmpegProcessService
{
    IFfmpegProcess Start(string parameter);
}

public class FfmpegProcessService : IFfmpegProcessService
{
    ILogger m_logger;

    public FfmpegProcessService(ILogger logger)
    {
        m_logger = logger;
    }
    
    public IFfmpegProcess Start(string parameter)
    {
        FfmpegProcess process = new FfmpegProcess(m_logger);
        process.Start(parameter);
        return process;
    }
}
