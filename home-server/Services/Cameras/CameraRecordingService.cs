using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace home_server.Services.Cameras;

public interface ICameraRecordingService
{
    void Start(Camera camera);
    Task StopAsync(string cameraId);
    Task StopAllAsync();
    Task ServeAsync(
        string cameraId,
        IWebSocket socket,
        CancellationToken cancellationToken);
}

public class CameraRecordingService : ICameraRecordingService
{
    CameraRecordingState m_state;
    ICameraRecordingProcessService m_processService;
    ILogger m_logger;

    public CameraRecordingService(
        CameraRecordingState state,
        ICameraRecordingProcessService processService,
        ILogger logger)
    {
        m_state = state;
        m_processService = processService;
        m_logger = logger.ForContext<CameraRecordingService>();
    }

    public void Start(Camera camera)
    {
        if (m_state.Recordings.ContainsKey(camera.Id))
        {
            throw new Exception("Camera recording already started");
        }
        
        CameraRecording recording = new CameraRecording();
        recording.Task = m_processService.ProcessAsync(
            camera,
            recording.HandleNewSegment,
            recording.CancellationToken);
        m_state.Recordings.Add(camera.Id, recording);
    }

    public async Task StopAsync(string cameraId)
    {
        CameraRecording? recording;

        if (!m_state.Recordings.TryGetValue(cameraId, out recording))
        {
            throw new Exception("Camera recording not started");
        }

        recording.Cancel();
        await recording.Task; // m_processService.ProcessAsync().
        recording.Dispose();
        m_state.Recordings.Remove(cameraId);
    }

    public async Task StopAllAsync()
    {
        foreach (string cameraId in m_state.Recordings.Keys)
        {
            await StopAsync(cameraId);
        }
    }

    public async Task ServeAsync(string cameraId, IWebSocket socket, CancellationToken cancellationToken)
    {
        CameraRecording? recording;
        
        if (!m_state.Recordings.TryGetValue(cameraId, out recording))
        {
            throw new Exception("Camera recording not started");
        }
        
        ILogger logger = m_logger
            .ForContext("cameraId", cameraId)
            .ForContext("ConnectionId", socket.ConnectionId);
        logger.Information("Begin serving recording segments");
        bool isCodecSent = false;
        bool isHeaderSent = false;
        TaskCompletionSource awaiter = new TaskCompletionSource();

        Action<CameraRecordingSegment> handler = async segment =>
        {
            if (awaiter.Task.IsCompleted)
            {
                return;
            }

            if (recording.Task.IsCompleted)
            {
                logger.Information("Recording stopped");
                awaiter.TrySetResult();
                return;
            }
            
            if (cancellationToken.IsCancellationRequested ||
                !socket.IsOpen)
            {
                logger.Information("Socket cancelled/closed");
                awaiter.TrySetResult();
                return;
            }

            try
            {
                if (!isCodecSent)
                {
                    byte[] codecBuff = new byte[128];
                    Array.Copy(
                        Encoding.ASCII.GetBytes(segment.Codec),
                        codecBuff,
                        segment.Codec.Length);
                    await socket.SendAsync(
                        codecBuff,
                        cancellationToken);
                    isCodecSent = true;
                    logger.Information("Codec sent to client");
                }

                if (!isHeaderSent &&
                    segment.Data.Length >= 8 &&
                    segment.Data[4] == 'm' &&
                    segment.Data[5] == 'o' &&
                    segment.Data[6] == 'o' &&
                    segment.Data[7] == 'f')
                {
                    // After sending header must be followed by 'moof'.
                    await socket.SendAsync(
                        segment.Header,
                        cancellationToken);
                    isHeaderSent = true;
                    logger.Information("Header sent to client");
                }

                if (isHeaderSent)
                {
                    await socket.SendAsync(
                        segment.Data,
                        cancellationToken);
                }
            }
            catch (Exception ex)
            {
                awaiter.TrySetException(ex);
            }
        };
        recording.NewSegment += handler;

        try
        {
            await awaiter.Task;
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Error serving recording segments");
        }
        finally
        {
            recording.NewSegment -= handler;
            logger.Information("End serving recording segments");
        }
    }
}
