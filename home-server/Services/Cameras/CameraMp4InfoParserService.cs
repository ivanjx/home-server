using System;
using System.Collections.Generic;
using System.IO;

namespace home_server.Services.Cameras;

public interface ICameraMp4InfoParserService
{
    string[] Parse(string info);
}

public class CameraMp4InfoParserService : ICameraMp4InfoParserService
{
    public string[] Parse(string info)
    {
        using StringReader reader = new StringReader(info);
        List<string> result = new List<string>();

        while (true)
        {
            string? line = reader.ReadLine();

            if (line == null)
            {
                break;
            }

            if (line == "")
            {
                continue;
            }

            line = line.Trim();

            if (!line.StartsWith("Codec String:"))
            {
                continue;
            }

            string[] lineSplit = line.Split(':');

            if (lineSplit.Length < 2)
            {
                continue;
            }

            result.Add(lineSplit[1].Trim());
        }

        return result.ToArray();
    }
}
