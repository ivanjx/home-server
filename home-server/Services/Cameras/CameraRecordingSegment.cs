using System;

namespace home_server.Services.Cameras;

public record CameraRecordingSegment(
    string Codec,
    byte[] Header,
    byte[] Data);
