using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace home_server.Services.Cameras;

public interface ICameraRepository
{
    Task<Camera?> GetByIdAsync(string id);
    Task<bool> IsRtspUrlExistsAsync(string url);
    Task<IEnumerable<Camera>> ListAllAsync();
    Task<Camera> AddAsync(
        string name,
        string url);
    Task UpdateAsync(Camera camera);
    Task DeleteAsync(string id);
}

public class CameraRepository : ICameraRepository
{
    DbConn m_conn;
    ITimeService m_timeService;

    public CameraRepository(
        DbConn conn,
        ITimeService timeService)
    {
        m_conn = conn;
        m_timeService = timeService;
    }

    public async Task<Camera?> GetByIdAsync(string id)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT id, name, rtsp_url
            FROM cameras
            WHERE id=$id";
        command.Parameters.AddWithValue("$id", id);
        await command.PrepareAsync();
        using SqliteDataReader reader = await command.ExecuteReaderAsync();

        if (reader.Read())
        {
            return new Camera(
                reader.GetString(0),
                reader.GetString(1),
                reader.GetString(2));
        }

        return null;
    }

    public async Task<IEnumerable<Camera>> ListAllAsync()
    {   
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT
                id,
                name,
                rtsp_url
            FROM cameras";
        using SqliteDataReader reader = await command.ExecuteReaderAsync();
        List<Camera> result = new List<Camera>();

        while (await reader.ReadAsync())
        {
            Camera camera = new Camera(
                reader.GetString(0),
                reader.GetString(1),
                reader.GetString(2));
            result.Add(camera);
        }

        return result;
    }

    public async Task<bool> IsRtspUrlExistsAsync(string url)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT COUNT(*)
            FROM cameras
            WHERE rtsp_url=$url";
        command.Parameters.AddWithValue("$url", url);
        await command.PrepareAsync();
        object? result = await command.ExecuteScalarAsync();

        if (result == null)
        {
            throw new Exception("Unable to count");
        }

        return (long)result != 0;
    }

    public async Task<Camera> AddAsync(string name, string url)
    {
        Guid id = Guid.NewGuid();

        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            INSERT INTO cameras (
                id,
                name,
                rtsp_url,
                created_at
            ) VALUES (
                $id,
                $name,
                $url,
                $createdAt
            )";
        command.Parameters.AddWithValue("$id", id.ToString());
        command.Parameters.AddWithValue("$name", name);
        command.Parameters.AddWithValue("$url", url);
        command.Parameters.AddWithValue(
            "$createdAt",
            new DateTimeOffset(m_timeService.GetUtcNow()).ToUnixTimeSeconds());
        command.Prepare();
        int count = await command.ExecuteNonQueryAsync();

        if (count != 1)
        {
            throw new Exception("Unable to add camera");
        }

        return new Camera(
            id.ToString(),
            name,
            url);
    }

    public async Task DeleteAsync(string id)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            DELETE FROM cameras
            WHERE id=$id;";
        command.Parameters.AddWithValue("$id", id);
        await command.PrepareAsync();
        int count = await command.ExecuteNonQueryAsync();

        if (count != 1)
        {
            throw new Exception("Unable to delete camera");
        }
    }

    public async Task UpdateAsync(Camera camera)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            UPDATE cameras
            SET
                name=$name,
                rtsp_url=$url
            WHERE id=$id;";
        command.Parameters.AddWithValue("$id", camera.Id);
        command.Parameters.AddWithValue("$name", camera.Name);
        command.Parameters.AddWithValue("$url", camera.RtspUrl);
        await command.PrepareAsync();
        int count = await command.ExecuteNonQueryAsync();

        if (count != 1)
        {
            throw new Exception("Unable to update camera");
        }
    }
}
