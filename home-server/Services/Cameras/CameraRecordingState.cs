using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace home_server.Services.Cameras;

public class CameraRecording : IDisposable
{
    CancellationTokenSource m_cts;

    public event Action<CameraRecordingSegment>? NewSegment;

    public Task Task
    {
        get;
        set;
    }

    public CancellationToken CancellationToken
    {
        get => m_cts.Token;
    }

    public CameraRecording()
    {
        m_cts = new CancellationTokenSource();
        Task = Task.CompletedTask;
    }

    public void HandleNewSegment(CameraRecordingSegment segment)
    {
        NewSegment?.Invoke(segment);
    }

    public void Cancel()
    {
        m_cts.Cancel();
    }

    public void Dispose()
    {
        m_cts.Dispose();
    }
}

public class CameraRecordingState
{
    public Dictionary<string, CameraRecording> Recordings
    {
        get;
    }

    public CameraRecordingState()
    {
        Recordings = new Dictionary<string, CameraRecording>();
    }
}
