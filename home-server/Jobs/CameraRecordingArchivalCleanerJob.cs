using System;
using System.Threading;
using System.Threading.Tasks;
using home_server.Services.Cameras;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace home_server.Jobs;

public class CameraRecordingArchivalCleanerJob : BackgroundService
{
    const int INTERVAL_MINS = 15;

    ICameraRecordingArchivalService m_archivalService;

    ILogger Logger
    {
        get => Log.Logger.ForContext<CameraRecordingArchivalCleanerJob>();
    }

    public CameraRecordingArchivalCleanerJob(
        ICameraRecordingArchivalService archivalService)
    {
        m_archivalService = archivalService;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        Log.Information("Starting camera recording archival cleaner job");

        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                await m_archivalService.CleanArchivesAsync();
                await Task.Delay(
                    TimeSpan.FromMinutes(INTERVAL_MINS),
                    stoppingToken);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Camera recording archival cleaner job error");
            }
        }
    }
}
