using System;
using System.Threading;
using System.Threading.Tasks;
using home_server.Services.Cameras;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace home_server.Jobs;

public class CameraRecordingJob : IHostedService
{
    ICameraService m_cameraService;
    ICameraRecordingService m_cameraRecordingService;
    
    ILogger Log
    {
        get => Serilog.Log.Logger.ForContext<CameraRecordingJob>();
    }

    public CameraRecordingJob(
        ICameraService cameraService,
        ICameraRecordingService cameraRecordingService)
    {
        m_cameraService = cameraService;
        m_cameraRecordingService = cameraRecordingService;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        Log.Information("Starting camera recording job");
        
        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                await m_cameraService.StartAllRecordingAsync();
                break;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error starting camera recording. Retrying in 10 secs");
                await Task.Delay(
                    TimeSpan.FromSeconds(10),
                    cancellationToken);
            }
        }
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        Log.Information("Stopping camera recording job");

        try
        {
            await m_cameraRecordingService.StopAllAsync();
        }
        catch (Exception ex)
        {
            Log.Error(ex, "Error stopping camera recording job");
        }
    }
}
