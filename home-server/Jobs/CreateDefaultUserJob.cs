using System;
using System.Threading;
using System.Threading.Tasks;
using home_server.Services.Users;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace home_server.Jobs;

public class CreateDefaultUserJob : IHostedService
{
    IDefaultUserRegistrationService m_defaultUserRegistrationService;

    ILogger Log
    {
        get => Serilog.Log.Logger.ForContext<CreateDefaultUserJob>();
    }

    public CreateDefaultUserJob(
        IDefaultUserRegistrationService defaultUserRegistrationService)
    {
        m_defaultUserRegistrationService = defaultUserRegistrationService;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        Log.Information("Starting create default user job");
        
        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                await m_defaultUserRegistrationService.CreateDefaultUserAsync();
                break;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error creating default user. Retrying in 10 secs");
                await Task.Delay(
                    TimeSpan.FromSeconds(10),
                    cancellationToken);
            }
        }
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}
