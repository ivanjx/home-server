using System;
using System.Threading;
using System.Threading.Tasks;
using home_server.Services.Cameras;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace home_server.Jobs;

public class CameraRecordingArchivalJob : BackgroundService
{
    const int INTERVAL_SECS = 3;

    ICameraRecordingArchivalService m_archivalService;
    
    ILogger Log
    {
        get => Serilog.Log.Logger.ForContext<CameraRecordingArchivalJob>();
    }

    public CameraRecordingArchivalJob(
        ICameraRecordingArchivalService archivalService)
    {
        m_archivalService = archivalService;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        Log.Information("Starting camera recording archival job");
        
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                await Task.Delay(
                    TimeSpan.FromSeconds(INTERVAL_SECS),
                    stoppingToken);
                await m_archivalService.CreateArchivesAsync();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Camera recording archival job error");
            }
        }
    }
}
