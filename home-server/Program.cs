﻿using System;
using home_server.Controllers;
using home_server.Jobs;
using home_server.Services;
using home_server.Services.Authentication;
using home_server.Services.Cameras;
using home_server.Services.Users;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;

ConfigurationService configurationService = new ConfigurationService();
DbConn dbConn = new DbConn(configurationService);

// Serilog.
var loggerConfiguration = new LoggerConfiguration()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
    .MinimumLevel.Override("System", LogEventLevel.Error)
    .Enrich.FromLogContext();

if (configurationService.SeqEndpoint != null &&
    configurationService.SeqApiKey != null)
{
    // Use seq.
    loggerConfiguration = loggerConfiguration
        .WriteTo.Seq(
            configurationService.SeqEndpoint,
            apiKey: configurationService.SeqApiKey);
}
else
{
    // Use console.
    loggerConfiguration = loggerConfiguration
        .WriteTo.Console(outputTemplate: "[{Timestamp:u} {Level:u3}] [{ConnectionId}] [{SourceContext}] {Message:lj}{NewLine}{Exception}");
}

Log.Logger = loggerConfiguration.CreateLogger();

try
{
    WebApplicationBuilder builder = WebApplication.CreateSlimBuilder(args);
    builder.WebHost.UseUrls(configurationService.ListenEndpoint);
    builder.Host.UseSerilog();
    
    builder.Services
        // Controllers json serializer context.
        .ConfigureHttpJsonOptions(options =>
        {
            options.SerializerOptions.TypeInfoResolverChain.Insert(
                0,
                ControllerJsonContext.Default);
        })

        // Register built-in/libraries services.
        .AddCors()
        .AddHttpClient()
        .AddHttpContextAccessor()
        .AddSerilog(Log.Logger)
        
        // Register custom services.
        .AddSingleton<IConfigurationService>(configurationService)
        .AddSingleton(dbConn)
        .AddSingleton<ITimeService, TimeService>()
        .AddSingleton<IAuthenticationStringGenerator, AuthenticationStringGenerator>()
        .AddSingleton<IUserAuthenticationRepository, UserAuthenticationRepository>()
        .AddSingleton<IUserAuthenticationTokenRepository, UserAuthenticationTokenRepository>()
        .AddSingleton<IAuthenticationState, AuthenticationState>()
        .AddSingleton<IAuthenticationService, AuthenticationService>()
        .AddSingleton<IUserRepository, UserRepository>()
        .AddSingleton<IUserRegistrationService, UserRegistrationService>()
        .AddSingleton<IDefaultUserRegistrationService, DefaultUserRegistrationService>()
        .AddSingleton<IFfmpegProcessService, FfmpegProcessService>()
        .AddSingleton<IMp4InfoService, Mp4InfoService>()
        .AddSingleton<ICameraMp4InfoParserService, CameraMp4InfoParserService>()
        .AddSingleton<ICameraRecordingProcessService, CameraRecordingProcessService>()
        .AddSingleton<CameraRecordingState>()
        .AddSingleton<ICameraRecordingService, CameraRecordingService>()
        .AddSingleton<ICameraRepository, CameraRepository>()
        .AddSingleton<ICameraService, CameraService>()
        .AddSingleton<ICameraRecordingArchivalSourceService, CameraRecordingArchivalSourceService>()
        .AddSingleton<ICameraRecordingProcessSourceService, CameraRecordingProcessSourceService>()
        .AddSingleton<ICameraRecordingArchivalService, CameraRecordingArchivalService>()

        // Register jobs.
        .AddHostedService<CreateDefaultUserJob>()
        .AddHostedService<CameraRecordingJob>()
        .AddHostedService<CameraRecordingArchivalJob>()
        .AddHostedService<CameraRecordingArchivalCleanerJob>();
    
    WebApplication application = builder.Build();
    application
        // All starts with /api.
        .UsePathBase(configurationService.BasePath)

        // Request logging.
        .UseSerilogRequestLogging()

        // CORS.
        .UseCors(builder => builder
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
            .SetIsOriginAllowed(origin =>
                origin.Contains("localhost") || // Allow from localhost.
                origin.Contains(".trycloudflare.com"))) // Allow from cloudflared tunnel.

        // Exception handlers (from bottom -> top).
        .UseMiddleware<ExceptionHandlerMiddleware>()
        .UseMiddleware<AuthenticationMiddleware>()

        // Routing.
        .UseRouting()
        
        // Web socket.
        .UseWebSockets(new WebSocketOptions()
        {
            KeepAliveInterval = TimeSpan.FromMinutes(1)
        });

    // Controllers.
    IndexController.Map(application);
    AuthenticationController.Map(application);
    CameraController.Map(application);
    CameraStreamController.Map(application);
    CameraArchiveController.Map(application);

    // Initializing database.
    Log.Information("Initiailzing database");
    await dbConn.InitAsync();

    // Done.
    Log.Information(
        "Listening on: {0}",
        configurationService.ListenEndpoint);
    await application.RunAsync();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Web server error");
}
finally
{
    Log.CloseAndFlush();
}
