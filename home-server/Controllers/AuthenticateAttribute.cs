using System;

namespace home_server.Controllers;

[AttributeUsage(AttributeTargets.Method)]
public class AuthenticateAttribute : Attribute
{
}
