using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace home_server.Controllers;

public class IndexController
{
    public static void Map(WebApplication app)
    {
        IndexController controller = new IndexController();
        app.MapGet("/", controller.GetAsync);
    }

    public Task<string> GetAsync()
    {
        return Task.FromResult("hello world");
    }
}
