using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;
using home_server.Services;
using System.Text.Json.Serialization;

namespace home_server.Controllers;

public class ExceptionHandlerMiddleware
{
    RequestDelegate m_next;

    public ExceptionHandlerMiddleware(RequestDelegate next)
    {
        m_next = next;
    }

    public async Task Invoke(HttpContext context, ILogger logger)
    {
        try
        {
            await m_next.Invoke(context);
        }
        catch (ApiException ex)
        {
            logger.Error(ex, ex.Message);

            // Override response.
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            await context.Response.WriteAsJsonAsync(
                new ErrorResult(
                    ex.Name,
                    context.Connection.Id),
                ControllerJsonContext.Default.ErrorResult);
        }
        catch (InvalidParameterException ex)
        {
            logger.Error(ex, ex.Message);

            // Override response.
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            await context.Response.WriteAsJsonAsync(
                new ErrorResult(
                    "bad_request",
                    context.Connection.Id),
                ControllerJsonContext.Default.ErrorResult);
        }
        catch (Exception ex)
        {
            logger.Error(ex, ex.Message);

            // Override response.
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            await context.Response.WriteAsJsonAsync(
                new ErrorResult(
                    "internal_server_error",
                    context.Connection.Id),
                ControllerJsonContext.Default.ErrorResult);
        }
    }

    public record ErrorResult
    {
        [JsonPropertyName("error")]
        public string Error
        {
            get;
            init;
        }

        [JsonPropertyName("traceId")]
        public string TraceId
        {
            get;
            init;
        }

        public ErrorResult(string error, string traceId)
        {
            Error = error;
            TraceId = traceId;
        }
    }
}
