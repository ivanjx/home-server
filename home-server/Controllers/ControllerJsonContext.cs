using System;
using System.Text.Json.Serialization;

namespace home_server.Controllers;

[JsonSerializable(typeof(string[]))]
[JsonSerializable(typeof(ExceptionHandlerMiddleware.ErrorResult))]
[JsonSerializable(typeof(AuthenticationController.LoginRequest))]
[JsonSerializable(typeof(AuthenticationController.LoginResponse))]
[JsonSerializable(typeof(AuthenticationController.ChangePasswordRequest))]
[JsonSerializable(typeof(CameraController.CameraRequest))]
[JsonSerializable(typeof(CameraController.CameraResponse))]
[JsonSerializable(typeof(CameraController.CameraResponse[]))]
public partial class ControllerJsonContext : JsonSerializerContext
{
}
