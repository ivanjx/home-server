using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using home_server.Services.Cameras;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace home_server.Controllers;

public class CameraController
{
    public static void Map(WebApplication app)
    {
        CameraController controller = new CameraController(
            app.Services.GetRequiredService<ICameraRepository>(),
            app.Services.GetRequiredService<ICameraService>());
        app.MapGet("/camera/list", controller.ListAsync);
        app.MapPost("/camera/create", controller.CreateAsync);
        app.MapPost("/camera/update", controller.UpdateAsync);
        app.MapPost("/camera/delete", controller.DeleteAsync);
    }
    
    ICameraRepository m_cameraRepository;
    ICameraService m_cameraService;

    public CameraController(
        ICameraRepository cameraRepository,
        ICameraService cameraService)
    {
        m_cameraRepository = cameraRepository;
        m_cameraService = cameraService;
    }

    public record CameraResponse
    {
        [JsonPropertyName("id")]
        public string Id
        {
            get;
            set;
        }

        [JsonPropertyName("name")]
        public string Name
        {
            get;
            set;
        }

        public CameraResponse(Camera camera)
        {
            Id = camera.Id;
            Name = camera.Name;
        }
    }

    public record CameraRequest
    {
        [JsonPropertyName("id")]
        public string? Id
        {
            get;
            set;
        }

        [JsonPropertyName("name")]
        public string? Name
        {
            get;
            set;
        }

        [JsonPropertyName("rtspUrl")]
        public string? RtspUrl
        {
            get;
            set;
        }
    }

    [Authenticate]
    public async Task<CameraResponse[]> ListAsync()
    {
        IEnumerable<Camera> cameras = await m_cameraRepository.ListAllAsync();
        return cameras
            .Select(x => new CameraResponse(x))
            .ToArray();
    }

    [Authenticate]
    public async Task<CameraResponse> CreateAsync(CameraRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Name) ||
            string.IsNullOrEmpty(request.RtspUrl))
        {
            throw new ApiException("missing_parameters");
        }

        CreateCameraResult result = await m_cameraService.CreateAsync(
            request.Name,
            request.RtspUrl);
        
        if (result is DuplicateUrlCreateCameraResult)
        {
            throw new ApiException("duplicate_camera_url");
        }
        else if (result is SuccessCreateCameraResult successResult)
        {
            return new CameraResponse(successResult.Camera);
        }
        else
        {
            // Impossible.
            throw new Exception("Invalid create camera result");
        }
    }

    [Authenticate]
    public async Task UpdateAsync(CameraRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Id) ||
            string.IsNullOrEmpty(request.Name) ||
            string.IsNullOrEmpty(request.RtspUrl))
        {
            throw new ApiException("missing_parameters");
        }

        UpdateCameraResult result = await m_cameraService.UpdateAsync(
            new Camera(
                request.Id,
                request.Name,
                request.RtspUrl));
        
        if (result is DuplicateUrlUpdateCameraResult)
        {
            throw new ApiException("duplicate_camera_url");
        }
    }

    [Authenticate]
    public async Task DeleteAsync(CameraRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Id))
        {
            throw new ApiException("missing_parameters");
        }

        await m_cameraService.DeleteAsync(request.Id);
    }
}
