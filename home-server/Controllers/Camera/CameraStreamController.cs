using System;
using System.Threading;
using System.Threading.Tasks;
using home_server.Services;
using home_server.Services.Cameras;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace home_server.Controllers;

public class CameraStreamController
{
    public static void Map(WebApplication app)
    {
        CameraStreamController controller = new CameraStreamController(
            app.Services.GetRequiredService<ICameraRecordingService>());
        app.MapGet("/camera/stream/{cameraId}", controller.ConnectWebSocketAsync);
    }
    
    ICameraRecordingService m_recordingService;

    public CameraStreamController(
        ICameraRecordingService recordingService)
    {
        m_recordingService = recordingService;
    }

    [Authenticate]
    public async Task ConnectWebSocketAsync(
        string? cameraId,
        HttpRequest httpRequest,
        CancellationToken cancellationToken)
    {
        if (!httpRequest.HttpContext.WebSockets.IsWebSocketRequest)
        {
            throw new ApiException("invalid_request");
        }
        
        if (string.IsNullOrEmpty(cameraId))
        {
            throw new ApiException("missing_parameters");
        }

        // Socket is disposed inside service.
        var ws = await httpRequest.HttpContext.WebSockets.AcceptWebSocketAsync();
        using WebSocket socket = new WebSocket(
            ws,
            httpRequest.HttpContext.Connection.Id);
        await m_recordingService.ServeAsync(
            cameraId,
            socket,
            cancellationToken);
    }
}
