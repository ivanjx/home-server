using System;
using System.Threading.Tasks;
using home_server.Services.Cameras;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace home_server.Controllers;

public class CameraArchiveController
{
    public static void Map(WebApplication app)
    {
        CameraArchiveController controller = new CameraArchiveController(
            app.Services.GetRequiredService<ICameraRecordingArchivalService>());
        app.MapGet("/camera/archive/list", controller.ListAsync);
        app.MapGet("/camera/archive/{cameraId}/{indexName}", controller.GetIndexAsync);
        app.MapGet("/camera/archive/{cameraId}/chunk/{chunkName}", controller.GetChunkAsync);
    }
    
    ICameraRecordingArchivalService m_archivalService;

    public CameraArchiveController(
        ICameraRecordingArchivalService archivalService)
    {
        m_archivalService = archivalService;
    }

    [Authenticate]
    public async Task<string[]> ListAsync(string? cameraId)
    {
        if (string.IsNullOrEmpty(cameraId))
        {
            throw new ApiException("missing_parameters");
        }

        return await m_archivalService.ListArchivesAsync(cameraId);
    }

    [Authenticate]
    public async Task<IResult> GetIndexAsync(
        string? cameraId,
        string? indexName)
    {
        if (string.IsNullOrEmpty(cameraId) ||
            string.IsNullOrEmpty(indexName))
        {
            throw new ApiException("missing_parameters");
        }

        string? index = await m_archivalService.GetArchiveIndexAsync(
            cameraId,
            indexName);
        
        if (index == null)
        {
            throw new ApiException("not_found");
        }

        return Results.Text(
            index,
            "application/x-mpegURL");
    }

    [Authenticate]
    public Task<IResult> GetChunkAsync(
        string? cameraId,
        string? chunkName)
    {
        if (string.IsNullOrEmpty(cameraId) ||
            string.IsNullOrEmpty(chunkName))
        {
            throw new ApiException("missing_parameters");
        }

        string? chunkPath = m_archivalService.GetArchiveChunkPath(
            cameraId,
            chunkName);
        
        if (chunkPath == null)
        {
            throw new ApiException("not_found");
        }

        IResult result = Results.File(
            chunkPath,
            "video/mp2t"); // .ts
        return Task.FromResult(result);
    }
}
