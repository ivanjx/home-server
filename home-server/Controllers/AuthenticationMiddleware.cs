using System;
using System.Threading.Tasks;
using home_server.Services.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Serilog;

namespace home_server.Controllers;

public class AuthenticationMiddleware
{
    RequestDelegate m_next;

    public AuthenticationMiddleware(RequestDelegate next)
    {
        m_next = next;
    }

    async Task<SuccessAuthenticationResult> GetUserAsync(
        HttpContext context,
        IAuthenticationService authenticationService,
        ILogger log)
    {
        string? token = context.GetAuthCookie();

        if (token == null)
        {
            throw new ApiException("not_authenticated");
        }

        AuthenticationResult result = await authenticationService.AuthenticateAsync(
            token,
            context.GetClientIpAddress());

        if (result is not SuccessAuthenticationResult successResult)
        {
            throw new ApiException("not_authenticated");
        }
        
        if (successResult is SuccessUpdatedAuthenticationResult)
        {
            log.Information("Updating cookie");
            context.SetAuthCookie(
                successResult.RawToken,
                successResult.Token.IsLongLived ?
                    successResult.Token.ExpireAt :
                    default);
        }

        return successResult;
    }

    public async Task Invoke(
        HttpContext context,
        IAuthenticationService authenticationService,
        IAuthenticationState authenticationState,
        ILogger log)
    {
        AuthenticateAttribute? attribute = context.Features
            .Get<IEndpointFeature>()?
            .Endpoint?
            .Metadata
            .GetMetadata<AuthenticateAttribute>();

        if (attribute != null)
        {
            SuccessAuthenticationResult authenticationResult = await GetUserAsync(
                context,
                authenticationService,
                log);
            ((AuthenticationState)authenticationState).RawToken =
                authenticationResult.Token.Token;
            ((AuthenticationState)authenticationState).User = new AuthenticationUser(
                authenticationResult.Token.UserId,
                authenticationResult.Token.Username);
        }
        
        await m_next(context);
    }
}
