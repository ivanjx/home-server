using System;
using Microsoft.AspNetCore.Http;

namespace home_server.Controllers;

public static class Extensions
{
    public static string GetClientIpAddress(this HttpContext context)
    {
        string? result = context.Request.Headers["X-REAL-IP"];

        if (string.IsNullOrEmpty(result))
        {
            result = context.Request.Headers["X-FORWARDED-FOR"];
        }

        if (string.IsNullOrEmpty(result))
        {
            result = context.Connection.RemoteIpAddress?.ToString() ?? "UNKNOWN";
        }

        return result;
    }

    public static string? GetAuthCookie(this HttpContext context)
    {
        context.Request.Cookies.TryGetValue(
            "Auth-Token",
            out string? token);
        return token;
    }

    public static void SetAuthCookie(
        this HttpContext context,
        string value,
        DateTime expireAt)
    {
        CookieOptions options = new CookieOptions()
        {
            IsEssential = true,
            HttpOnly = true,
            Secure = context.Request.IsHttps
        };

        if (expireAt != default)
        {
            options.Expires = expireAt;
        }
        
        context.Response.Cookies.Append(
            "Auth-Token",
            value,
            options);
    }
}
