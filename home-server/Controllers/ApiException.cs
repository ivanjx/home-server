using System;

namespace home_server.Controllers;

public class ApiException : Exception
{
    public string Name
    {
        get;
    }

    public ApiException(string name) :
        base(name)
    {
        Name = name;
    }
}
