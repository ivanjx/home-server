using System;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using home_server.Services.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace home_server.Controllers;

public class AuthenticationController
{
    public static void Map(WebApplication app)
    {
        AuthenticationController controller = new AuthenticationController(
            app.Services.GetRequiredService<IAuthenticationState>(),
            app.Services.GetRequiredService<IAuthenticationService>(),
            app.Services.GetRequiredService<ILogger>());
        app.MapGet("/authentication/check", controller.CheckAsync);
        app.MapPost("/authentication/login", controller.LoginAsync);
        app.MapPost("/authentication/logout", controller.LogoutAsync);
        app.MapPost(
            "/authentication/change-password",
            controller.ChangePasswordAsync);
    }

    IAuthenticationState m_authenticationState;
    IAuthenticationService m_authenticationService;
    ILogger m_logger;

    public AuthenticationController(
        IAuthenticationState authenticationState,
        IAuthenticationService authenticationService,
        ILogger logger)
    {
        m_authenticationState = authenticationState;
        m_authenticationService = authenticationService;
        m_logger = logger.ForContext<AuthenticationController>();
    }

    public record LoginRequest
    {
        [JsonPropertyName("username")]
        public string? Username
        {
            get;
            set;
        }

        [JsonPropertyName("password")]
        public string? Password
        {
            get;
            set;
        }

        [JsonPropertyName("deviceName")]
        public string? DeviceName
        {
            get;
            set;
        }

        [JsonPropertyName("isLongLived")]
        public bool? IsLongLived
        {
            get;
            set;
        }
    }

    public record LoginResponse
    {
        [JsonPropertyName("userId")]
        public string UserId
        {
            get;
            set;
        }

        [JsonPropertyName("username")]
        public string Username
        {
            get;
            set;
        }

        public LoginResponse(string userId, string username)
        {
            UserId = userId;
            Username = username;
        }
    }

    public async Task<LoginResponse> LoginAsync(
        HttpRequest httpRequest,
        LoginRequest? request)
    {
        // Validating request.
        if (request == null ||
            string.IsNullOrEmpty(request.Username) ||
            string.IsNullOrEmpty(request.Password) ||
            string.IsNullOrEmpty(request.DeviceName))
        {
            throw new ApiException("missing_parameters");
        }

        // Authenticating.
        AuthenticationResult result = await m_authenticationService.AuthenticateAsync(
            request.Username,
            request.Password,
            request.DeviceName,
            httpRequest.HttpContext.GetClientIpAddress(),
            request.IsLongLived ?? false);
        
        if (result is not SuccessAuthenticationResult successResult)
        {
            throw new ApiException("invalid_authentication");
        }

        // Done.
        m_logger.Information("Creating auth cookie");
        httpRequest.HttpContext.SetAuthCookie(
            successResult.RawToken,
            successResult.Token.IsLongLived ?
                successResult.Token.ExpireAt :
                default);
        return new LoginResponse(
            successResult.Token.UserId,
            successResult.Token.Username);
    }

    [Authenticate]
    public Task<LoginResponse> CheckAsync()
    {
        LoginResponse response = new LoginResponse(
            m_authenticationState.User.Id,
            m_authenticationState.User.Username);
        return Task.FromResult(response);
    }

    [Authenticate]
    public async Task LogoutAsync(HttpRequest httpRequest)
    {
        // Deauthenticating.
        await m_authenticationService.DeauthenticateAsync(
            m_authenticationState.RawToken);

        // Invalidating cookie.
        httpRequest.HttpContext.SetAuthCookie(
            m_authenticationState.RawToken,
            DateTime.UtcNow.AddSeconds(-1));
    }

    public record ChangePasswordRequest
    {
        [JsonPropertyName("oldPassword")]
        public string? OldPassword
        {
            get;
            set;
        }

        [JsonPropertyName("newPassword")]
        public string? NewPassword
        {
            get;
            set;
        }
    }

    [Authenticate]
    public async Task ChangePasswordAsync(ChangePasswordRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.OldPassword) ||
            string.IsNullOrEmpty(request.NewPassword))
        {
            throw new ApiException("missing_parameters");
        }

        bool success = await m_authenticationService.ChangePasswordAsync(
            m_authenticationState.User.Id,
            request.OldPassword,
            request.NewPassword);
        
        if (!success)
        {
            throw new ApiException("invalid_authentication");
        }
    }
}
