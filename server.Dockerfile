# FFMPEG & mp4info download.
FROM debian:stable-slim as deps-download
RUN apt-get update && \
    apt-get install -y wget xz-utils
ARG TARGETPLATFORM
ARG FFMPEG=5.1.1
RUN ARCH=$(echo $TARGETPLATFORM | cut -d '/' -f 2) && \
    wget --tries=10 -O ffmpeg.tar.xz https://www.johnvansickle.com/ffmpeg/old-releases/ffmpeg-$FFMPEG-$ARCH-static.tar.xz && \
    tar -xf ffmpeg.tar.xz && \
    cp ffmpeg-$FFMPEG-$ARCH-static/ffmpeg /bin/ffmpeg
RUN ARCH=$(echo $TARGETPLATFORM | cut -d '/' -f 2) && \
    wget --tries=10 -O bento4.tar.gz https://github.com/ivanjx/Bento4/releases/download/latest/bento4-$ARCH.tar.gz && \
    mkdir bento4 && \
    tar -xvf bento4.tar.gz

# Build.
FROM mcr.microsoft.com/dotnet/sdk:8.0.100-bookworm-slim-amd64 as build
RUN dpkg --add-architecture arm64 && \
    apt-get update && \
    apt-get install -y \
    clang zlib1g-dev zlib1g-dev:arm64 \
    binutils-aarch64-linux-gnu gcc-aarch64-linux-gnu
COPY ./home-server /home-server
WORKDIR /home-server
ARG TARGETPLATFORM
RUN ARCH=$(echo $TARGETPLATFORM | cut -d '/' -f 2) && \
    PREFIX=$(echo $ARCH | awk '{if ($0 == "arm64") print "aarch64-linux-gnu-";}') && \
    dotnet publish \
    -c Release \
    -r linux-$ARCH \
    -p:ObjCopyName=${PREFIX}objcopy \
    -o /output && \
    rm /output/*.dbg

# Runtime.
FROM mcr.microsoft.com/dotnet/runtime-deps:8.0.0-bookworm-slim as runtime
COPY --from=build /output /app
COPY --from=deps-download /bin/ffmpeg /usr/local/bin/ffmpeg
COPY --from=deps-download /bento4/mp4info /usr/local/bin/mp4info
ENTRYPOINT [ "/app/home-server" ]
